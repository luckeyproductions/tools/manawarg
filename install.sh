sudo apt-get update
sudo apt-get install --no-upgrade make cmake build-essential libc-dev-bin libx11-dev libxrandr-dev libasound2-dev libegl1-mesa-dev libwayland-dev wayland-protocols qt5-default qtbase5-dev qt5-qmake

cd $(dirname "$0")

./update.sh
