include(src/ManaWarg.pri)

lessThan(QT_VERSION, 4.2) {
    error("ManaWarg requires Qt 4.2 or greater")
}

TARGET = manawarg

QT += core gui widgets

LIBS += \
    $${PWD}/Dry/lib/libDry.a \
    -lpthread \
    -ldl \
    -lGL

QMAKE_CXXFLAGS += -std=c++17 -O2

INCLUDEPATH += \
    Dry/include \
    Dry/include/Dry/ThirdParty \

TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle

DISTFILES += \
    LICENSE_TEMPLATE

RESOURCES += \
    resources.qrc

unix {
    isEmpty(PREFIX) {
        PREFIX = /usr/local
    }
    isEmpty(BINDIR) {
        BINDIR = $$PREFIX/bin
    }
    isEmpty(DATADIR) {
        DATADIR = $$PREFIX/share
    }

    target.path = $$BINDIR

    pixmap.files = linux/icons/manawarg.xpm
    pixmap.path = $$DATADIR/pixmaps

    icon.files = linux/icons/hicolor/*
    icon.path = $$DATADIR/icons/hicolor

    desktop.files = linux/manawarg.desktop
    desktop.path = $$DATADIR/applications/

    appdata.files = linux/manawarg.appdata.xml
    appdata.path = $$DATADIR/appdata/

    resources.files = Resources/*
    resources.path = $$(HOME)/.local/share/luckey/manawarg

    INSTALLS += target pixmap icon desktop appdata resources
}
