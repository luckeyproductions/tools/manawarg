class Saucer: ScriptObject
{
	void Start()
	{
	    StaticModel@ object = node.GetOrCreateComponent("StaticModel");
	    object.model = cache.GetResource("Model", "Models/Saucer.mdl");
	    object.material = cache.GetResource("Material", "Materials/Metal.xml");
	}

	void Update(float timeStep)
	{
	    node.Yaw(55 * timeStep);
		node.Translate(Vector3(0, 0, 5 * timeStep));
	}
}