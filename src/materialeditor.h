/* ManaWarg
// Copyright (C) 2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef MATERIALEDITOR_H
#define MATERIALEDITOR_H

#include <QAction>
#include <QGroupBox>
#include <QScrollArea>
#include <QLineEdit>
#include <QFormLayout>

#include "view3d.h"
#include "propertieswidget.h"

#include "filewidget.h"

class MaterialEditor: public FileWidget
{
    Q_OBJECT
    DRY_OBJECT(MaterialEditor, FileWidget)

public:
    explicit MaterialEditor(Context* context, QWidget *parent = nullptr);
    ~MaterialEditor();

    void setOrientation(Qt::Orientation orientation) override;

    Material* material() const { return material_; }
    bool openMaterial(const String& materialFileName);
    void setMaterial(Material* material);
    void setTechnique(Technique* technique);
    void setPreviewModel(String modelName);

public slots:
    void showEvent(QShowEvent*) override;
    void updateMaterial(QObject* sender);
    void actionNew() override;

protected:
    void dragEnterEvent(QDragEnterEvent* event) override;
    void dropEvent(QDropEvent* event) override;

protected slots:
    void actionOpen() override;
    void actionSave(bool saveAs = false) override;

private slots:
    void showPreviewMenu(const QPoint& pos);
    void pickBackgroundColor();
    void updateBackgroundColor(const Color& color);

    void setPreviewModel();

private:
    void createPreviewScene();

    View3D* materialView_;
    FlipSplitter* splitter_;
    QVBoxLayout* bottomLayout_;
    FlipSplitter* bottomSplitter_;
    PropertiesWidget* propertiesWidget_;

    Scene* scene_;
    Camera* camera_;
    StaticModel* previewModel_;
    SharedPtr<Material> material_;
    Color backgroundColor_;
    void updatePropertiesVisibility();
};

#endif // MATERIALEDITOR_H
