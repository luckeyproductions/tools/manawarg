/* ManaWarg
// Copyright (C) 2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef PROPERTIESEDITOR_H
#define PROPERTIESEDITOR_H

#include <QLabel>
#include "propertieswidget.h"
#include "filewidget.h"

class PropertiesEditor: public FileWidget
{
    Q_OBJECT
    DRY_OBJECT(PropertiesEditor, FileWidget)

public:
    explicit PropertiesEditor(Context* context, QWidget* parent = nullptr);

    void updateFields() { propertiesWidget_->updatePropertyFields(); }
    PropertiesWidget* widget() const { return propertiesWidget_; }
    Scene* scene() const { return propertiesWidget_->scene(); }

public slots:
    void activeObjectChanged(Object* object);

private:
    PropertiesWidget* propertiesWidget_;
    QLabel* typeLabel_;
};

#endif // PROPERTIESEDITOR_H
