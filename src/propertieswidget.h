/* ManaWarg
// Copyright (C) 2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef PROPERTIESWIDGET_H
#define PROPERTIESWIDGET_H

#include <QFormLayout>
#include <QScrollArea>
#include <QLineEdit>
#include <QTimer>

#include "dry.h"
#include "propertyrow.h"

class FileWidget;

class PropertiesWidget: public QScrollArea, Object
{
    friend class PropertiesEditor;

    Q_OBJECT
    DRY_OBJECT(PropertiesWidget, Object)

public:
    explicit PropertiesWidget(Context* context, FileWidget* parent = nullptr);

    Serializable* serializable() const
    {
        if (!object_ || !object_->IsInstanceOf<Serializable>())
            return nullptr;
        else
            return static_cast<Serializable*>(object_.Get());
    }
    void setObject(Object* object);
    void updatePropertyFields();
    void setPropertiesVisible(bool visible);
    void setRowWrapPolicy(QFormLayout::RowWrapPolicy policy) { propertiesLayout_->setRowWrapPolicy(policy); }
    void delayedResize() const { QTimer::singleShot(1, this, SLOT(resizeEvent())); }

    FileWidget* dryWidget() { return dryWidget_; }

    void createAttributeFields();
    static PropertyType toPropertyType(VariantType variantType);
    int countSubAttributes();
    void handleNumSubAttributesChanged();

signals:
    void subAttributeCountChanged(Serializable* serializable);

public slots:
    void propertyModified(const String& attributeName = "");

protected:
    void resizeEvent(QResizeEvent* event) override;

private slots:
    void pickTechnique();
    void resizeEvent() { resizeEvent(nullptr); }

private:
    void clear();
    void createPropertyFields();
    PropertyRow* createPropertyRow(const QString& text, PropertyType type);
    PropertyRow* createResourcePropertyRow(const QString& text, StringHash resourceType, bool list);
    bool techniqueUsesUnit(Technique* technique, TextureUnit unit);
    void createMaterialPropertyFields();
    void updateLabels();
    Variant attributeValue(const String& attributeName);
    Scene* scene();

    FileWidget* dryWidget_;
    WeakPtr<Object> object_;
    QFormLayout* propertiesLayout_;
    QWidget* containerWidget_;
    std::vector<PropertyRow*> rows_;
    bool ignoreModifications_;

};

#endif // PROPERTIESWIDGET_H
