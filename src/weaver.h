/* ManaWarg
// Copyright (C) 2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef WEAVER_H
#define WEAVER_H

#include "dry.h"
#include <QApplication>

#define FILENAME_PROJECT Weaver::applicationName().append(".xml").toLatin1().data()

class Qocoon;

class Weaver: public QApplication,  public Application
{
    Q_OBJECT
    DRY_OBJECT(Weaver, Application)

public:
    Weaver(int & argc, char** argv, Context* context);
    virtual ~Weaver();

    static Context* context() { return context_s;}

    void Setup() override;
    void Start() override;
    void Stop() override;
    void runFrame();

public slots:
    void exit();
    void onTimeout();
    void requestUpdate() { requireUpdate_ = true; }

private:
    static Context* context_s;
    void handleArgument();
    bool looksLikeUrho(const QString& engineFolder) const;
    bool looksLikeUrho(const String& engineFolder) const;

    String argument_;
    bool requireUpdate_;
    Qocoon* qocoon_;
};

#endif // WEAVER_H
