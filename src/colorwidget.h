/* ManaWarg
// Copyright (C) 2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef COLORWIDGET_H
#define COLORWIDGET_H

#include "dry.h"
#include <QFormLayout>
#include <QPushButton>
#include <QLabel>
#include <QGroupBox>
#include <QMap>

#include <QWidget>

#define SPECPOWER 4.f

class FileWidget;

class ColorWidget: public QWidget
{
    Q_OBJECT

public:
    explicit ColorWidget(const QString& name, QWidget* parent = nullptr);

    QGroupBox* pickerBox() const;
    FileWidget* dryWidget() { return dryWidget_; }

    Color& color() { return color_; }

    bool isDiffuse() const;
    bool isSpecular() const;
    bool isEmissive() const;

public slots:
    void resizeEvent(QResizeEvent*) override;
    void updateColorButton();
    void setColor(const Color& color);

signals:
    void colorChanged();
    void colorPicked();
    void pickerShown();

private slots:
    void pickColor();

private:
    FileWidget* dryWidget_;
    QPushButton* colorButton_;
    Color color_;
};

#include <QSlider>
#include <QTabBar>

enum ColorEnum{ RED, GREEN, BLUE, ALPHA };
enum ColorMode{ RGB, HSV, Hex };

class ColorPicker: public QWidget
{
    Q_OBJECT

public:
    explicit ColorPicker(ColorWidget* parent);
    explicit ColorPicker(Color& color);
    void setColor(const Color& color)
    {
        color_ = color;
        updateSliderValues();
    }


public slots:
    void hideBox();
    void updateColor();

    void resizeEvent(QResizeEvent*) override;
    void paintEvent(QPaintEvent*) override;

signals:
    void colorChanged(const Color& color);

public slots:
    void updateSliderValues();
    void changeMode();

private:
    static ColorMode mode_;
    ColorWidget* colorWidget_;
    QVBoxLayout* mainLayout_;
    QFormLayout* formLayout_;
    Color color_;
    QLabel* colorLabel_;
    QTabBar* tabBar_;

    QMap<int, QSlider*> sliders_;
    QMap<int, QLabel*> labels_;

    void createMainLayout();
    void createFormLayout();
    void createSliders();
    void updateToolTips();
    bool modeRGB() { return mode_ == RGB; }
    bool modeHSV() { return mode_ == HSV; }
    bool modeHex() { return mode_ == Hex; }
    void updateFormLabels();
    void createTabBar();
    void updateColorLabelVisibility();
    void truncateLabels();
};

#endif // COLORDIALOG_H
