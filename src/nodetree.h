/* ManaWarg
// Copyright (C) 2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef NODETREE_H
#define NODETREE_H

#include <QTreeWidget>
#include "selector.h"

#include "filewidget.h"

class NodeTree: public FileWidget
{
    Q_OBJECT

public:
    explicit NodeTree(Context* context, QWidget* parent = nullptr);

    Scene* scene() const { return scene_; }

    PODVector<Animatable*> selectedObjects();
    PODVector<Node*> selectedNodes(bool recursive, bool excludeScene = false);

    PODVector<Animatable*> deletableAnimatables();

protected:
    void dragEnterEvent(QDragEnterEvent* event) override;
    void dragMoveEvent(QDragMoveEvent* event) override;
    void dropEvent(QDropEvent* event) override;

public slots:
    void updateTree(bool restore = true);
    void selectionChanged(Selector* selector);

private slots:
    void treeItemSelectionChanged();
    void showContextMenu(const QPoint& pos);

private:
    void createTreeWidget();
    void grow(QTreeWidgetItem* item, Node* node);
    QTreeWidgetItem* toItem(Object* object, QTreeWidgetItem* parent = nullptr);
    Object* getObject(QTreeWidgetItem* item);
    QList<QTreeWidgetItem*> nodeItems() const;

    std::vector<QTreeWidgetItem*> allItems() const;
    void collectChildren(std::vector<QTreeWidgetItem*>& children, QTreeWidgetItem* item, bool recursive) const;
    QVector<unsigned> collectExpandedNodeIDs() const;
    QPair<QVector<unsigned>, QVector<unsigned> > collectSelectedObjectIDs() const;
    void restoreExpanded(const QVector<unsigned>& expandedNodes);
    void restoreSelection(const QPair<QVector<unsigned>, QVector<unsigned> >& selectedObjects);

    void savePrefab(Node* node);

    QTreeWidget* treeWidget_;
    Scene* scene_;

    QList<QTreeWidgetItem*> selectedItems_;
};

#endif // NODETREE_H
