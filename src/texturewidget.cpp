/* ManaWarg
// Copyright (C) 2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QHBoxLayout>
#include <QDragEnterEvent>
#include "weaver.h"
#include "utility.h"
#include "qocoon.h"

#include "texturewidget.h"

TextureWidget::TextureWidget(TextureUnit textureUnit, QWidget* parent): DryWidget(Weaver::context(), parent),
    textureButton_{ new QPushButton(this) },
    texture_{ nullptr },
    textureUnit_{ textureUnit }
{
    QHBoxLayout* layout{ new QHBoxLayout(this) };
    layout->setMargin(0);

    textureButton_->setMaximumSize(textureButton_->maximumWidth(), 29);
//    textureButton_->setMinimumSize(32, 32);
//    textureButton_->setIconSize(QSize(512, 30));
    textureButton_->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Maximum);
    setAcceptDrops(true);
    layout->addWidget(textureButton_);

    setLayout(layout);

    updateTextureButton();

    connect(textureButton_, SIGNAL(clicked(bool)), this, SLOT(pickTexture()));
}

void TextureWidget::setTexture(Texture* texture)
{
    if (texture == texture_)
        return;

    texture_ = texture;

    updateTextureButton();
}

void TextureWidget::updateTextureButton()
{
    if (!texture_)
    {
        textureButton_->setText(textureUnit_ == TU_ENVIRONMENT ? "Cubemap" : "Texture");
        QString unitName{ toQString(textureUnitNames[textureUnit_]) };
        if (!unitName.isEmpty())
            unitName = unitName.append(" ");
        textureButton_->setToolTip("No " + unitName + "texture set");
        textureButton_->setIcon(QIcon());
        return;
    }

    textureButton_->setText("");
    textureButton_->setToolTip(toQString(texture_->GetName()));

    QPixmap pixmap{ textureButton_->iconSize() };
    QPixmap texturePixmap;
    Image* image{ nullptr };
    paintCheckerboard(&pixmap);

    if (GetExtension(texture()->GetName()) != ".png")
        texturePixmap = QPixmap("");
    else
        image = static_cast<Texture2D*>(texture_)->GetImage();

    if (image)
        texturePixmap = toPixmap(image);

    if (!texturePixmap.isNull())
    {
        QPainter p{ &pixmap };
        p.drawPixmap(pixmap.rect(), texturePixmap);
        p.end();
    }

    textureButton_->setIconSize(pixmap.size());
    textureButton_->setIcon(QIcon(pixmap));
}

void TextureWidget::resizeEvent(QResizeEvent* event)
{
    QSize iconSize{ size() - QSize(8, 4) };

    if (iconSize.height() < 8)
        iconSize.setHeight(8);

    textureButton_->setIconSize(iconSize);
    updateTextureButton();
}

void TextureWidget::pickTexture()
{

}

void TextureWidget::dragEnterEvent(QDragEnterEvent* event)
{
    const QMimeData* mime{ event->mimeData() };

    if (isOfType<Texture2D>(mime))
        event->acceptProposedAction();
}

void TextureWidget::dropEvent(QDropEvent* event)
{
    const QMimeData* mime{ event->mimeData() };

    String fileName{ toString(extractDataMap(mime)[FileName].toString()) };

    if (!fileName.IsEmpty())
    {
        ResourceCache* cache{ GetSubsystem<ResourceCache>() };
        setTexture(cache->GetResource<Texture2D>(fileName));

        emit textureChanged();
    }
}
