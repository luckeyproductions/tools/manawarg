/* ManaWarg
// Copyright (C) 2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef UTILITY_H
#define UTILITY_H

#include "dry.h"
#include <QMimeData>
#include <QPixmap>
#include <QPainter>
#include <QIODevice>
#include <QApplication>
#include <QPalette>

enum TransformationSpace{ TS_View   = -1,
                          TS_Local  = TS_LOCAL,
                          TS_Parent = TS_PARENT,
                          TS_World  = TS_WORLD
                        };

enum TransformationType{ TT_None, TT_Translate, TT_Rotate, TT_Scale };

enum AxisLock{ AL_None, AL_X, AL_Y, AL_Z,
               AL_NotX, AL_NotY, AL_NotZ };

enum UserRoles{ FileName = Qt::UserRole, DryTypeHash, ID };

enum PropertyType
{
    PT_None,
    PT_Technique,
    PT_Color,
    PT_Texture,
    PT_ColorAndTexture,
    PT_Bool,
    PT_Float,
    PT_Int,
    PT_Vector2,
    PT_Vector3,
    PT_Vector4,
    PT_Quaternion,
    PT_IntVector2,
    PT_IntVector3,
    PT_String,
    PT_StringList,
    PT_Enum,
    PT_Resource,
    PT_ResourceList,
};

static void paintCheckerboard(QPaintDevice* paintDevice, int squareSize = 8)
{
    if (squareSize == 0)
        return;

    QPixmap checkerboard{ 2 * squareSize, 2 * squareSize }; // Create pattern
    checkerboard.fill(QColor("#3a3b3c"));
    QPainter cP{ &checkerboard };
    cP.setBrush(QColor("#acabad"));
    cP.setPen(Qt::transparent);

    for (bool second: { false, true })
        cP.drawRect(second * squareSize, second * squareSize, squareSize, squareSize);

    cP.end();

    QPainter p{ paintDevice }; // Fill target with checkboard pattern
    p.setBrush(QBrush(checkerboard));
    p.setPen(QPen(QColor("#555555"), 2));
    p.drawRect(QRect(0, 0, paintDevice->width(), paintDevice->height()));
    p.end();
}

static inline QString toQString(String string)
{
    return QString{ string.CString() };
}

static inline String toString(QString qString)
{
    return String{ qString.toLatin1().data() };
}

static inline QColor toQColor(const Color color, bool alpha = true)
{
    return QColor::fromRgbF( std::min(std::max(0.f, color.r_), 1.f),
                             std::min(std::max(0.f, color.g_), 1.f),
                             std::min(std::max(0.f, color.b_), 1.f),
                    (alpha ? std::min(std::max(0.f, color.a_), 1.f) : 1.f) );
}

static inline Color toColor(const QColor qColor)
{
    return Color(qColor.redF(), qColor.greenF(), qColor.blueF(), qColor.alphaF());
}

static Color activeColor()
{
    Color color{ toColor(QApplication::palette().color(QPalette::Highlight)) };
    color.FromHSV(color.Hue(), 1.f, 1.f);
    return color;
}

static inline QPixmap toPixmap(Image* image)
{
    return QPixmap::fromImage(QImage{
                    image->GetData(),
                    image->GetWidth(),
                    image->GetHeight(),
                    QImage::Format_RGBA8888 });
}

static QMap<int, QVariant> extractDataMap(const QMimeData* mime)
{
    QByteArray encoded{ mime->data("application/x-qabstractitemmodeldatalist") };
    QDataStream stream(&encoded, QIODevice::ReadOnly);
    QMap<int, QVariant> roleDataMap{};

    while (!stream.atEnd())
    {
        int row, col;
        stream >> row >> col >> roleDataMap;
    }

//    for (int k: roleDataMap.keys())
//    {
//        const QVariant& val{ roleDataMap.value(k) };
//        Log::Write(LOG_INFO, "DATA: " + String(k) + ": " + toString(val.toString()));
//    }

    return roleDataMap;
}

template <class T> static bool isOfType(unsigned typeHash)
{
    return typeHash == T::GetTypeStatic().ToHash();
}

template <class T> static bool isOfType(const QMimeData* mimeData)
{
    QMap<int, QVariant> dataMap{ extractDataMap(mimeData) };

    if (dataMap.keys().contains(DryTypeHash))
        return isOfType<T>(dataMap[DryTypeHash].toUInt());
    else
        return false;
}

static inline bool isOfBinaryType(const QMimeData* mimeData)
{
    return isOfType<Model>(mimeData)
        || isOfType<Animation>(mimeData)
        || isOfType<Texture2D>(mimeData)
        || isOfType<Sound>(mimeData);
}

static inline Object* getObjectFromScene(Scene* scene, unsigned typeHash, unsigned id)
{
    Object* object{ nullptr };

    if (scene)
    {
        if (isOfType<Node>(typeHash) || isOfType<Scene>(typeHash))
            object = scene->GetNode(id);
        else
            object = scene->GetComponent(id);
    }

    return object;
}

static BoundingBox sceneBoundingBox(Node* scene)
{
    BoundingBox bounds{};
    PODVector<Drawable*> drawables{};
    scene->GetDerivedComponents<Drawable>(drawables, true);

    if (!drawables.IsEmpty())
    {
        for (Drawable* drawable: drawables)
        {
            if (drawable->IsInstanceOf<Light>()  ||
                drawable->IsInstanceOf<Zone>()   ||
                drawable->IsInstanceOf<Octree>() ||
                drawable->IsInstanceOf<Skybox>())
                continue;

            BoundingBox drawableBounds{ drawable->GetWorldBoundingBox() };

            if (bounds.min_.x_ == M_INFINITY)
                bounds = drawableBounds;
            else
                bounds.Merge(drawableBounds);
        }
    }

    for (Node* node: scene->GetChildren())
    {
        const Vector3 nodePos{ node->GetWorldPosition() };
        const BoundingBox nodeBounds{ nodePos - Vector3::ONE, nodePos + Vector3::ONE };

        if (bounds.min_.x_ == M_INFINITY)
            bounds = nodeBounds;
        else
            bounds.Merge(nodeBounds);
    }

    if (bounds.HalfSize().Length() < M_SQRT3)
        bounds = { bounds.Center() - Vector3::ONE, bounds.Center() + Vector3::ONE };

    return bounds;
}

static void completeMacroName(const PODVector<Animatable*> animatables, QString& macroName)
{
    QString componentName{};
    int nodeCount{};
    int componentCount{};
    for (Animatable* animatable: animatables)
    {
        if (animatable->IsInstanceOf<Component>())
        {
            Component* component{ static_cast<Component*>(animatable) };
            const QString typeName{ toQString(component->GetTypeName()) };
            if (componentName.isEmpty())
                componentName = typeName;
            else if (componentName != typeName)
                componentName = "Component";

            ++componentCount;
        }
        else
            ++nodeCount;
    }

    if (nodeCount > 0)
    {
        macroName += "Node";
        if (nodeCount > 1)
            macroName += "s";
    }

    if (componentCount > 0)
    {
        if (nodeCount > 0)
            macroName += " and ";
        macroName += componentName;
        if (componentCount > 1)
        {
            if (componentName.endsWith('y'))
            {
                macroName.chop(1);
                macroName += "ies";
            }
            else if (componentName.endsWith('x') || componentName.endsWith('h'))
            {
                macroName += "es";
            }
            else
            {
                macroName += "s";
            }
        }
    }
}

#endif // UTILITY_H
