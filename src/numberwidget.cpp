/* ManaWarg
// Copyright (C) 2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QHBoxLayout>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include "propertieswidget.h"

#include "numberwidget.h"

NumberWidget::NumberWidget(PropertyType type, QWidget* parent): QWidget(parent),
    quaternion_{ type == PT_Quaternion }
{
    QHBoxLayout* mainLayout{ new QHBoxLayout{} };
    PropertyRow* row{ static_cast<PropertyRow*>(parent) };

    mainLayout->setMargin(1);

    bool isFloat{ PropertyRow::isFloatNumber(type) };
    unsigned dimensions{ 1 };

    if (type == PT_Vector2 || type == PT_IntVector2)
        dimensions = 2;
    else if (type == PT_Vector3 || type == PT_IntVector3 || type == PT_Quaternion)
        dimensions = 3;
    else if (type == PT_Vector4)
        dimensions = 4;

    for (unsigned d{ 0u }; d < dimensions; ++d)
    {
        if (isFloat)
        {
            QDoubleSpinBox* spinBox{ new QDoubleSpinBox(this) };
            double maxmimum{ 10000. };
            spinBox->setFocusPolicy(Qt::StrongFocus);
            spinBox->setAccelerated(true);
            spinBox->setMaximum( maxmimum);
            spinBox->setMinimum(-maxmimum);
            mainLayout->addWidget(spinBox);

            if (row)
                connect(spinBox, SIGNAL(editingFinished()), row, SLOT(updateValue()));
        }
        else
        {
            QSpinBox* spinBox{ new QSpinBox(this) };
            int maxmimum{ 10000 };
            spinBox->setFocusPolicy(Qt::StrongFocus);
            spinBox->setAccelerated(true);
            spinBox->setMaximum( maxmimum);
            spinBox->setMinimum(-maxmimum);
            mainLayout->addWidget(spinBox);

            if (row)
                connect(spinBox, SIGNAL(editingFinished()), row, SLOT(updateValue()));
        }
    }

    setLayout(mainLayout);
}

void NumberWidget::setValue(const Variant& value)
{
    if (PropertyRow::isFloatNumber(PropertiesWidget::toPropertyType(value.GetType())))
    {
        QList<QDoubleSpinBox*> spinBoxes{ findChildren<QDoubleSpinBox*>() };

        for (QDoubleSpinBox* box: spinBoxes)
        {
            switch (spinBoxes.count())
            {
            case 0: default: break;
            case 1: box->setValue(value.GetFloat()); break;
            case 2: box->setValue(value.GetVector2().Data()[spinBoxes.indexOf(box)]); break;
            case 3: (quaternion_ == true
                    ? box->setValue(value.GetQuaternion().EulerAngles().Data()[spinBoxes.indexOf(box)])
                    : box->setValue(value.GetVector3().Data()[spinBoxes.indexOf(box)])); break;
            case 4: box->setValue(value.GetVector4().Data()[spinBoxes.indexOf(box)]); break;
                break;
            }
        }
    }
    else
    {
        QList<QSpinBox*> spinBoxes{ findChildren<QSpinBox*>() };

        for (QSpinBox* box: spinBoxes)
        {
            switch (spinBoxes.count())
            {
            case 0: default: break;
            case 1: box->setValue(value.GetInt()); break;
            case 2: box->setValue(value.GetIntVector2().Data()[spinBoxes.indexOf(box)]); break;
            case 3: box->setValue(value.GetIntVector3().Data()[spinBoxes.indexOf(box)]); break;
                break;
            }
        }
    }
}

Variant NumberWidget::value()
{
    QList<QDoubleSpinBox*> doubleSpinBoxes{ findChildren<QDoubleSpinBox*>() };
    QList<QSpinBox*> spinBoxes{ findChildren<QSpinBox*>() };

    switch (doubleSpinBoxes.size())
    {
    case 0: default: break;
    case 1: return { static_cast<float>(doubleSpinBoxes.first()->value()) };
    case 2: return { Vector2{ static_cast<float>(doubleSpinBoxes.first()->value()),
                              static_cast<float>(doubleSpinBoxes.last()->value()) } };
    case 3:
    {
        const Vector3 vec{ static_cast<float>(doubleSpinBoxes.first()->value()),
                           static_cast<float>(doubleSpinBoxes.at(1)->value()),
                           static_cast<float>(doubleSpinBoxes.last()->value()) };

        if (quaternion_)
            return { Quaternion{ vec } };
        else
            return { vec };
    }
    case 4: return { Vector4{ static_cast<float>(doubleSpinBoxes.first()->value()),
                              static_cast<float>(doubleSpinBoxes.at(1)->value()),
                              static_cast<float>(doubleSpinBoxes.at(2)->value()),
                              static_cast<float>(doubleSpinBoxes.last()->value()) } };
    }
    switch (spinBoxes.size())
    {
    case 0: default: break;
    case 1: return {             spinBoxes.first()->value() };
    case 2: return { IntVector2{ spinBoxes.first()->value(),
                                 spinBoxes.last()->value() } };
    case 3: return { IntVector3{ spinBoxes.first()->value(),
                                 spinBoxes.at(1)->value(),
                                 spinBoxes.last()->value() } };
    }

    return {};
}
