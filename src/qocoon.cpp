/* ManaWarg
// Copyright (C) 2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QMenuBar>
#include <QToolBar>
#include <QStatusBar>
#include <QDialogButtonBox>
#include <QPushButton>
#include <QLabel>
#include <QSettings>
#include <QFileDialog>
#include <QDesktopServices>
#include <QUrl>
#include <QAction>
#include <QStackedWidget>
#include <QScreen>

#include "undostack/attributecommand.h"
#include "undostack/hierarchycommand.h"
#include "nodetree.h"
#include "propertieseditor.h"
#include "materialeditor.h"
#include "clipboard.h"
#include "resourcebrowser/resourcebrowser.h"
#include "texteditor.h"
#include "noprojectwidget.h"
#include "weaver.h"
#include "utility.h"

#include "qocoon.h"

auto EMPTY_SCENE = []()
{
    Scene* scene{ new Scene(Qocoon::project()->GetContext()) };
    scene->CreateComponent<Octree>();
    return scene;
};

SharedPtr<Project> Qocoon::project_{ nullptr };

Qocoon::Qocoon(Context* context): QMainWindow(), Object(context),
    singleWidget_{ false },
    mode_{ EM_AllModes },
    modeActions_{},
    projectDependentActions_{},
    sceneDependentActions_{},
    mainToolBar_{ nullptr },
    centralStackWidget_{ new QStackedWidget{ this } },
    noProjectWidget_{ new NoProjectWidget{ context_, this } },
    centralView_{ new SceneViewer{ context_, this } },
    hiddenDocks_{},
    undoStacks_{},
    undoView_{ new QUndoView{ this } },
    undoAction_{ nullptr },
    redoAction_{ nullptr },
    runAction_{ nullptr },
    fullViewAction_{ new QAction{ QIcon{ ":/FullView" }, "Fullscreen Scene View", this } },
    deleteAction_{ nullptr },
    recentMenu_{ nullptr },
    transformSpaceBox_{ nullptr }
{
    context_->RegisterSubsystem(this);

    setWindowIcon(QIcon{ ":/Icon" });
    setMinimumSize({ 640, 480 });

    setCentralWidget(centralStackWidget_);
    centralStackWidget_->addWidget(noProjectWidget_);
    centralStackWidget_->addWidget(centralView_);
    centralStackWidget_->setMinimumSize({ 320, 240 });

    connect(noProjectWidget_, SIGNAL(newProject()), this, SLOT(newProject()));
    connect(noProjectWidget_, SIGNAL(openProject(QString)), this, SLOT(openProject(QString)));
    connect(this, SIGNAL(recentProjectsChanged()), noProjectWidget_, SLOT(updateRecentList()));

    undoView_->setEmptyLabel("Initial");
    QDockWidget* undoDockWidget{ new QDockWidget{ "Scene Undo History", this } };
    undoDockWidget->setObjectName("Scene Undo History");
    undoDockWidget->setWidget(undoView_);
    addDockWidget(Qt::LeftDockWidgetArea, undoDockWidget);
    connect(undoView_->selectionModel(), SIGNAL(currentRowChanged(QModelIndex, QModelIndex)),
            centralView_, SLOT(updateView()));

    createToolBar();

    createDockWidget<MaterialEditor>(Qt::BottomDockWidgetArea);
    createDockWidget<ResourceBrowser>(Qt::BottomDockWidgetArea);
    createDockWidget<PropertiesEditor>(Qt::RightDockWidgetArea);
    
    DryDockWidget* nodeTreeDockWidget{ createDockWidget<NodeTree>(Qt::RightDockWidgetArea) };
    DryDockWidget* textDockWidget{ createDockWidget<TextEditor>(Qt::RightDockWidgetArea) };
    tabifyDockWidget(textDockWidget, nodeTreeDockWidget);

    setCorner(Qt::BottomRightCorner, Qt::RightDockWidgetArea);
    setCorner(Qt::BottomLeftCorner,  Qt::BottomDockWidgetArea);

    fullViewAction_->setCheckable(true);
    fullViewAction_->setShortcut(QKeySequence{ "F11" });
    addAction(fullViewAction_);
    connect(fullViewAction_, SIGNAL(triggered(bool)), this, SLOT(toggleFullView(bool)));

    connect(this, SIGNAL(currentProjectChanged()), SLOT(updateAvailableActions()));

    Clipboard* clipboard{ context_->RegisterSubsystem<Clipboard>() };

    createMenuBar();
    setStatusBar(new QStatusBar{ this });
    updateRecentProjects();
    loadSettings();

    show();
}

Qocoon::Qocoon(Context* context, XMLFile* xmlFile): QMainWindow(), Object(context),
    singleWidget_{ true },
    mode_{ EM_AllModes },
    undoAction_{ nullptr },
    redoAction_{ nullptr }
{
    context_->RegisterSubsystem(this);
    setWindowIcon(QIcon{ ":/Icon" });
    setStatusBar(new QStatusBar{ this });

    String fileName{ xmlFile->GetName() };
    const unsigned rootNameHash{ xmlFile->GetRoot().GetName().ToHash() };
    const String resourceRoot{ Qocoon::locateResourceRoot(fileName) };
    if (!loadProject(toQString(resourceRoot + FILENAME_PROJECT)))
    {
        /// Pick new project location
        /// Add resource folder
        return;
    }

    FileWidget* fileWidget{ nullptr };
    QToolBar* toolBar{ new QToolBar{ "Toolbar", this } };
    QMenu* fileMenu{ createMenuBar() };
    ResourceCache* cache{ GetSubsystem<ResourceCache>() };
    const String trimmedFilename{ fileName.Replaced(Qocoon::locateResourceRoot(fileName), "") };
    QSizeF sizeHintMultiplier{ 1.f, 1.f };

    if (rootNameHash == StringHash{ "material" }.ToHash())
    {
        SharedPtr<Material> material{ cache->GetTempResource<Material>(trimmedFilename) };

        if (!material.IsNull())
        {
            MaterialEditor* materialEditor{ new MaterialEditor{ context_ } };
            context_->RegisterSubsystem(materialEditor);
            materialEditor->setMaterial(material);
            sizeHintMultiplier = { 3.4f, 5.5f };
            fileWidget = materialEditor;
        }
    }
    else
    {
        TextEditor* textEditor{ new TextEditor{ context_ } };
        context_->RegisterSubsystem(textEditor);
        textEditor->openFile(fileName);
        sizeHintMultiplier = { 4.f, 2.f };
        fileWidget = textEditor;
    }

    if (fileWidget)
    {
        setCentralWidget(fileWidget);
        resize(sizeHint().width() * sizeHintMultiplier.width(), sizeHint().height() * sizeHintMultiplier.height());
        fileWidget->setFileButtonsVisible(false);
        fileMenu->insertActions(fileMenu->actions().last(), fileWidget->fileOperations());
        toolBar->addActions(fileWidget->fileOperations());
    }

    addToolBar(toolBar);
    updateRecentProjects();
    createDockWidget<ResourceBrowser>(Qt::BottomDockWidgetArea);

    setGeometry(QStyle::alignedRect(
            Qt::LeftToRight, Qt::AlignCenter,
            size(), QRect{ {}, QGuiApplication::screens().first()->size() } ));

    show();
}

Qocoon::~Qocoon()
{
    if (singleWidget_)
        return;

    QSettings settings{};

    if (fullViewAction_->isChecked())
        fullViewAction_->trigger();

    settings.setValue("geometry", saveGeometry());
    settings.setValue("state", saveState());
}

void Qocoon::loadSettings()
{
    QSettings settings{};

    restoreGeometry(settings.value("geometry").toByteArray());
    restoreState(settings.value("state").toByteArray());
}

void Qocoon::createToolBar()
{
    mainToolBar_ = new QToolBar{ "Toolbar" };
    mainToolBar_->setObjectName("MainToolbar");

    for (EditMode mode: MODES)
    {
        QString modeName{ modeToString(mode) };
        QAction* action{ new QAction{ modeName + " Mode", this } };
        action->setProperty("mode", mode);
        action->setIcon(QIcon{ ":/" + modeName + "Mode" });
        action->setCheckable(true);
        modeActions_[mode] = action;
        action->setShortcut({ "Ctrl+Shift+" + QString::number(modeActions_.count()) });

        connect(action, SIGNAL(triggered(bool)), this, SLOT(changeMode(bool)));
        mainToolBar_->addAction(action);

        if (mode != EM_Standard) // Currently only one mode available
            action->setEnabled(false);
    }

    mainToolBar_->addSeparator();

    for (bool undo: { true, false })
    {
        QString  actionName{ QString{ (undo ? "Un" : "Re") } + "do" };
        QAction* action{ new QAction{ actionName, this } };
        action->setObjectName(actionName + "action");
        action->setIcon(QIcon(":/" + actionName ));
        action->setShortcut(("Ctrl+" + QString{ (undo ? "" : "Shift+") } + "Z"));
        action->setEnabled(false);

        if (undo)
        {
            undoAction_ = action;
            connect(action, SIGNAL(triggered(bool)), this, SLOT(undo()));
        }
        else
        {
            redoAction_ = action;
            connect(action, SIGNAL(triggered(bool)), this, SLOT(redo()));
        }

        mainToolBar_->addAction(action);
    }

    runAction_ = new QAction{ QIcon{ ":/Play" }, "Run" };
    runAction_->setShortcut({ "Ctrl+R" });
    runAction_->setCheckable(true);
    connect(runAction_, SIGNAL(triggered(bool)), this, SLOT(runSimulation(bool)));
    sceneDependentActions_.push_back(runAction_);

    mainToolBar_->addActions(sceneDependentActions_);

    transformSpaceBox_= new QComboBox{ mainToolBar_ };
    transformSpaceBox_->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    for (TransformationSpace space: { TS_View, TS_Local, TS_Parent, TS_World })
    {
        QString itemText{};
        switch (space) {
        default: break;
        case TS_View:   itemText = "View"; break;
        case TS_Local:  itemText = "Local"; break;
        case TS_Parent: itemText = "Parent"; break;
        case TS_World:  itemText = "World"; break;
        }

        transformSpaceBox_->addItem(itemText, { space });
    }
    transformSpaceBox_->setCurrentIndex(transformSpaceBox_->count() - 1);
    transformSpaceBox_->setMinimumWidth(64);
    mainToolBar_->addWidget(transformSpaceBox_);

    addToolBar(mainToolBar_);
    setMode(EM_Standard);
}

QMenu* Qocoon::createMenuBar()
{
    QMenu* fileMenu{ new QMenu{ "File" } };

    fileMenu->addMenu(createProjectMenu());
    if (!singleWidget_)
        fileMenu->addMenu(createSceneMenu());
    fileMenu->addSeparator();
    projectDependentActions_.push_back(fileMenu->addAction(QIcon{ ":/Open" }, "Add Resource Folder...",
                                                           this, SLOT(addResourceFolder())));
    fileMenu->addSeparator();
    fileMenu->addAction(QIcon(":/Quit"), "Exit", this, SLOT(close()), QKeySequence("Ctrl+Q"));
    menuBar()->addMenu(fileMenu);

    if (!singleWidget_)
        createEditMenu();

    if (!SceneViewer::sceneViewers_.empty())
        menuBar()->addMenu(createAddMenu());

    if (!singleWidget_)
        createViewMenu();

    createHelpMenu();

    updateAvailableActions();

    return fileMenu;
}

QMenu* Qocoon::createProjectMenu()
{
    QMenu* projectMenu{ new QMenu{ "Project" }};
    projectMenu->setIcon(QIcon{ ":/Icon" });

    recentMenu_ = projectMenu->addMenu("Recent Projects");
    connect(this, SIGNAL(recentProjectsChanged()), SLOT(updateRecentProjectMenu()));
    projectMenu->addAction(QIcon{ ":/New"  }, "New Project...",  this, SLOT(newProject()),  { "Ctrl+N" });
    projectMenu->addAction(QIcon{ ":/Open" }, "Open Project...", this, SLOT(openProject()), { "Ctrl+O" });
    projectDependentActions_.push_back(projectMenu->addAction(QIcon{ ":/Save" }, "Save Project",
                                                           this, SLOT(saveProject()), { "Ctrl+S" }));
    projectDependentActions_.push_back(projectMenu->addAction(QIcon{ ":/Delete" }, "Close Project",
                                                           this, SLOT(closeProject())));

    return projectMenu;
}

QMenu* Qocoon::createSceneMenu()
{
    QMenu* sceneMenu{ new QMenu{ "Scene" }};
    sceneMenu->setIcon(QIcon{ ":/SceneResource" });

    connect(this, SIGNAL(recentProjectsChanged()), SLOT(updateRecentProjectMenu()));
    projectDependentActions_.push_back(sceneMenu->addAction(QIcon{ ":/New"  }, "New Scene",  this, SLOT(newScene())));
    projectDependentActions_.push_back(sceneMenu->addAction(QIcon{ ":/Open" }, "Open Scene...", this, SLOT(openScene())));
    sceneDependentActions_.push_back(sceneMenu->addAction(QIcon{ ":/Save" }, "Save Scene",
                                                           this, SLOT(saveScene())));
    sceneDependentActions_.push_back(sceneMenu->addAction(QIcon{ ":/SaveAs" }, "Save Scene As...",
                                                           this, SLOT(saveSceneAs())));
    sceneDependentActions_.push_back(sceneMenu->addAction(QIcon{ ":/Delete" }, "Close Scene",
                                                           this, SLOT(closeScene())));

    return sceneMenu;
}

void Qocoon::createEditMenu()
{
    QMenu* editMenu{ new QMenu{ "Edit" } };
    editMenu->addAction(undoAction_);
    editMenu->addAction(redoAction_);

    editMenu->addSeparator();
    editMenu->addActions(GetSubsystem<Clipboard>()->editActions());
    deleteAction_ = editMenu->addAction(QIcon{ ":/Delete" }, "Delete", this, SLOT(actionDelete()), { "Delete" });

    menuBar()->addMenu(editMenu);
}

void Qocoon::createViewMenu()
{
    QMenu* viewMenu{ new QMenu{ "View" } };
    viewMenu->addAction(fullViewAction_);
    menuBar()->addMenu(viewMenu);
}

void Qocoon::createHelpMenu()
{
    QMenu* helpMenu{ new QMenu{ "Help" } };
    helpMenu->addAction(QIcon(":/About"), tr("About %1...").arg(Weaver::applicationDisplayName()), this, SLOT(about()));
    menuBar()->addMenu(helpMenu);
}

void Qocoon::updateAvailableActions()
{
    for (QAction* action: projectDependentActions_)
        action->setEnabled(project() != nullptr);

    for (QAction* action: sceneDependentActions_)
        action->setEnabled(activeScene() != nullptr);

    const bool running{ isRunning() };
    addMenu()->setEnabled(activeScene() != nullptr && !running);
    deleteAction_->setEnabled(!running);

    if (SceneUndoStack* activeUndoStack{ sceneStack(activeScene()) })
    {
        allowUndo(activeUndoStack->canUndo() && !running);
        allowRedo(activeUndoStack->canRedo() && !running);
    }

    if (PropertiesEditor* propertiesEditr{ GetSubsystem<PropertiesEditor>() })
        propertiesEditr->setEnabled(!running);
}

void Qocoon::updateRecentProjectMenu()
{
    const QSettings settings{};
    const QStringList recentProjects{( settings.value("recentprojects").toStringList() )};

    recentMenu_->clear();

    for (const QString& recent: recentProjects)
    {
        QString displayName{ recent.left(recent.length() - (String{ FILENAME_PROJECT }.Length() + 1)) };
        recentMenu_->addAction(displayName, this, SLOT(openRecent()))->setObjectName(recent);
    }
}

void Qocoon::updateRecentProjects()
{
    QSettings settings{};
    QStringList recentProjects{( settings.value("recentprojects").toStringList() )};
    QString currentProject{ toQString(projectLocation() + FILENAME_PROJECT) };

    for (const QString& recent: recentProjects)
    {
        if (!QFileInfo(recent).exists() || currentProject == recent )
            recentProjects.removeOne(recent);
    }

    if (project_)
        recentProjects.push_front(currentProject);

    while (recentProjects.size() > 10)
        recentProjects.pop_back();

    settings.setValue("recentprojects", recentProjects);

    emit recentProjectsChanged();
}

void Qocoon::newProject(QString startPath)
{
    if (startPath.isEmpty())
        startPath = (project_ ? toQString(GetParentPath(projectLocation()))
                              : QStandardPaths::writableLocation(QStandardPaths::HomeLocation));

    String selectedFolder{ toString(QFileDialog::getExistingDirectory(this, tr("Create Project"), startPath)) };

    if (selectedFolder.IsEmpty()) // Cancelled
        return;
    else
        selectedFolder = AddTrailingSlash(selectedFolder);

    String projectFileName{ selectedFolder + FILENAME_PROJECT };

    if (QFileInfo(toQString(projectFileName)).exists())
    {
        statusBar()->showMessage("A project already exists in this folder");
        return;
    }
    else
    {
        SharedPtr<Project> newProject{ new Project{ context_ } };
        newProject->setName(selectedFolder.Split('/').Back());
        newProject->setLocation(selectedFolder);

        if (!newProject->Save() ||
            !loadProject(toQString(projectFileName)))
        {
            statusBar()->showMessage("Failed to create new project");
        }
    }
}

void Qocoon::openProject(QString filename)
{
    if (filename.isEmpty()) // Pick a project through a file dialog
    {
        const QString startPath{ (project_ ? toQString(projectLocation())
                                           : QStandardPaths::writableLocation(QStandardPaths::HomeLocation)) };

        filename = QFileDialog::getOpenFileName(nullptr, tr("Open Project"), startPath, "*.XML");
        if (filename.isEmpty()) // Cancelled
            return;
    }

    loadProject(filename);
}

bool Qocoon::loadProject(const QString& filename)
{
    const String path{ AddTrailingSlash(GetPath(toString(filename))) };

    if (project_ && project_->location() == path)
    {
        statusBar()->showMessage("Project already open");
        return false;
    }

    if (QFileInfo{ filename }.exists())
    {
        SharedPtr<XMLFile> projectFile{ GetSubsystem<ResourceCache>()->GetTempResource<XMLFile>(toString(filename)) };

        if (!projectFile.IsNull() && !projectFile->GetRoot("project").IsNull())
        {
            SharedPtr<Project> newProject{ new Project{ context_ } };
            newProject->setLocation(path);

            if (!newProject->LoadXML(projectFile->GetRoot("project")))
                goto failed;

            if (project_)
                closeProject();

            project_ = newProject;
            if (!singleWidget_)
                newScene();

            setWindowTitle(toQString(project()->name()));
            statusBar()->showMessage("Opened project " + toQString(project_->name()));
            updateRecentProjects();

            emit currentProjectChanged();
            return true;
        }
    }

    failed:
    statusBar()->showMessage("Failed to open project");
    return false;
}

bool Qocoon::saveProject()
{
    if (project_)
        return project_->Save();
    else
        return false;
}

void Qocoon::closeProject() //\\\ Should check for modified files
{
    if (!project_)
        return;

    if (!singleWidget_)
    {
        setScene(nullptr);
        runSimulation(false);
        centralStackWidget_->setCurrentIndex(0);
    }

    GetSubsystem<MaterialEditor>()->actionNew();

    project_->remove();
    project_ = nullptr;

    emit currentProjectChanged();
}

void Qocoon::addResourceFolder()
{
    const String selectedFolder{ toString(QFileDialog::getExistingDirectory(
                                              nullptr, tr("Add Resource Folder"),
                                              toQString(projectLocation()))) };

    if (selectedFolder.IsEmpty()) // Cancelled
        return;

    statusBar()->showMessage("Added resource folder " + toQString(selectedFolder));
    project_->addResourceFolder(selectedFolder);
}

String Qocoon::trimmedResourceName(const String& fileName)
{
    if (!project_->inResourceFolder(fileName))
        return "";
    else
        return fileName.Replaced(containingFolder(fileName), "");
}

String Qocoon::containingFolder(const String& path)
{
    if (project_)
    {
        for (const String& folder: project_->resourceFolders())
        {
            const String absoluteFolder{ project()->absoluteResourceFolder(folder) };
            if (path.Contains(absoluteFolder))
                return absoluteFolder;
        }
    }

    return "";
}

String Qocoon::locateResourceRoot(const String& resourcePath)
{
    if (resourcePath.IsEmpty())
        return "";

    if (project_ && project()->inResourceFolder(resourcePath))
        return containingFolder(resourcePath);

    String trail{ resourcePath };

    while (GetParentPath(trail) != trail)
    {
        String projectFile{ trail + FILENAME_PROJECT };

        if (QFileInfo(toQString(projectFile)).exists())
            return trail;

        trail = GetParentPath(trail);
    }

    return "";
}

String Qocoon::projectLocation()
{
    if (project_)
        return GetPath(project_->location());
    else
        return "";
}

void Qocoon::setScene(Scene* scene, SceneViewer* viewer)
{
    if (activeScene())
    {
        SceneUndoStack* activeStack{ sceneStack(activeScene()) };
        disconnect(activeStack, SIGNAL(canUndoChanged(bool)), this, SLOT(allowUndo(bool)));
        disconnect(activeStack, SIGNAL(canRedoChanged(bool)), this, SLOT(allowRedo(bool)));
    }

    if (scene)
    {
        scene->SetUpdateEnabled(false);
        if (runAction_->isChecked())
            runSimulation(false);

        if (!sceneStack(scene))
        {
            SceneUndoStack* undoStack{ new SceneUndoStack{ scene, this } };
            undoStacks_.push_back(undoStack);

            allowUndo(false);
            allowRedo(false);
        }

        connect(sceneStack(scene), SIGNAL(canUndoChanged(bool)), this, SLOT(allowUndo(bool)));
        connect(sceneStack(scene), SIGNAL(canRedoChanged(bool)), this, SLOT(allowRedo(bool)));
    }

    if (!viewer)
        viewer = centralView_;

    if (viewer)
    {
        viewer->setScene(scene);

        if (scene == activeScene())
            undoView_->setStack(sceneStack(scene));

        if (NodeTree* nodeTree{ GetSubsystem<NodeTree>() })
        {
            nodeTree->updateTree(false);
            nodeTree->raiseDockWidget();
        }
    }
}

void Qocoon::newScene()
{
    setScene(EMPTY_SCENE());
    centralStackWidget_->setCurrentIndex(1);
}

bool Qocoon::openScene(String filename, SceneViewer* viewer)
{
    if (filename.IsEmpty()) // Pick a scene through a file dialog
    {
        const QString startPath{ toQString(projectLocation()) };
        filename = toString(QFileDialog::getOpenFileName(nullptr, tr("Open Scene"), startPath, "XML Files (*.xml);;JSON Files (*.json)"));
        if (filename.IsEmpty()) // Cancelled
            return false;
    }

    const String extension{ GetExtension(filename) };
    ResourceCache* cache{ GetSubsystem<ResourceCache>() };
    Scene* scene{ new Scene{ context_ }};
    bool success{ false };

    if (extension == ".xml")
    {
        SharedPtr<XMLFile> sceneXML{ cache->GetTempResource<XMLFile>(filename) };
        success = scene->LoadXML(sceneXML->GetRoot("scene"));
    }
    else if (extension == ".json")
    {
        SharedPtr<JSONFile> sceneJSON{ cache->GetTempResource<JSONFile>(filename) };
        success = scene->LoadJSON(sceneJSON->GetRoot());
    }

    if (!success)
    {
        statusBar()->showMessage("Failed to open scene: " + toQString(filename));
        delete scene;
    }
    else
    {
        setScene(scene, viewer);
        sceneStack(scene)->setFilename(filename);
        statusBar()->showMessage("Opened scene: " + toQString(filename));
    }

    return success;
}

bool Qocoon::saveScene(bool saveAs)
{
    Scene* scene{ centralView_->scene() };
    if (!scene)
        return false;

    String filename{ sceneStack(scene)->filename() };
    bool success{ false };

    if (filename.IsEmpty() || saveAs) // Pick destination
    {
        Qocoon* qocoon{ GetSubsystem<Qocoon>() };
        const QString projectFolder{ toQString(qocoon->projectLocation()) };
        std::shared_ptr<QString> selectedFilter{ new QString{} };
        if (!filename.IsEmpty())
        {
            if (filename.EndsWith(".xml", false))
                selectedFilter->operator=("XML Files (*.xml)");
            else if (filename.EndsWith(".json", false))
                selectedFilter->operator=("JSON Files (*.json)");
        }

        filename = toString(QFileDialog::getSaveFileName(
                                nullptr, "Save Scene As...", projectFolder,
                                "XML Files (*.xml);;JSON Files (*.json)", selectedFilter.get()));

        if (filename.IsEmpty())
            return false;

        const int asterisk{ selectedFilter->lastIndexOf('*') + 1 };
        const String extension{ toString(selectedFilter->mid(asterisk, selectedFilter->lastIndexOf(')') - asterisk)) };
        if (!filename.EndsWith(extension))
            filename.Append(extension);
    }

    const String extension{ GetExtension(filename) };
    if (extension == ".xml")
    {
        SharedPtr<XMLFile> sceneXML{ new XMLFile{ context_ } };
        XMLElement root{ sceneXML->CreateRoot("scene") };
        success = scene->SaveXML(root) && sceneXML->SaveFile(filename);

    }
    else if (extension == ".json")
    {
        SharedPtr<JSONFile> sceneJSON{ new JSONFile{ context_ } };
        success = scene->SaveJSON(sceneJSON->GetRoot()) && sceneJSON->SaveFile(filename);
    }

    if (success)
    {
        statusBar()->showMessage("Saved scene: " + toQString(filename));
        sceneStack(scene)->setFilename(filename);
    }
    else
    {
        statusBar()->showMessage("Failed to save scene: " + toQString(filename));
    }

    return success;
}

void Qocoon::closeScene(SceneViewer* viewer)
{
    if (!viewer)
        viewer = centralView_;

    if (viewer)
    {
        if (!viewer->scene())
            return;

        viewer->setScene(nullptr);
        viewer->repaint();
        undoView_->setStack(nullptr);

        if (NodeTree* nodeTree{ GetSubsystem<NodeTree>() })
            nodeTree->updateTree(false);
    }
}

QMenu* Qocoon::createAddMenu()
{
    addMenu_ = new QMenu{ "Add" };
    QAction* addNodeAction{ new QAction{ QIcon{ ":/Node" }, "Node", addMenu_ } };
    addNodeAction->setObjectName("Node");
    connect(addNodeAction, SIGNAL(triggered(bool)), SLOT(addNodeOrComponent()));
    addMenu_->addAction(addNodeAction);

    const HashMap<String, Vector<StringHash> >& categories{ context_->GetObjectCategories() };
    const HashMap<StringHash, SharedPtr<ObjectFactory> >& factories{ context_->GetObjectFactories() };

    for(const String& categoryName: categories.Keys())
    {
        if (categoryName == "UI")
            continue;

        QMenu* const menu{ addMenu_->addMenu(toQString(categoryName)) };
        const Vector<StringHash>& factoryHashes{ *categories[categoryName] };

        for (StringHash factoryHash: factoryHashes)
        {
            QString iconName{ ":/Component" };
            const QString componentName{ toQString(factories[factoryHash]->Get()->GetTypeName()) };
            const QFileInfo iconCheck{ ":/" + componentName };

            if (iconCheck.exists())
                iconName = iconCheck.filePath();

            QAction* addComponentAction{ new QAction{ QIcon{ iconName }, componentName, this } };
            addComponentAction->setObjectName(componentName);
            connect(addComponentAction, SIGNAL(triggered(bool)), this, SLOT(addNodeOrComponent()));
            menu->addAction(addComponentAction);
        }
    }

    prefabMenu_ = new QMenu{ "Prefabs" };
    updatePrefabMenu();
    addMenu_->addMenu(prefabMenu_);

    return addMenu_;
}

void Qocoon::updatePrefabMenu(const QStringList& prefabs)
{
    if (!prefabMenu_)
        return;

    prefabMenu_->clear();

    if (prefabs.isEmpty())
    {
        QAction* noPrefabAction{ new QAction{ "No prefabs found" } };
        noPrefabAction->setEnabled(false);
        prefabMenu_->addAction(noPrefabAction);
    }
    else for (const QString& prefab: prefabs)
    {
        const int slash{ prefab.lastIndexOf('/') + 1 };
        const QString prefabName{ prefab.mid(slash, prefab.lastIndexOf('.') - slash) };
        QAction* prefabAction{ new QAction{ prefabName } };
        prefabAction->setObjectName(prefab);
        connect(prefabAction, SIGNAL(triggered(bool)), SLOT(addPrefabAction()));
        prefabMenu_->addAction(prefabAction);
    }
}

Node* Qocoon::activeNode()
{
    Selector* selector{ GetSubsystem<Selector>() };
    Object* activeObject{ selector->activeObject() };

    if (!activeObject)
        return static_cast<Node*>(activeScene());
    else if (activeObject->IsInstanceOf<Component>())
        return static_cast<Component*>(activeObject)->GetNode();
    else
        return static_cast<Node*>(activeObject);
}

void Qocoon::addNodeOrComponent()
{
    const String componentName{ toString(sender()->objectName()) };
    Node* parent{ activeNode() };

    if (parent)
    {
        if (componentName == "Node")
            addNode(parent);
        else
            addComponent(parent, componentName);
    }
}

void Qocoon::addPrefabAction()
{
    const String prefabFilename{ toString(sender()->objectName()) };
    Node* parent{ activeNode() };

    if (Node* prefabNode{ addPrefabToNode(parent, prefabFilename) })
        GetSubsystem<Selector>()->select(prefabNode, false);
}

Node* Qocoon::addPrefabToNode(Node* parent, const String& prefabFilename, const Matrix3x4& transform)
{
    if (parent && !prefabFilename.IsEmpty())
    {
        const unsigned slash{ prefabFilename.FindLast('/') + 1 };
        const unsigned period{ prefabFilename.FindLast('.') };
        const QString prefabName{ toQString(prefabFilename.Substring(slash, period - slash)) };

        ResourceCache* cache{ GetSubsystem<ResourceCache>() };
        SharedPtr<Node> prefabNode{ context_->CreateObject<Node>() };

        bool success{ false };
        if (prefabFilename.EndsWith(".xml", false))
        {
            SharedPtr<XMLFile> prefabXML{ cache->GetTempResource<XMLFile>(prefabFilename) };
            success = prefabNode->LoadXML(prefabXML->GetRoot());
        }
        else if (prefabFilename.EndsWith(".json", false))
        {
            SharedPtr<JSONFile> prefabXML{ cache->GetTempResource<JSONFile>(prefabFilename) };
            success = prefabNode->LoadJSON(prefabXML->GetRoot());
        }

        if (success)
        {
            prefabNode->SetTransform(transform);
            if (prefabNode->GetName().IsEmpty())
                prefabNode->SetName(toString(prefabName));

            const unsigned nodeID{ parent->GetScene()->GetFreeNodeID(REPLICATED) };
            HierarchyCommand* hierarchyModification{ new HierarchyCommand{ prefabNode } };
            hierarchyModification->setID(nodeID);
            hierarchyModification->setScene(activeScene());
            hierarchyModification->setNewParent(parent->GetID());
            hierarchyModification->setText("Add " + prefabName + " prefab");

            SceneUndoStack* undoStack{ sceneStack(activeScene()) };
            undoStack->push(hierarchyModification);

            GetSubsystem<NodeTree>()->updateTree(true);

            return hierarchyModification->node();
        }
    }

    return nullptr;
}

void Qocoon::applyPrefab(PODVector<Node*> nodes, const String& prefabFilename)
{
    if (nodes.IsEmpty() ||
        (nodes.Size() == 1 && nodes.Front()->IsInstanceOf<Scene>()))
        return;

    SceneUndoStack* undoStack{ sceneStack(activeScene()) };
    HashSet<Object*> prefabs{};

    undoStack->beginMacro("Apply prefab");
    for (Node* n: nodes)
    {
        if (n->GetScene() == n)
            continue;

        if (Node* prefabNode{ addPrefabToNode(n->GetParent(), prefabFilename, n->GetTransform()) })
        {
            prefabs.Insert(prefabNode);
            removeObject(n);
        }
    }
    undoStack->endMacro();

    if (!prefabs.IsEmpty())
        GetSubsystem<Selector>()->select(prefabs, false);
}

Node* Qocoon::addNode(Node* parent, XMLFile* data)
{
    SharedPtr<Node> node{ context_->CreateObject<Node>() };
    HierarchyCommand* hierarchyModification{ new HierarchyCommand{ node } };
    hierarchyModification->setID(parent->GetScene()->GetFreeNodeID(REPLICATED));
    hierarchyModification->setScene(activeScene());
    hierarchyModification->setNewParent(parent->GetID());

    if (data)
        hierarchyModification->setData(data);

    sceneStack(activeScene())->push(hierarchyModification);
    return hierarchyModification->node();
}

SharedPtr<Component> Qocoon::addComponent(Node* node, StringHash componentType, XMLFile* data)
{
    SharedPtr<Component> component{ context_->CreateObject(componentType)->Cast<Component>() };
    HierarchyCommand* hierarchyModification{ new HierarchyCommand{ component } };
    hierarchyModification->setID(activeScene()->GetFreeComponentID(REPLICATED));
    hierarchyModification->setScene(activeScene());
    hierarchyModification->setNewParent(node->GetID());

    if (data)
        hierarchyModification->setData(data);

    sceneStack(activeScene())->push(hierarchyModification);
    return component;
}

void Qocoon::reparent(const PODVector<Animatable*>& animatables, Node* target)
{
    if (!target)
        return;

    SceneUndoStack* undoStack{ sceneStack(activeScene()) };
    QString macroName{ "Reparent " };
    const PODVector<Animatable*> unconflicted{ filterConflicting(animatables, target) };
    bool inMacro{ false };

    if (unconflicted.Size() > 1u)
    {
        completeMacroName(unconflicted, macroName);
        undoStack->beginMacro(macroName);
        inMacro = true;
    }

    for (Animatable* animatable: unconflicted)
    {
        HierarchyCommand* hierarchyModification{ new HierarchyCommand{ animatable } };
        const bool isComponent{ animatable->IsInstanceOf<Component>() };
        const unsigned id{ (isComponent ? static_cast<Component*>(animatable)->GetID()
                                        : static_cast<Node*>(animatable)->GetID()) };
        hierarchyModification->setID(id);
        hierarchyModification->setNewParent(target->GetID());
        hierarchyModification->setScene(activeScene());
        hierarchyModification->setText("Reparent " + toQString(animatable->GetTypeName()));
        if (hierarchyModification->isChange())
        {
            Node* node{ nullptr };
            Matrix3x4 oldLocalTransform{};
            Matrix3x4 oldWorldTransform{};
            if (!isComponent)
            {
                node = static_cast<Node*>(animatable);
                oldLocalTransform = node->GetTransform();
                oldWorldTransform = node->GetWorldTransform();

                if (!inMacro)
                {
                    undoStack->beginMacro(hierarchyModification->text());
                    inMacro = true;
                }
            }

            undoStack->push(hierarchyModification);

            if (!isComponent)
            {
                node = hierarchyModification->node();
                node->SetWorldTransform(oldWorldTransform);

                int a{ 0 };
                for (const String attribute: { "Position", "Rotation", "Scale" })
                {
                    AttributeCommand* transformCommand{ new AttributeCommand{ node, attribute } };

                    switch(a)
                    {
                    case 0: transformCommand->setOldValue(oldLocalTransform.Translation()); break;
                    case 1: transformCommand->setOldValue(oldLocalTransform.Rotation());    break;
                    case 2: transformCommand->setOldValue(oldLocalTransform.Scale());       break;
                    }

                    if (transformCommand->isChange())
                        undoStack->push(transformCommand);
                    else
                        delete transformCommand;

                    ++a;
                }
            }
        }
        else
        {
            delete hierarchyModification;
        }
    }

    if (inMacro)
        undoStack->endMacro();
}

void Qocoon::removeObjects(const PODVector<Animatable*>& animatables)
{
    /// BUG: Undo puts back an extra PhysicsWorld when removed together with RigidBody or CollisionShape

    SceneUndoStack* undoStack{ sceneStack(activeScene()) };
    QString macroName{ "Remove " };
    const PODVector<Animatable*> unconflicted{ filterConflicting(animatables) };

    if (unconflicted.Size() > 1u)
    {
        completeMacroName(unconflicted, macroName);
        undoStack->beginMacro(macroName);
    }

    for (Animatable* animatable: unconflicted)
        removeObject(animatable);

    if (unconflicted.Size() > 1u)
        undoStack->endMacro();
}

void Qocoon::removeObject(Animatable* animatable)
{
    SceneUndoStack* undoStack{ sceneStack(activeScene()) };
    HierarchyCommand* hierarchyModification{ new HierarchyCommand{ animatable } };
    const bool isComponent{ animatable->IsInstanceOf<Component>() };
    const unsigned id{ (isComponent ? static_cast<Component*>(animatable)->GetID()
                                    : static_cast<Node*>(animatable)->GetID()) };
    hierarchyModification->setID(id);
    hierarchyModification->setScene(activeScene());
    hierarchyModification->setText("Remove " + toQString(animatable->GetTypeName()));
    undoStack->push(hierarchyModification);
}

PODVector<Node*> Qocoon::filterConflicting(const PODVector<Node*>& nodes, Node* target) const
{
    PODVector<Animatable*> animatables{};
    for (Node* n: nodes)
        animatables.Push(static_cast<Animatable*>(n));
    animatables = filterConflicting(animatables, target);

    PODVector<Node*> res{};
    for (Animatable* a: animatables)
        res.Push(static_cast<Node*>(a));
    return res;
}

PODVector<Animatable*> Qocoon::filterConflicting(const PODVector<Animatable*>& animatables, Node* target) const
{
    PODVector<Animatable*> unconflicted{};

    for (Animatable* animatable: animatables)
    {
        bool conflict{ false };
        const bool isNode{ animatable->GetTypeInfo()->IsTypeOf<Node>() };
        Node* parent{ (isNode ? static_cast<Node*>(animatable)->GetParent()
                              : static_cast<Component*>(animatable)->GetNode()) };

        if (target && target == parent)
        {
            conflict = true;
        }
        else while (parent)
        {
            if (animatables.Contains(parent))
            {
                conflict = true;
                break;
            }

            parent = parent->GetParent();
        }

        if (!conflict)
            unconflicted.Push(animatable);
    }

    return unconflicted;
}

SceneUndoStack* Qocoon::sceneStack(Scene* scene)
{
    if (!scene)
        return nullptr;

    for (SceneUndoStack* stack: undoStacks_)
        if (stack->scene() == scene)
            return stack;

    return nullptr;
}

void Qocoon::toggleFullView(bool toggled)
{
    QSettings settings{};

    if (toggled)
    {
        settings.setValue("geometry", saveGeometry());
        settings.setValue("state", saveState());

        for (QDockWidget* dock: findChildren<QDockWidget*>()) {
            if (dock->isVisible()) {
                dock->setVisible(false);
            }
        }

        statusBar()->setVisible(false);
        menuBar()->setVisible(false);
        mainToolBar_->setVisible(false);

        if (!isFullScreen())
            showFullScreen();

        fullViewAction_->setShortcuts({ QKeySequence("F11"), QKeySequence("Esc") });
    }
    else
    {
        statusBar()->setVisible(true);
        menuBar()->setVisible(true);

        if (isFullScreen())
            showMaximized();

        restoreGeometry(settings.value("geometry").toByteArray());
        restoreState(settings.value("state").toByteArray());

        fullViewAction_->setShortcuts({ QKeySequence("F11") });
    }
}

void Qocoon::runSimulation(bool start)
{
    SceneViewer* viewer{ SceneViewer::sceneViewers_.front() };
    if (!viewer || viewer->isTransforming())
    {
        runAction_->setChecked(false);
        return;
    }

    if (start)
    {
        viewer->setContinuousUpdate(true);

        if (!runAction_->isChecked())
        {
            runAction_->blockSignals(true);
            runAction_->setChecked(true);
            runAction_->blockSignals(false);
        }
    }
    else
    {
        viewer->setContinuousUpdate(false);

        if (runAction_->isChecked())
        {
            runAction_->blockSignals(true);
            runAction_->setChecked(false);
            runAction_->blockSignals(false);
        }
    }

    updateAvailableActions();
}

void Qocoon::resizeEvent(QResizeEvent* event)
{
    if (static_cast<double>(event->size().width()) / event->size().height() > 1.0) {

        setCorner(Qt::BottomRightCorner, Qt::RightDockWidgetArea);


    } else {

        setCorner(Qt::BottomRightCorner, Qt::BottomDockWidgetArea);
    }
}

void Qocoon::setMode(EditMode mode)
{
    if (mode == mode_)
        return;

    mode_ = mode;

    for (EditMode m: modeActions_.keys())
        modeActions_[m]->setChecked(m == mode_);
}

void Qocoon::changeMode(bool toggled)
{
    if (toggled)
        setMode(static_cast<EditMode>(sender()->property("mode").toInt()));
    else
        static_cast<QAction*>(sender())->setChecked(true);
}

QString Qocoon::modeToString(EditMode mode)
{
    switch (mode)
    {
    case EM_Standard: return "Standard"; break;
    case EM_Block:    return "Block"; break;
    case EM_Terrain:  return "Terrain"; break;
    default:          return ""; break;
    }
}

void Qocoon::about()
{
    QString aboutText{ tr("<p>Copyleft 🄯 2024 <a href=\"https://luckey.games\">LucKey Productions</a></b>"
                          "<p>You may use and redistribute this software under the terms "
                          "of the<br><a href=\"https://www.gnu.org/licenses/gpl.html\">"
                          "GNU General Public License Version 3</a>.</p>") };

    QDialog* aboutBox{ new QDialog(this) };

    aboutBox->setWindowTitle("About " + Weaver::applicationDisplayName());
    QVBoxLayout* aboutLayout{ new QVBoxLayout() };
    aboutLayout->setContentsMargins(0, 8, 0, 4);

    QPushButton* manawargButton{ new QPushButton() };
    QPixmap manawargLogo{ ":/Logo" };
    manawargButton->setIcon(manawargLogo);
    manawargButton->setFlat(true);
    manawargButton->setMinimumSize(manawargLogo.width(), manawargLogo.height());
    manawargButton->setIconSize(manawargLogo.size());
    manawargButton->setToolTip("https://gitlab.com/luckeyproductions/tools/manawarg");
    manawargButton->setCursor(Qt::CursorShape::PointingHandCursor);
    aboutLayout->addWidget(manawargButton);
    aboutLayout->setAlignment(manawargButton, Qt::AlignHCenter);
    connect(manawargButton, SIGNAL(clicked(bool)), this, SLOT(openUrl()));

    QLabel* aboutLabel{ new QLabel(aboutText) };
    aboutLabel->setWordWrap(true);
    aboutLabel->setAlignment(Qt::AlignJustify);
    QVBoxLayout* labelLayout{ new QVBoxLayout() };
    labelLayout->setContentsMargins(42, 34, 42, 12);
    labelLayout->addWidget(aboutLabel);
    aboutLayout->addLayout(labelLayout);

    QDialogButtonBox* buttonBox{ new QDialogButtonBox(QDialogButtonBox::Ok, aboutBox) };
    connect(buttonBox, SIGNAL(accepted()), aboutBox, SLOT(accept()));
    aboutLayout->addWidget(buttonBox);

    aboutBox->setLayout(aboutLayout);
    aboutBox->resize(aboutBox->minimumSize());
    aboutBox->exec();
}

void Qocoon::openUrl()
{
    QDesktopServices::openUrl(QUrl(qobject_cast<QPushButton*>(sender())->toolTip()));
}

void Qocoon::undo()
{
    if (centralView_->scene())
        sceneStack(centralView_->scene())->undo();
}

void Qocoon::redo()
{
    if (centralView_->scene())
        sceneStack(centralView_->scene())->redo();
}

void Qocoon::actionDelete()
{
    removeObjects(GetSubsystem<NodeTree>()->deletableAnimatables());
}
