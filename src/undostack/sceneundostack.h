/* ManaWarg
// Copyright (C) 2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef SCENEUNDOSTACK_H
#define SCENEUNDOSTACK_H

#include <QUndoStack>

#include "../dry.h"

class SceneUndoStack: public QUndoStack, public Object
{
    Q_OBJECT
    DRY_OBJECT(SceneUndoStack, Object)

public:
    SceneUndoStack(Scene* scene, QObject* parent);

    Scene* scene() const { return scene_; }
    void setFilename(const String& filename) { filename_ = filename; }
    String filename() const { return filename_; }

private slots:
    void updateSceneViewsAndValueFields();

private:
    Scene* scene_;
    String filename_;
};

#endif // SCENEUNDOSTACK_H
