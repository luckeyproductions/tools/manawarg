/* ManaWarg
// Copyright (C) 2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../propertieseditor.h"

#include "attributecommand.h"
#include "../utility.h"

AttributeCommand::AttributeCommand(Serializable* serializable, String attributeName, QUndoCommand* parent): QUndoCommand(parent),
    info_{ serializable },
    attributeName_{ attributeName }
{
    newValue_ = oldValue_ = object()->GetAttribute(attributeName_);
    setText(toQString(object()->GetTypeName() + " " + attributeName_));
}

void AttributeCommand::redo()
{
    if (!isChange())
        return;

    PropertiesWidget* propertiesWidget{ object()->GetSubsystem<PropertiesEditor>()->widget() };
    bool editorRelevance{ propertiesWidget && propertiesWidget->serializable() == object() };
    int oldCount;

    if (editorRelevance)
        oldCount = propertiesWidget->countSubAttributes();

    object()->SetAttribute(attributeName_, newValue_);

    if (editorRelevance && oldCount != propertiesWidget->countSubAttributes())
       propertiesWidget->handleNumSubAttributesChanged();
}

void AttributeCommand::undo()
{
    if (!isChange())
        return;

    PropertiesWidget* propertiesWidget{ object()->GetSubsystem<PropertiesEditor>()->widget() };
    bool editorRelevance{ propertiesWidget && propertiesWidget->serializable() == object() };
    int oldCount;

    if (editorRelevance)
        oldCount = propertiesWidget->countSubAttributes();

    object()->SetAttribute(attributeName_, oldValue_);

    if (editorRelevance && oldCount != propertiesWidget->countSubAttributes())
       propertiesWidget->handleNumSubAttributesChanged();
}

void AttributeCommand::setNewValue(const Variant& value)
{
    if (value.GetType() == newValue_.GetType() ||
        value.GetResourceRef().type_ == newValue_.GetResourceRef().type_ ||
        value.GetResourceRefList().type_ == newValue_.GetResourceRefList().type_)
        newValue_ = value;

    if (newValue_.GetType() == VAR_BOOL)
        setText(toQString((newValue_.GetBool() ? "Enable " : "Disable ") +
                object()->GetTypeName() +
                          (attributeName_ == "Is Enabled" ? "" : " " + attributeName_)));
}

Serializable* AttributeCommand::object() const
{
    if (info_.node_)
        return info_.scene_->GetNode(info_.id_);
    else
        return info_.scene_->GetComponent(info_.id_);
}

