/* ManaWarg
// Copyright (C) 2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#include "transformcommand.h"


TransformCommand::TransformCommand(Node* node): QUndoCommand(),
    node_{ node },
    oldTransform_{ node->GetTransform() },
    newTransform_{ oldTransform_ },
    positionModification_{ new AttributeCommand(node, "Position", this) },
    rotationModification_{ new AttributeCommand(node, "Rotation", this) },
    scaleModification_{    new AttributeCommand(node, "Scale",    this) }
{
    setText("Node");
}

//void TransformOperation::undo()
//{
//    node_->SetTransform(oldTransform_);
//}

//void TransformOperation::redo()
//{
//    node_->SetTransform(newTransform_);
//}

void TransformCommand::translate(const Vector3& translation)
{
    if (!text().contains( "Tr"))
        setText(text() + " Translation");

    newTransform_ = Matrix3x4(newTransform_.Translation() + translation,
                              newTransform_.Rotation(),
                              newTransform_.Scale());

    positionModification_->setNewValue(newTransform_.Translation());
}

void TransformCommand::rotate(const Quaternion& rotation)
{
    if (!text().contains( "Ro"))
        setText(text() + " Rotation");

    newTransform_ = Matrix3x4(newTransform_.Translation(),
                              newTransform_.Rotation() * rotation,
                              newTransform_.Scale());

    rotationModification_->setNewValue(newTransform_.Rotation());
}

void TransformCommand::scale(const Vector3& scale)
{
    if (!text().contains( "Sc"))
        setText(text() + " Scaling");

    newTransform_ = Matrix3x4(newTransform_.Translation(),
                              newTransform_.Rotation(),
                              newTransform_.Scale() * scale);

    scaleModification_->setNewValue(newTransform_.Scale());
}
