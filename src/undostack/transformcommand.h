/* ManaWarg
// Copyright (C) 2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#ifndef TRANSFORMCOMMAND_H
#define TRANSFORMCOMMAND_H

#include "attributecommand.h"
#include "../dry.h"

class TransformCommand: public QUndoCommand
{
public:
    explicit TransformCommand(Node* node);

//    void undo() override;
//    void redo() override;

    void translate( const Vector3&    translation);
    void rotate(    const Quaternion& rotation);
    void scale(     const Vector3&    scale);

private:
    Node* node_;
    const Matrix3x4 oldTransform_;
          Matrix3x4 newTransform_;

    AttributeCommand* positionModification_;
    AttributeCommand* rotationModification_;
    AttributeCommand* scaleModification_;
};

#endif // TRANSFORMCOMMAND_H
