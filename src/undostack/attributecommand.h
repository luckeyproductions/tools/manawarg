/* ManaWarg
// Copyright (C) 2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef ATTRIBUTEMODIFICATION_H
#define ATTRIBUTEMODIFICATION_H

#include <QUndoCommand>
#include "../dry.h"

struct ObjectInfo
{
    ObjectInfo(Serializable* object)
    {
        if (object->IsInstanceOf<Node>())
        {
            Node* node{ static_cast<Node*>(object) };
            scene_ = node->GetScene();
            id_ = node->GetID();
            node_ = true;
        }
        else
        {
            Component* component{ static_cast<Component*>(object) };
            scene_ = component->GetScene();
            id_ = component->GetID();
            node_ = false;
        }
    }

    Scene* scene_;
    unsigned id_;
    bool node_;
};

class AttributeCommand: public QUndoCommand
{
public:
    AttributeCommand(Serializable* serializable, String attributeName, QUndoCommand* parent = nullptr);

    void redo() override;
    void undo() override;

    void setOldValue(const Variant& value) { oldValue_ = value; }
    void setNewValue(const Variant& value);
    bool isChange() const { return newValue_ != oldValue_; }

private:
    Serializable* object() const;

    const ObjectInfo info_;
    const String attributeName_;
    Variant oldValue_;
    Variant newValue_;
};

#endif // ATTRIBUTEMODIFICATION_H
