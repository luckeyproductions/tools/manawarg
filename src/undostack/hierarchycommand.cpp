/* ManaWarg
// Copyright (C) 2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../nodetree.h"
#include "../utility.h"

#include "hierarchycommand.h"

HierarchyCommand::HierarchyCommand(Animatable* animatable, QUndoCommand* parent): QUndoCommand(parent),
    animatable_{ animatable },
    context_{ animatable->GetContext() },
    scene_{ nullptr },
    xmlFile_{ new XMLFile{ context_ } },
    id_{ 0u },
    oldParent_{ 0u },
    newParent_{ 0u },
    hadEventsBlocked_{ false },
    mode_{ REPLICATED }
{
    oldParent_ = parentID();

    if (oldParent_)
        hadEventsBlocked_ = animatable_->GetBlockEvents();

    if (isNode())
    {
        XMLElement rootElem{ xmlFile_->CreateRoot("node") };
        animatable_->SaveXML(rootElem);
        mode_ = (node()->IsReplicated() ? REPLICATED : LOCAL);
    }
    else
    {
        XMLElement rootElem{ xmlFile_->CreateRoot("component") };
        animatable_->SaveXML(rootElem);
        mode_ = (component()->IsReplicated() ? REPLICATED : LOCAL);
    }
}

void HierarchyCommand::redo()
{
    Selector* selector{ context_->GetSubsystem<Selector>() };
    NodeTree* nodeTree{ context_->GetSubsystem<NodeTree>() };
    const bool selected{ selector->selection().Contains(animatable_) };

    if (newParent_)
    {
        if (oldParent_)
            remove();

        XMLElement rootElem{ xmlFile_->GetRoot() };
        Node* np{ scene_->GetNode(newParent_) };
        if (isNode())
        {
            animatable_ = np->CreateChild(id_, mode_);
            node()->SetID(id_);
        }
        else
        {
            animatable_ = np->CreateComponent(rootElem.GetAttribute("type"), mode_, id_);
        }

        if (animatable_->LoadXML(rootElem))
        {
            animatable_->ApplyAttributes();
            nodeTree->updateTree(true);

            if (!oldParent_|| selected)
                selector->select(animatable_, selected);
        }

        updatePointer();
    }
    else
    {
        remove();
        nodeTree->updateTree(true);
    }
}

void HierarchyCommand::undo()
{
    NodeTree* nodeTree{ context_->GetSubsystem<NodeTree>() };
    Selector* selector{ context_->GetSubsystem<Selector>() };
    const bool selected{ selector->selection().Contains(animatable_) };

    if (oldParent_)
    {
        if (newParent_)
            remove();

        XMLElement rootElem{ xmlFile_->GetRoot() };
        Node* op{ scene_->GetNode(oldParent_) };
        if (isNode())
        {
            animatable_ = op->CreateChild(id_, mode_);
            node()->SetID(id_);
        }
        else
        {
            animatable_ = op->CreateComponent(rootElem.GetAttribute("type"), mode_, id_);
        }

        if (animatable_->LoadXML(rootElem))
        {
            animatable_->ApplyAttributes();
            nodeTree->updateTree(true);

            if (!oldParent_|| selected)
                selector->select(animatable_, selected);
        }

        updatePointer();
    }
    else
    {
        remove();
        nodeTree->updateTree(true);
    }
}

void HierarchyCommand::setNewParent(unsigned id)
{
    newParent_ = id;

    const QString typeName{ toQString(animatable_->GetTypeName()) };

    if (!oldParent_)
        setText("Add " + typeName);
    else if (!newParent_)
        setText("Remove " + typeName);
    else
        setText("Reparent " + typeName);

}

void HierarchyCommand::setScene(Scene* scene)
{
    scene_ = scene;
}

void HierarchyCommand::updatePointer()
{
    if (isNode())
        animatable_ = scene_->GetNode(id_);
    else
        animatable_ = scene_->GetComponent(id_);
}

void HierarchyCommand::remove()
{
    updatePointer();
    context_->GetSubsystem<Selector>()->deselect(animatable_);

    if (Node* p{ parent() })
    {
        if (isNode())
            p->RemoveChild(node());
        else
            p->RemoveComponent(component());
    }
}
