/* ManaWarg
// Copyright (C) 2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef HIERARCHYCOMMAND_H
#define HIERARCHYCOMMAND_H

#include <QUndoCommand>
#include "../dry.h"

class HierarchyCommand: public QUndoCommand
{
public:
    HierarchyCommand(Animatable* animatable, QUndoCommand* parent = nullptr);

    void redo() override;
    void undo() override;

    void setID(unsigned id) { id_ = id; }
    void setNewParent(unsigned id);
    void setScene(Scene* scene);
    void setData(XMLFile* data)
    {
        xmlFile_ = data;
        oldParent_ = 0u;
    }

    bool isChange() const { return oldParent_ != newParent_; }

    Node* node() const
    {
        return static_cast<Node*>(animatable_.Get());
    }

private:
    void updatePointer();
    void remove();

    bool isNode() const
    {
        return animatable_->IsInstanceOf<Node>();
    }


    Component* component() const
    {
        return static_cast<Component*>(animatable_.Get());
    }

    int parentID() const
    {
        if (!parent())
            return 0u;
        else
            return parent()->GetID();
    }

    Node* parent() const
    {
        if (!animatable_)
            return 0u;
        else if (isNode())
            return node()->GetParent();
        else
            return component()->GetNode();
    }

    SharedPtr<Animatable> animatable_;
    Context* context_;
    Scene* scene_;
    SharedPtr<XMLFile> xmlFile_;
    unsigned id_;
    unsigned oldParent_;
    unsigned newParent_;
    bool hadEventsBlocked_;
    CreateMode mode_;
};

#endif // HIERARCHYCOMMAND_H
