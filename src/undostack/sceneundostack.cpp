/* ManaWarg
// Copyright (C) 2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../sceneviewer.h"
#include "../propertieseditor.h"
#include "../nodetree.h"

#include "sceneundostack.h"

SceneUndoStack::SceneUndoStack(Scene* scene, QObject* parent): QUndoStack(parent), Object(scene->GetContext()),
    scene_{ scene },
    filename_{}
{
    connect(this, SIGNAL(indexChanged(int)), this, SLOT(updateSceneViewsAndValueFields()));
}

void SceneUndoStack::updateSceneViewsAndValueFields()
{
    for (SceneViewer* viewer: SceneViewer::sceneViewers_)
    {
        if (scene_ == viewer->scene())
        {
            viewer->updateAstralScene();
            viewer->updateView();
        }
    }

    if (PropertiesEditor* propertiesEditor{ GetSubsystem<PropertiesEditor>() })
    {
        if (scene_ == propertiesEditor->scene())
            propertiesEditor->updateFields();
    }

//    if (NodeTree* nodeTree = GetSubsystem<NodeTree>())
//        if (scene_ == nodeTree->scene())
//            nodeTree->updateTree();
}
