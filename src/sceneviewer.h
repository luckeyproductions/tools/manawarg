/* ManaWarg
// Copyright (C) 2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef SCENEVIEWER_H
#define SCENEVIEWER_H

#include "selector.h"

#include "view3d.h"

class QRubberBand;
class Transformer;

class SceneViewer: public View3D
{
    Q_OBJECT
    DRY_OBJECT(SceneViewer, View3D)

public:
    static std::vector<SceneViewer*> sceneViewers_;

    SceneViewer(Context* context, QWidget* parent = nullptr);

    void setScene(Scene* scene) override;

    void setContinuousUpdate(bool enabled) override;
    void storeScene();
    void restoreScene();

    bool isTransforming() const;


public slots:
    void updateView() override;
    void updateAstralScene();
    void updateAstralIcons();

signals:
    void sceneChanged();

protected:
    void updateViewport() override;

    void mousePressEvent(QMouseEvent* event) override;
    void mouseMoveEvent(QMouseEvent* event) override;
    void mouseReleaseEvent(QMouseEvent* event) override;
    void wheelEvent(QWheelEvent* event) override;

    void dragEnterEvent(QDragEnterEvent* event) override;
    void dropEvent(QDropEvent* event) override;

private slots:
    void selectionChanged();
    void showAddMenu();

private:
    void createAstralscene();
    void updateAstralJib();
    void updateAstralModels();
    Node* addAstralStaticModel(Node* node, Model* model);
    void addAstralCollisionshape(CollisionShape* collisionShape);
    void clearAstral();

    Object* sceneRaycastFirst(Qt::KeyboardModifiers modifiers);
    void updateSelection(bool append);

    Scene* astralScene_;
    Jib* astralJib_;
    HashMap<StringHash, BillboardSet*> astralIcons_;
    Vector<Node*> astralModelNodes_;
    Vector<Component*> astrallyRepresented_;
    SharedPtr<Material> glowWire_;
    SharedPtr<XMLFile> revertData_;
    QPoint clickStart_;
    QRubberBand* selectionRect_;
    Transformer* transformer_;
};

#endif // SCENEVIEWER_H
