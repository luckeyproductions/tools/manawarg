/* ManaWarg
// Copyright (C) 2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef TEXTUREWIDGET_H
#define TEXTUREWIDGET_H

#include <QPushButton>

#include "drywidget.h"

static const std::vector<String> textureUnitNames {

    "diffuse",
    "normal",
    "specular",
    "emissive",
    "environment",
    "volume",
    "custom1",
    "custom2",
    "lightramp",
    "lightshape",
    "shadowmap",
    "faceselect",
    "indirection",
    "depth",
    "light",
    "zone"
};

class TextureWidget: public DryWidget
{
    Q_OBJECT
    DRY_OBJECT(TextureWidget, DryWidget)

public:
    explicit TextureWidget(TextureUnit textureUnit = TU_DIFFUSE, QWidget* parent = nullptr);

    void setTexture(Texture* texture);
    Texture* texture() { return texture_; }
    TextureUnit textureUnit() { return textureUnit_; }

public slots:
    void updateTextureButton();

signals:
    void textureChanged();

protected:
    void resizeEvent(QResizeEvent* event) override;
    void dragEnterEvent(QDragEnterEvent* event) override;
    void dropEvent(QDropEvent* event) override;

private slots:
    void pickTexture();

private:
    QPushButton* textureButton_;
    Texture* texture_;
    TextureUnit textureUnit_;
};

#endif // TEXTUREWIDGET_H
