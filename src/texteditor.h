/* ManaWarg
// Copyright (C) 2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef TEXTEDITOR_H
#define TEXTEDITOR_H

#include <QTextEdit>

#include "filewidget.h"

class TextEditor: public FileWidget
{
    Q_OBJECT
    DRY_OBJECT(TextEditor, DryWidget)

public:
    explicit TextEditor(Context* context, QWidget* parent = nullptr);

    void setOrientation(Qt::Orientation orientation) override;

    void openFile(const String& fileName);

protected:
    void createToolBar() override;

    void dragEnterEvent(QDragEnterEvent* event) override;
    void dropEvent(QDropEvent* event) override;

protected slots:
    void actionNew() override;
    void actionOpen() override;
    void actionSave(bool SaveAs = false) override;

private slots:
    void lineWrapToggled(bool toggled);
    void runActionToggled(bool start);
    void actionComment();

private:
    QVBoxLayout* mainLayout_;
    QTextEdit* textEdit_;
    QAction* lineWrapAction_;
    QAction* runAction_;
    QAction* commentAction_;

    Node* tempScriptNode_;
    SharedPtr<ScriptFile> tempScriptFile_;
    File* file_;
};

#endif // TEXTEDITOR_H
