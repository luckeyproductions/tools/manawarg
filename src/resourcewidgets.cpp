/* ManaWarg
// Copyright (C) 2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QDragEnterEvent>
#include <QDropEvent>
#include "qocoon.h"

#include "resourcewidgets.h"

ResourceLineEdit::ResourceLineEdit(StringHash type, QWidget* parent): QLineEdit(parent),
    resourceType_{ type }
{}

void ResourceLineEdit::dragEnterEvent(QDragEnterEvent* event)
{
    const unsigned fileType{ extractDataMap(event->mimeData())[DryTypeHash].toUInt() };

    if (fileType == resourceType_.ToHash())
        event->acceptProposedAction();
}

void ResourceLineEdit::dropEvent(QDropEvent* event)
{
    const QMimeData* mime{ event->mimeData() };

    const String fileName{ toString(extractDataMap(mime)[FileName].toString()) };
    const String attributeName{ toString(objectName()) };

    if (!fileName.IsEmpty())
    {
        setText(toQString(Qocoon::trimmedResourceName(fileName)));
        emit editingFinished();
    }
}

ResourceListWidget::ResourceListWidget(StringHash type, QWidget* parent): QWidget(parent),
    resourceType_{ type },
    layout_{ new QVBoxLayout{ this } },
    resourceWidgets_{}
{
    layout_->setMargin(0);
    setLayout(layout_);
}

void ResourceListWidget::setNumResources(int count)
{
    storeValues();
    clear();

    while (resourceWidgets_.size() < count)
        addResourceEdit();

    restoreValues();
}

void ResourceListWidget::setValues(const Variant& values)
{
    if (values.GetType() != VAR_RESOURCEREFLIST)
        return;

    const ResourceRefList& refList{ values.GetResourceRefList() };

    if (refList.type_ != resourceType_)
        return;

    for (unsigned n{ 0u }; n < refList.names_.Size(); ++n)
    {
        if (static_cast<int>(n) >= resourceWidgets_.size())
            return;

        resourceWidgets_.at(n)->setText(toQString(refList.names_.At(n)));
    }
}

ResourceRefList ResourceListWidget::refList() const
{
    ResourceRefList resourceRefList{ resourceType_ };
    for (int w{ 0 }; w < resourceWidgets_.size(); ++w)
        resourceRefList.names_.Push(toString(resourceWidgets_.at(w)->text()));

    return resourceRefList;
}

void ResourceListWidget::updateNumResources(Serializable* serializable)
{
    if (!serializable)
        return;

    setNumResources(serializable->GetAttribute(attributeName_).GetResourceRefList().names_.Size());
}

void ResourceListWidget::clear()
{
    while (QLayoutItem* item{ layout_->takeAt(0) })
        item->widget()->deleteLater();

    resourceWidgets_.clear();
}

void ResourceListWidget::addResourceEdit()
{
    ResourceLineEdit* resourceWidget{ new ResourceLineEdit{ resourceType_, this } };;
    layout_->addWidget(resourceWidget);
    resourceWidgets_.push_back(resourceWidget);

    connect(resourceWidget, SIGNAL(editingFinished()), this, SIGNAL(editingFinished()));
}

void ResourceListWidget::storeValues()
{
    QStringList values{};
    for (ResourceLineEdit* lineEdit: resourceWidgets_)
        values.push_back(lineEdit->text());

    for (int v{ 0 }; v < values.size(); ++v)
    {
        const QString& value{ values.at(v) };
        if (v < valueStore_.size())
            valueStore_.replace(v, value);
        else
            valueStore_.push_back(value);
    }
}

void ResourceListWidget::restoreValues()
{
    for (int w{ 0 }; w < resourceWidgets_.size(); ++w)
    {
        if (w >= valueStore_.size())
            return;

        resourceWidgets_.at(w)->setText(valueStore_.at(w));
    }
}
