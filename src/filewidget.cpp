/* ManaWarg
// Copyright (C) 2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QPushButton>
#include "drydockwidget.h"

#include "filewidget.h"

FileWidget::FileWidget(Context* context, QWidget* parent): DryWidget(context, parent),
    fileName_{ "" },
    fileOperations_{},
    fileToolBar_{ new QToolBar(this) },
    actionsToolBar_{ new QToolBar(this) },
    toolBarSpacer_{ new QWidget(fileToolBar_) },
    toolBarLayout_{ new QHBoxLayout() },
    pickerBox_{ nullptr },
    firstShow_{ true }
{
    for (const QString& actionName: { "New", "Open", "Save", "Save As" })
    {
        QAction* action{ new QAction(QIcon(":/" + QString{ actionName }.replace(' ', "")), actionName, this) };

        if (actionName == "New")
            connect(action, SIGNAL(triggered(bool)), this, SLOT(actionNew()));
        else if (actionName == "Open")
            connect(action, SIGNAL(triggered(bool)), this, SLOT(actionOpen()));
        else if (actionName == "Save")
            connect(action, SIGNAL(triggered(bool)), this, SLOT(actionSave()));
        else if (actionName == "Save As")
            connect(action, SIGNAL(triggered(bool)), this, SLOT(actionSaveAs()));

        fileOperations_.push_back(action);
    }
}

void FileWidget::createPickerBox()
{
    pickerBox_ = new QGroupBox();
    QVBoxLayout* pickerBoxLayout{ new QVBoxLayout() };
    pickerBoxLayout->setMargin(4);
    pickerBox_->setLayout(pickerBoxLayout);
    pickerBox_->setVisible(false);
    pickerBox_->setStyleSheet("QGroupBox { border-width: 1px; border-color: rgba(0, 0, 0, 0.23); border-style: solid; border-radius: 2px; margin: 1px; }"
                              "QGroupBox::title { background-color: transparent; }");
}

void FileWidget::createToolBar()
{
    toolBarSpacer_->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Minimum);
    fileToolBar_->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    fileToolBar_->addActions(fileOperations_);

    fileToolBar_->setIconSize(QSize(16, 16));
    actionsToolBar_->setIconSize(QSize(16, 16));

    fileToolBar_->layout()->setMargin(0);
    actionsToolBar_->layout()->setMargin(0);
    toolBarLayout_->setMargin(0);
    toolBarLayout_->setSpacing(0);

    toolBarLayout_->addWidget(actionsToolBar_);
    toolBarLayout_->addWidget(toolBarSpacer_);
    toolBarLayout_->addWidget(fileToolBar_);
}

void FileWidget::completeOperationsText(const QString& addition)
{
    for (QAction* operation: fileOperations_)
    {
        const QString name{ operation->text() };

        if (!name.contains(' '))
            operation->setText(name + " " + addition);
        else
            operation->setText(QString{ name }.replace(' ', " " + addition + " "));
    }
}

void FileWidget::setFileButtonsVisible(bool visible)
{
    fileToolBar_->setVisible(visible);
}

void FileWidget::resizeEvent(QResizeEvent* /*event*/)
{
    updatePickerBoxTitle();
    updateTitle();
}

void FileWidget::setOrientation(Qt::Orientation orientation)
{
    DryWidget::setOrientation(orientation);
    bool horizontal{ orientation_ == Qt::Horizontal };

    fileToolBar_->setOrientation(   horizontal ? Qt::Vertical            : Qt::Horizontal);
    actionsToolBar_->setOrientation(horizontal ? Qt::Vertical            : Qt::Horizontal);
    toolBarLayout_->setDirection(   horizontal ? QBoxLayout::BottomToTop : QBoxLayout::LeftToRight);
    toolBarSpacer_->setSizePolicy(  horizontal ? QSizePolicy::Minimum    : QSizePolicy::Expanding,
                                    horizontal ? QSizePolicy::Expanding  : QSizePolicy::Minimum);
}

void FileWidget::updatePickerBoxTitle()
{
    if (pickerBox_)
    {
        QList<QString> titleWords{ pickerBox_->objectName().split(' ') };
        bool background{ titleWords.contains("Background") };
        QString title{ titleWords.first() };
        if (background)
            title = titleWords[1];

        for (int w{ 1 }; w < titleWords.count(); ++w)
        {
            QString longerTitle{ title + " " + titleWords[w] };

            if (w == 1 && background)
                longerTitle = titleWords[0] + " " + title;

            if (pickerBox_->fontMetrics().horizontalAdvance(longerTitle) < pickerBox_->width() - 32)
                title = longerTitle;
            else
                break;
        }

        if (pickerBox_->fontMetrics().horizontalAdvance(title) > pickerBox_->width() - 31 &&
            title == "Background")
        {
            title = "Back...";
        }

        if (pickerBox_->title() != title)
            pickerBox_->setTitle(title);
    }
}

void FileWidget::raiseDockWidget()
{
    DryDockWidget* dockWidget{ static_cast<DryDockWidget*>(parentWidget()) };
    if (dockWidget)
        dockWidget->raise();
}

void FileWidget::setTitle(const QString& title)
{
    title_ = title;

    if (DryDockWidget* dockWidget{ qobject_cast<DryDockWidget*>(parentWidget()) })
        dockWidget->updateTitle();
}

void FileWidget::updateTitle()
{
    QString title{ objectName() };

    ///Also affects tabs :(
//    QString trimmedName{ toQString(GetSubsystem<Weaver>()->trimmedResourceName(fileName_)) };

//    if (!trimmedName.isEmpty()) {

//        title.append(" - " + trimmedName);

//        int start{ title.indexOf('-') + 2 };
//        int length{ title.lastIndexOf('/') - start };

//        if (tooLong(title)) {

//            title.remove(start, ++length);

//            if (tooLong(title)) {

//                title.remove(0, start);
//            }

//        }
//    }

    setTitle(title);
}

bool FileWidget::tooLong(QString title)
{
    int margin{ 45 };

    return (orientation_ == Qt::Vertical   && fontMetrics().horizontalAdvance(title) > width()  - margin)
        || (orientation_ == Qt::Horizontal && fontMetrics().horizontalAdvance(title) > height() - margin);
}

///

FlipSplitter::FlipSplitter(QWidget* parent, int snap): QSplitter(parent),
    threshold_{ 1.f },
    squareSnap_{ snap }
{
    if (squareSnap_ != -1)
        connect(this, SIGNAL(splitterMoved(int, int)), SLOT(snap()));
}

void FlipSplitter::setStretchFactors(Qt::Orientation orientation, int first, int second)
{
    stretchFactors_[orientation] = std::pair<int, int>{ first, second };

    if (QSplitter::orientation() == orientation)
    {
        setStretchFactor(0, stretchFactors_[orientation].first);
        setStretchFactor(1, stretchFactors_[orientation].second);
    }
}

void FlipSplitter::resizeEvent(QResizeEvent* event)
{
    float ratio{ static_cast<float>(width()) / height() };
//    double bias{ static_cast<double>(sizes()[0] + sizes()[1]) / sizes()[1] };
//    ratio -= bias * 0.1;

    Qt::Orientation splitterOrientation{ orientation() };

    if (ratio > threshold_ + 0.1f)
        splitterOrientation = Qt::Horizontal;
    else if (ratio < threshold_ - 0.1f)
        splitterOrientation = Qt::Vertical;

    if (orientation() != splitterOrientation)
    {
        setOrientation(splitterOrientation);
        setStretchFactor(0, stretchFactors_[splitterOrientation].first);
        setStretchFactor(1, stretchFactors_[splitterOrientation].second);

        emit orientationChanged(splitterOrientation);
    }

    QSplitter::resizeEvent(event);
}

void FlipSplitter::snap()
{
//    const int margin{ 8 };

//    if (squareSnap_ > -1)
//    {
//        if (orientation() == Qt::Horizontal)
//        {
//            const int near{ sizes().at(squareSnap_) - height() };

//            if (abs(near > margin))
//                return;

//            if (squareSnap_ == 0)
//                setSizes({ height(), width() - height() });
//            else
//                setSizes({ width() - height(), height() });
//        }
//        else if (orientation() == Qt::Vertical)
//        {
//            const int near{ sizes().at(squareSnap_) - width() };

//            if (abs(near > margin))
//                return;

//            if (squareSnap_ == 0)
//                setSizes({ width(), height() - width() });
//            else
//                setSizes({ height() - width(), width() });
//        }
//    }
}
