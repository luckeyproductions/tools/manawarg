/* ManaWarg
// Copyright (C) 2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "qocoon.h"

#include "selector.h"

Selection Selector::emptySelection_{ nullptr };
Selection Selector::storedSelection_{ emptySelection_ };

Selector::Selector(Context* context, QObject* parent): DryObject(context, parent),
    selections_{}
{
}

Selection& Selector::selectionForScene(Scene* scene, bool create)
{
    if (scene)
    {
        for (Selection& selection: selections_)
        {
            if (selection.scene() == scene)
                return selection;
        }

        if (create)
        {
            selections_.Push(Selection{ scene });
            return selections_.Back();
        }
    }

    return emptySelection_;
}

Selection& Selector::activeSelection(bool create)
{
    Scene* activeScene{ GetSubsystem<Qocoon>()->activeScene() };
    return selectionForScene(activeScene, create);
}

Object* Selector::activeObject()
{
    return activeSelection(false).activeObject();
}

HashSet<Object*> Selector::selection(bool excludeScene)
{
    return activeSelection(false).selection(excludeScene);
}

HashSet<Node*> Selector::selectedNodes(bool filterConflicting)
{
    HashSet<Node*> selected{ activeSelection(false).selectedNodes() };

    if (filterConflicting)
    {
        PODVector<Animatable*> toFilter{};
        for (Node* node: selected)
            toFilter.Push(node);
        toFilter = GetSubsystem<Qocoon>()->filterConflicting(toFilter);
        selected.Clear();
        for (Animatable* a: toFilter)
            selected.Insert(static_cast<Node*>(a));
    }

    return selected;
}

HashSet<Component*> Selector::selectedComponents()
{
    return activeSelection(false).selectedComponents();
}

bool Selector::isSelected(Object* object)
{
    return activeSelection(false).selection().Contains(object);
}

bool Selector::isActive(Object* object)
{
    return activeSelection(false).activeObject() == object;
}

bool Selector::clearSelection()
{
    return activeSelection(false).clear();
}

void Selector::deselectAll()
{
    if (clearSelection())
    {
        emit selectionChanged(this);
        emit activeObjectChanged(nullptr);
    }
}

bool Selector::addToSelection(Object* object)
{
    return activeSelection().add(object);
}

void Selector::select(const HashSet<Object*>& objects, bool append)
{
    Selection& sel{ activeSelection(true) };
    const HashSet<Object*> lastSelected{ sel.selection(false) };
    Object* lastActive{ sel.activeObject() };
    bool otherSelection{ false };

    PODVector<Object*> newlySelected{};
    for (Object* o: objects)
    {
        if (o && !lastSelected.Contains(o))
            newlySelected.Push(o);
    }

    if (!append && lastSelected.Size() && lastSelected != objects)
    {
        clearSelection();
        otherSelection = true;
    }

    for (Object* o: objects)
        otherSelection |= addToSelection(o);

    const HashSet<Object*> newSelection{ selection(false) };
    if (newlySelected.Size())
        sel.setActiveObject(newlySelected.Back());
    else if (newSelection.Contains(lastActive))
        sel.setActiveObject(lastActive);
    else if (newSelection.Size() && !activeObject())
        sel.setActiveObject(newSelection.Front());

    if (otherSelection)
        emit selectionChanged(this);

    if (activeObject() != lastActive)
        emit activeObjectChanged(activeObject());
}

void Selector::select(Object* object, bool append)
{
    if (!object)
        return;

    if (activeSelection().setActiveObject(object))
        emit activeObjectChanged(activeObject());

    select(HashSet<Object*>{ object }, append);
}

bool Selector::removeFromSelection(Object* object)
{
    return activeSelection(false).remove(object);
}

void Selector::deselect(const HashSet<Object*>& objects)
{
    if (objects.IsEmpty())
        return;

    bool deselected{ false };

    for (Object* o: objects)
        deselected |= removeFromSelection(o);

    Selection& sel{ activeSelection() };
    if (objects.Contains(activeObject()))
    {
        if (!sel.isEmpty())
            sel.setActiveObject(selection(false).Back());
        else
            sel.setActiveObject(nullptr);

        emit activeObjectChanged(activeObject());
    }

    if (deselected)
        emit selectionChanged(this);
}

void Selector::deselect(Object* object)
{
    deselect(HashSet<Object*>{ object });
}

void Selector::reselect()
{
    emit selectionChanged(this);
    emit activeObjectChanged(activeObject());
}

void Selector::restore()
{
    Selection& restoring{ selectionForScene(storedSelection_.scene(), true) };
    const bool selectionChange( storedSelection_.selectionIDs() != restoring.selectionIDs() );
    const bool activeChange{ storedSelection_.activeObject() != restoring.activeObject() };
    restoring = storedSelection_;

    if (storedSelection_.scene() == activeSelection().scene())
    {
        if (selectionChange)
            emit selectionChanged(this);
        if (activeChange)
            emit activeObjectChanged(activeObject());
    }
}
