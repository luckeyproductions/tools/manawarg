/* ManaWarg
// Copyright (C) 2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef CLIPBOARD_H
#define CLIPBOARD_H

#include "dryobject.h"
#include <QAction>

class Clipboard: public DryObject
{
    Q_OBJECT
    DRY_OBJECT(Clipboard, DryObject);

public:
    Clipboard(Context* context, QObject* parent = nullptr);

    QList<QAction*> editActions() const
    {
        return { cutAction_, copyAction_, pasteAction_ };
    }

private slots:
    void cut();
    void copy();
    void paste();

private:
    void createActions();

    Vector<SharedPtr<XMLFile>> copied_;
    QAction* cutAction_;
    QAction* copyAction_;
    QAction* pasteAction_;
};

#endif // CLIPBOARD_H
