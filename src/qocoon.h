/* ManaWarg
// Copyright (C) 2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef QOCOON_H
#define QOCOON_H

#include <QMap>
#include <QMainWindow>
#include <QUndoView>
#include <QComboBox>
#include <QStandardPaths>

#include "project.h"
#include "sceneviewer.h"
#include "drydockwidget.h"
#include "undostack/sceneundostack.h"

#include "dry.h"

enum EditMode{ EM_Standard, EM_Block, EM_Terrain, EM_AllModes }; //EM_PiRMIT
static const Vector<EditMode> MODES { EM_Standard, EM_Block, EM_Terrain };

class Project;
class QStackedWidget;
class NoProjectWidget;

class Qocoon: public QMainWindow, public Object
{
    Q_OBJECT
    DRY_OBJECT(Qocoon, Object)

public:
    explicit Qocoon(Context* context);
    explicit Qocoon(Context* context, XMLFile* xmlFile);
    ~Qocoon();

    QMenu* addMenu() { return addMenu_; }
    QMenu* prefabMenu() { return prefabMenu_; }
    void updatePrefabMenu(const QStringList& prefabs = {});

    bool loadProject(const QString& filename);
    void setScene(Scene* scene, SceneViewer* viewer = nullptr);
    SceneViewer* activeSceneViewer() const { return centralView_; }
    Scene* activeScene() const { return centralView_->scene(); }
    Node* activeNode();

    SceneUndoStack* sceneStack(Scene* scene);
    QAction* runAction() const { return runAction_; }
    bool isRunning() const { return runAction_->isChecked(); }

    QAction* deleteAction() const { return deleteAction_; }
    TransformationSpace selectedTransformSpace() const
    {
        return static_cast<TransformationSpace>(transformSpaceBox_->itemData(transformSpaceBox_->currentIndex()).toInt());
    }

    template <class T>
    DryDockWidget* createDockWidget(Qt::DockWidgetArea area)
    {
        T* widget{ new T(context_) };
        context_->RegisterSubsystem(widget);
        DryDockWidget* dockWidget{ new DryDockWidget(widget) };

        if (area != Qt::NoDockWidgetArea)
            addDockWidget(area, dockWidget);

        return dockWidget;
    }

    static Project* project() { return project_; }
    static String projectLocation();
    static String trimmedResourceName(const String& fileName);
    static String containingFolder(const String& path);
    static String locateResourceRoot(const String& resourcePath);

    Node* addPrefabToNode(Node* parent, const String& prefabFilename, const Matrix3x4& transform = Matrix3x4::IDENTITY);
    void applyPrefab(PODVector<Node*> nodes, const String& prefabFilename);
    void reparent(const PODVector<Animatable*>& animatables, Node* target);
    void removeObjects(const PODVector<Animatable*>& animatables);

    PODVector<Animatable*> filterConflicting(const PODVector<Animatable*>& animatables, Node* target = nullptr) const;
    PODVector<Node*> filterConflicting(const PODVector<Node*>& nodes, Node* target = nullptr) const;

    void removeObject(Animatable* animatable);

signals:
    void resourceFoldersChanged();
    void recentProjectsChanged();
    void currentProjectChanged();

public slots:
    void newProject(QString startPath = "");
    void openRecent() { openProject(sender()->objectName()); }
    bool openScene(String filename = "", SceneViewer* viewer = nullptr);
    void closeScene(SceneViewer* viewer = nullptr);
    void addResourceFolder();
    void runSimulation(bool start);
    void actionDelete();

    SharedPtr<Component> addComponent(Node* node, StringHash componentType, XMLFile* data = nullptr);
    Node* addNode(Node* parent, XMLFile* data = nullptr);

    void allowUndo(bool enabled) { undoAction_->setEnabled(enabled); }
    void allowRedo(bool enabled) { redoAction_->setEnabled(enabled); }

protected:
    void resizeEvent(QResizeEvent* event) override;

private slots:
    void openProject(QString filename = "");
    bool saveProject();
    void closeProject();
    void updateRecentProjectMenu();
    void updateAvailableActions();

    void newScene();
    bool saveScene(bool saveAs = false);
    bool saveSceneAs() { return saveScene(true); }

    void toggleFullView(bool toggled);
    void about();
    void openUrl();
    void changeMode(bool toggled);
    void undo();
    void redo();

    void addNodeOrComponent();
    void addPrefabAction();

private:
    QMenu* createMenuBar();
    QMenu* createProjectMenu();
    QMenu* createSceneMenu();
    void createEditMenu();
    QMenu* createAddMenu();
    void createViewMenu();
    void createHelpMenu();
    void createToolBar();
    void loadSettings();
    void updateRecentProjects();
    QString modeToString(EditMode mode);
    void setMode(EditMode mode);

    static SharedPtr<Project> project_;
    bool singleWidget_;
    EditMode mode_;
    QMap<EditMode, QAction*> modeActions_;
    QList<QAction*> projectDependentActions_;
    QList<QAction*> sceneDependentActions_;
    QToolBar* mainToolBar_;
    QStackedWidget* centralStackWidget_;
    SharedPtr<NoProjectWidget> noProjectWidget_;
    SceneViewer* centralView_;
    std::vector<QDockWidget*> hiddenDocks_;
    std::vector<SceneUndoStack*> undoStacks_;
    QUndoView* undoView_;

    QAction* undoAction_;
    QAction* redoAction_;
    QAction* runAction_;
    QAction* fullViewAction_;
    QAction* deleteAction_;
    QMenu* recentMenu_;
    QMenu* addMenu_;
    QMenu* prefabMenu_;
    QComboBox* transformSpaceBox_;
};

#endif // QOCOON_H
