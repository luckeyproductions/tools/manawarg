/* ManaWarg
// Copyright (C) 2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "selector.h"
#include "nodetree.h"
#include "qocoon.h"

#include "clipboard.h"

Clipboard::Clipboard(Context* context, QObject* parent): DryObject(context, parent),
    copied_{},
    cutAction_{ nullptr },
    copyAction_{ nullptr },
    pasteAction_{ nullptr }
{
    createActions();
}

void Clipboard::cut()
{
    if (GetSubsystem<Selector>()->selection().IsEmpty())
        return;

    copy();

    Qocoon* qocoon{ GetSubsystem<Qocoon>() };
    SceneUndoStack* undoStack{ qocoon->sceneStack(qocoon->activeScene()) };
    undoStack->beginMacro("Cut selection");
    qocoon->actionDelete();
    undoStack->endMacro();
}

void Clipboard::copy()
{
    Selector* selector{ GetSubsystem<Selector>() };
    if (selector->selection().IsEmpty())
        return;

    copied_.Clear();

    const HashSet<Node*> selectedNodes{ selector->selectedNodes(true) };
    const HashSet<Component*> selectedComponents{ selector->selectedComponents() };

    for (Component* c: selectedComponents)
    {
        Node* parent{ c->GetNode() };
        while (parent)
        {
            if (selectedNodes.Contains(parent))
                continue;

            parent = parent->GetParent();
        }

        SharedPtr<XMLFile> componentFile{ context_->CreateObject<XMLFile>() };
        XMLElement componentElem{ componentFile->CreateRoot("component") };
        c->SaveXML(componentElem);
        copied_.Push(componentFile);
    }

    for (Node* n: selectedNodes)
    {
        SharedPtr<XMLFile> nodeFile{ context_->CreateObject<XMLFile>() };
        XMLElement nodeElem{ nodeFile->CreateRoot("node") };
        n->SaveXML(nodeElem);
        copied_.Push(nodeFile);
    }
}

void Clipboard::paste()
{
    if (copied_.IsEmpty())
        return;

    Qocoon* qocoon{ GetSubsystem<Qocoon>() };
    Selector* selector{ GetSubsystem<Selector>() };
    Node* parent{ qocoon->activeScene() };
    Object* activeObject{ selector->activeObject() };
    if (activeObject)
    {
        if (activeObject->IsInstanceOf<Node>())
            parent = static_cast<Node*>(activeObject);
        else if (parent != activeObject)
            parent = static_cast<Component*>(activeObject)->GetNode();
    }

    SceneUndoStack* undoStack{ qocoon->sceneStack(qocoon->activeScene()) };
    undoStack->beginMacro("Paste");

    for (XMLFile* file: copied_)
    {
        XMLElement root{ file->GetRoot() };
        if (root.GetName() == "node")
            qocoon->addNode(parent, file);
        else if (root.GetName() == "component")
            qocoon->addComponent(parent, root.GetAttribute("type"), file);
    }

    undoStack->endMacro();

    GetSubsystem<NodeTree>()->updateTree();
}

void Clipboard::createActions()
{
    cutAction_ = new QAction{ "Cut", this };
    cutAction_->setShortcut({ "Ctrl+X" });
    connect(cutAction_, SIGNAL(triggered(bool)), this, SLOT(cut()));

    copyAction_ = new QAction{ "Copy", this };
    copyAction_->setShortcut({ "Ctrl+C" });
    connect(copyAction_, SIGNAL(triggered(bool)), this, SLOT(copy()));

    pasteAction_ = new QAction{ "Paste", this };
    pasteAction_->setShortcut({ "Ctrl+V" });
    connect(pasteAction_, SIGNAL(triggered(bool)), this, SLOT(paste()));
}
