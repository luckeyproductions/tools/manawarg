/* ManaWarg
// Copyright (C) 2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef SELECTOR_H
#define SELECTOR_H

#include "dryobject.h"

class Selection
{

public:
    Selection(Scene* scene):
        scene_{ scene },
        nodes_{},
        components_{},
        active_{ 0u, false }
    {}

    Scene* scene() const { return scene_; }

    HashSet<Object*> selection(bool excludeScene = true) const
    {
        HashSet<Object*> objects{};
        for (Node* n: selectedNodes(excludeScene))
            objects.Insert(static_cast<Object*>(n));
        for (Component* c: selectedComponents())
            objects.Insert(static_cast<Object*>(c));

        return objects;
    }

    Pair<const HashSet<unsigned>&, const HashSet<unsigned>&> selectionIDs() const
    {
        return { nodes_, components_ };
    }

    HashSet<Node*> selectedNodes(bool excludeScene = true) const
    {
        HashSet<Node*> nodes{};
        if (scene_)
        {
            for (unsigned id: nodes_)
            {
                if (excludeScene && id == scene_->GetID())
                    continue;

                if (Node* node{ scene_->GetNode(id) })
                    nodes.Insert(node);
            }
        }

        return nodes;
    }

    HashSet<Component*> selectedComponents() const
    {
        HashSet<Component*> components{};
        if (scene_)
        {
            for (unsigned id: components_)
            {
                if (Component* component{ scene_->GetComponent(id) })
                    components.Insert(component);
            }
        }

        return components;
    }

    bool setActiveObject(Object* object)
    {
        const Pair<unsigned, bool> lastActive{ active_ };
        if (!object)
        {
            active_ = {};
            return lastActive.first_ != 0u;
        }

        if (isSceneOrNode(object))
            active_ = { static_cast<Node*>(object)->GetID(), true };
        else
            active_ = { static_cast<Component*>(object)->GetID(), false };

        return active_ != lastActive;
    }

    Object* activeObject() const
    {
        if (!scene_ || active_.first_ == 0u)
            return nullptr;

        if (active_.second_)
            return static_cast<Object*>(scene_->GetNode(active_.first_));
        else
            return static_cast<Object*>(scene_->GetComponent(active_.first_));
    }

    bool clear()
    {
        bool any{ !isEmpty() };

        nodes_.Clear();
        components_.Clear();
        active_.first_ = 0u;

        return any;
    }

    bool add(Object* object)
    {
        if (!object || selection(false).Contains(object))
            return false;

        if (isSceneOrNode(object))
            nodes_.Insert(static_cast<Node*>(object)->GetID());
        else
            components_.Insert(static_cast<Component*>(object)->GetID());

        return true;
    }

    bool remove(Object* object)
    {
        if (!object || !selection(false).Contains(object))
            return false;

        if (isSceneOrNode(object))
            nodes_.Erase(static_cast<Node*>(object)->GetID());
        else
            components_.Erase(static_cast<Component*>(object)->GetID());

        return true;
    }

    bool isEmpty() const
    {
        return nodes_.IsEmpty() && components_.IsEmpty();
    }

private:
    bool isSceneOrNode(Object* object)
    {
        if (!object)
            return false;

        return object->GetType() == Node::GetTypeStatic() ||
               object->GetType() == Scene::GetTypeStatic();
    }

    Scene* scene_;
    HashSet<unsigned> nodes_;
    HashSet<unsigned> components_;
    Pair<unsigned, bool> active_;
};

class Selector: public DryObject
{
    Q_OBJECT
    DRY_OBJECT(Selector, DryObject)

public:
    Selector(Context* context, QObject* parent = nullptr);

    Object* activeObject();
    HashSet<Object*> selection(bool excludeScene = true);
    HashSet<Node*> selectedNodes(bool filterConflicting = false);
    HashSet<Component*> selectedComponents();
    bool isSelected(Object* object);
    bool isActive(Object* object);

    void select(Object* object, bool append);
    void select(const HashSet<Object*>& objects, bool append);
    void deselect(Object* object);
    void deselect(const HashSet<Object*>& objects);
    void deselectAll();
    void reselect();

    inline void store() { storedSelection_ = activeSelection(false); }
    void restore();

signals:
    void selectionChanged(Selector* selector);
    void activeObjectChanged(Object* object);

private:
    bool addToSelection(Object* object);
    bool removeFromSelection(Object* object);
    bool clearSelection();
    Selection& selectionForScene(Scene* scene, bool create);
    Selection& activeSelection(bool create = true);
    Vector<Selection> selections_;
    static Selection emptySelection_;
    static Selection storedSelection_;
};

#endif // SELECTOR_H
