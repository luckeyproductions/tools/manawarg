/* ManaWarg
// Copyright (C) 2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "selector.h"
#include "weaver.h"
#include "qocoon.h"
#include "sceneviewer.h"
#include "propertieseditor.h"
#include "undostack/attributecommand.h"

#include "transformer.h"

Transformer::Transformer(Context* context, SceneViewer* parent): DryObject(context, parent),
//    activeGizmo_{ nullptr },
    translateAction_{ nullptr },
    rotateAction_{ nullptr },
    scaleAction_{ nullptr },
    applyAction_{ nullptr },
    cancelAction_{ nullptr },
    xAction_{ nullptr },
    yAction_{ nullptr },
    zAction_{ nullptr },
    xPlaneAction_{ nullptr },
    yPlaneAction_{ nullptr },
    zPlaneAction_{ nullptr },
    resetTranslationAction_{ nullptr },
    resetRotationAction_{ nullptr },
    resetScaleAction_{ nullptr },
    space_{ TS_View },
    transformationType_{ TT_None },
    pivot_{},
    axis_{ AL_None },
    transformation_{},
    transforming_{ false }
{
    createTransformActions();
}

void Transformer::createTransformActions()
{
    translateAction_ = new QAction{ "Translate", this };
    translateAction_->setShortcut({ "G" });
    connect(translateAction_, SIGNAL(triggered(bool)), this, SLOT(beginTranslate()));

    rotateAction_ = new QAction{ "Rotate", this };
    rotateAction_->setShortcut({ "R" });
    connect(rotateAction_, SIGNAL(triggered(bool)), this, SLOT(beginRotate()));

    scaleAction_ = new QAction{ "Scale", this };
    scaleAction_->setShortcut({ "S" });
    connect(scaleAction_, SIGNAL(triggered(bool)), this, SLOT(beginScale()));

    applyAction_ = new QAction{ "Apply", this };
    applyAction_->setShortcut({ "Return" });
    connect(applyAction_, SIGNAL(triggered(bool)), this, SLOT(endTransformation()));

    cancelAction_ = new QAction{ "Cancel", this };
    cancelAction_->setShortcut({ "Esc" });
    connect(cancelAction_, SIGNAL(triggered(bool)), this, SLOT(cancelTransformation()));

    xAction_ = new QAction{ "X-Lock", this };
    xAction_->setShortcut({ "X" });
    connect(xAction_, SIGNAL(triggered(bool)), this, SLOT(setAxisLock()));

    yAction_ = new QAction{ "Y-Lock", this };
    yAction_->setShortcut({ "Y" });
    connect(yAction_, SIGNAL(triggered(bool)), this, SLOT(setAxisLock()));

    zAction_ = new QAction{ "Z-Lock", this };
    zAction_->setShortcut({ "Z" });
    connect(zAction_, SIGNAL(triggered(bool)), this, SLOT(setAxisLock()));

    xPlaneAction_ = new QAction{ "X-PlaneLock", this };
    xPlaneAction_->setShortcut({ "Shift+X" });
    connect(xPlaneAction_, SIGNAL(triggered(bool)), this, SLOT(setPlaneLock()));

    yPlaneAction_ = new QAction{ "Y-PlaneLock", this };
    yPlaneAction_->setShortcut({ "Shift+Y" });
    connect(yPlaneAction_, SIGNAL(triggered(bool)), this, SLOT(setPlaneLock()));

    zPlaneAction_ = new QAction{ "Z-PlaneLock", this };
    zPlaneAction_->setShortcut({ "Shift+Z" });
    connect(zPlaneAction_, SIGNAL(triggered(bool)), this, SLOT(setPlaneLock()));

    resetTranslationAction_ = new QAction{ "Translate", this };
    resetTranslationAction_->setShortcut({ "Alt+G" });
    connect(resetTranslationAction_, SIGNAL(triggered(bool)), this, SLOT(resetTranslation()));

    resetRotationAction_ = new QAction{ "Rotate", this };
    resetRotationAction_->setShortcut({ "Alt+R" });
    connect(resetRotationAction_, SIGNAL(triggered(bool)), this, SLOT(resetRotation()));

    resetScaleAction_ = new QAction{ "Reset Scale", this };
    resetScaleAction_->setShortcut({ "Alt+S" });
    connect(resetScaleAction_, SIGNAL(triggered(bool)), this, SLOT(resetScale()));
}

void Transformer::updateAvailableActions()
{
    for (QAction* action: duringTransformActions())
        action->setEnabled(transforming_);
    for (QAction* action: notDuringTransformActions())
        action->setEnabled(!transforming_);
}

void Transformer::beginTransformation()
{
    Qocoon* qocoon{ context_->GetSubsystem<Qocoon>() };
    Selector* selector{ context_->GetSubsystem<Selector>() };

    if (selector->selectedNodes().IsEmpty() || qocoon->isRunning())
        return;

    const QPoint localMousePos{ sceneViewer()->mapFromGlobal(QCursor::pos()) };
    if (!sceneViewer()->rect().contains(localMousePos))
        return;

    if (transforming_)
        cancelTransformation();

    const HashSet<Node*> selectedNodes{ selector->selectedNodes() };
    if (!selectedNodes.IsEmpty())
    {
        qocoon->activeSceneViewer()->storeScene();

        Vector3 averagePosition{};
        for (Node* node: selectedNodes)
            averagePosition += node->GetWorldPosition();
        averagePosition /= selectedNodes.Size();
        pivot_ = averagePosition;
        space_ = qocoon->selectedTransformSpace();
        axis_ = AL_None;

        transforming_ = true;        
        sceneViewer()->grabMouse();
        updateAvailableActions();

        Log::Write(LOG_INFO, "Began transformation");
    }
}

void Transformer::endTransformation()
{
    if (!transforming_)
        return;

    applyTransformation();
    transformation_ = Matrix3x4::IDENTITY;
    transforming_ = false;
    sceneViewer()->releaseMouse();
    updateAvailableActions();

    Log::Write(LOG_INFO, "Ended transformation");
}

void Transformer::cancelTransformation()
{
    if (!transforming_)
        return;

    Qocoon* qocoon{ context_->GetSubsystem<Qocoon>() };
    qocoon->activeSceneViewer()->restoreScene();

    transformation_ = Matrix3x4::IDENTITY;
    transforming_ = false;
    sceneViewer()->releaseMouse();
    updateAvailableActions();

    Log::Write(LOG_INFO, "Cancelled transformation");
}

void Transformer::beginTranslate()
{
    transformationType_ = TT_Translate;
    beginTransformation();
}

void Transformer::beginRotate()
{
    transformationType_ = TT_Rotate;
    beginTransformation();
}

void Transformer::beginScale()
{
    transformationType_ = TT_Scale;
    beginTransformation();
}

void Transformer::setAxisLock()
{
    const AxisLock oldAxis{ axis_ };
    const char axisName{ static_cast<QAction*>(sender())->shortcut().toString().back().toLatin1() };
    axis_ = charToLock(axisName, false);
    if (axis_ == oldAxis)
        axis_ = AL_None;
}

void Transformer::setPlaneLock()
{
    const AxisLock oldAxis{ axis_ };
    const char axisName{ static_cast<QAction*>(sender())->shortcut().toString().back().toLatin1() };
    axis_ = charToLock(axisName, true);
    if (axis_ == oldAxis)
        axis_ = AL_None;
}

void Transformer::resetTranslation()
{
    resetAttribute("Position");
}

void Transformer::resetRotation()
{
    resetAttribute("Rotation");
}

void Transformer::resetScale()
{
    resetAttribute("Scale");
}

void Transformer::resetAttribute(const String& attribute)
{
    Qocoon* qocoon{ context_->GetSubsystem<Qocoon>() };
    Selector* selector{ context_->GetSubsystem<Selector>() };
    const HashSet<Node*> selectedNodes{ selector->selectedNodes() };

    if (selectedNodes.IsEmpty() || qocoon->isRunning())
        return;

    SceneUndoStack* undoStack{ qocoon->sceneStack(qocoon->activeScene()) };
    PODVector<AttributeCommand*> commands{};
    for (Node* node: selectedNodes)
    {
        const Variant defaultValue{ node->GetAttributeDefault(attribute) };
        if (node->GetAttribute(attribute) == defaultValue)
            continue;

        AttributeCommand* command{ new AttributeCommand{ node, attribute } };
        command->setNewValue(defaultValue);
        commands.Push(command);
    }

    if (!commands.IsEmpty())
    {
        undoStack->beginMacro("Reset " + toQString(attribute));
        for (AttributeCommand* command: commands)
            undoStack->push(command);
        undoStack->endMacro();
    }
}

void Transformer::handleMouseMove(const QPoint& dPos)
{
    SceneViewer* view{ sceneViewer() };
    Camera* camera{ view->camera() };
    Node* cameraNode{ camera->GetNode() };
    const float pivotDistance{ cameraNode->GetWorldPosition().DistanceToPoint(pivot_) };

    Vector3 xTo{};
    Vector3 yTo{};
    Matrix3x4 transformDelta{};

    if (transformationType_ == TT_Translate)
    {
        const float factor{ camera->GetFov() / view->height() * pivotDistance * M_SQRT3 * .01f };
        translationAxes(xTo, yTo);
        transformDelta.SetTranslation(factor * Vector3{ dPos.x() * xTo - dPos.y() * yTo });
    }
    else if (transformationType_ == TT_Rotate)
    {
        const float factor{ camera->GetFov() / view->height() };
        rotationAxes(xTo, yTo);
        transformDelta.SetRotation((Quaternion{ factor * dPos.x(), xTo } * Quaternion{ -factor * dPos.y(), yTo }).RotationMatrix());
    }
    else if (transformationType_ == TT_Scale)
    {
        const float factor{ camera->GetFov() / view->height() * M_SQRT3 * .01f };
        scaleAxes(xTo, yTo);
        transformDelta.SetScale(Vector3::ONE + factor * Vector3{ dPos.x() * xTo - dPos.y() * yTo });
    }

    if (transformDelta != Matrix3x4::IDENTITY)
    {
        transformation_ = Matrix3x4{ transformation_.Translation() + transformDelta.Translation(),
                          transformDelta.Rotation() * transformation_.Rotation(),
                          transformation_.Scale() * transformDelta.Scale() };
        updateTransforms(transformDelta);
        view->updateView();
    }
}

void Transformer::updateTransforms(const Matrix3x4& delta) const
{
    Selector* selector{ context_->GetSubsystem<Selector>() };
    const HashSet<Node*> selectedNodes{ selector->selectedNodes(space_ == TS_View || space_ == TS_World) };

    for (Node* node: selectedNodes)
        node->SetTransform(determineNewTransform(node, delta));

    GetSubsystem<PropertiesEditor>()->updateFields();
}

void Transformer::applyTransformation() const
{
    Selector* selector{ context_->GetSubsystem<Selector>() };
    SharedPtr<XMLFile> transformData{ new XMLFile{ context_ } };
    XMLElement rootElem{ transformData->CreateRoot("transformations")};
    HashMap<unsigned, XMLElement> transformElements{};

    for (Node* node: selector->selectedNodes())
    {
        XMLElement nodeElem{ rootElem.CreateChild("node") };
        node->SaveXML(nodeElem);
        transformElements[node->GetID()] = nodeElem;
    }

    sceneViewer()->restoreScene();
    HashSet<Node*> selectedNodes{ selector->selectedNodes(space_ == TS_World || space_ == TS_View) };

    if (transformation_ == Matrix3x4::IDENTITY)
        return;

    Qocoon* qocoon{ context_->GetSubsystem<Qocoon>() };
    SceneUndoStack* undoStack{ qocoon->sceneStack(qocoon->activeScene()) };
    QString macroName;
    switch (transformationType_)
    {
    case TT_None: default: return;
    case TT_Translate: macroName = "Translate Node"; break;
    case TT_Rotate:    macroName = "Rotate Node";    break;
    case TT_Scale:     macroName = "Scale Node";     break;
    }

    if (selectedNodes.Size() > 1u)
        macroName.append("s");

    undoStack->beginMacro(macroName);
    for (Node* node: selectedNodes)
    {
        AttributeCommand* positionCommand{ new AttributeCommand{ node, "Position" } };
        AttributeCommand* rotationCommand{ new AttributeCommand{ node, "Rotation" } };
        AttributeCommand* scaleCommand   { new AttributeCommand{ node, "Scale"    } };

        XMLElement nodeElement{ transformElements[node->GetID()] };
        XMLElement attributeElement{ nodeElement.GetChild("attribute") };
        while (attributeElement)
        {
            const String attributeName{ attributeElement.GetAttribute("name") };
            if (attributeName == "Position")
                positionCommand->setNewValue(attributeElement.GetVector3("value"));
            else if (attributeName == "Rotation")
                rotationCommand->setNewValue(attributeElement.GetQuaternion("value"));
            else if (attributeName == "Scale")
                scaleCommand->setNewValue(attributeElement.GetVector3("value"));

            attributeElement = attributeElement.GetNext("attribute");
        }

        undoStack->push(positionCommand);
        undoStack->push(rotationCommand);
        undoStack->push(scaleCommand);
    }

    undoStack->endMacro();
}

Matrix3x4 Transformer::determineNewTransform(Node* node, const Matrix3x4& delta) const
{
    Node* parent{ node->GetParent() };
    Node*  camNode{ sceneViewer()->camera()->GetNode() };
    Matrix3x4 transform{ node->GetTransform() };
    const Vector3 parentSpacePivot{ parent->GetWorldTransform().Inverse() * pivot_ };
    const Vector3 fromPivot{ node->GetPosition() - parentSpacePivot };
    const Quaternion invParRot{ parent->GetWorldRotation().Inverse() };
    const Vector3 flipper{ flipVector(node, camNode) };

    if (transformationType_ == TT_Translate)
    {
        if (space_ == TS_View || space_ == TS_World || axis_ == AL_None)
        {
            const Vector3 translation{ transform.Translation() + invParRot * delta.Translation() };
            transform = Matrix3x4{ translation, transform.Rotation(), transform.Scale() };
        }
        else if (space_ == TS_Local)
        {
            const Vector3 translation{ transform.Translation() + flipper * (node->GetRotation() * delta.Translation()) };
            transform = Matrix3x4{ translation, transform.Rotation(), transform.Scale() };
        }
        else if (space_ == TS_Parent)
        {
            const Vector3 translation{ transform.Translation() + flipper * delta.Translation() };
            transform = Matrix3x4{ translation, transform.Rotation(), transform.Scale() };
        }
    }
    else if (transformationType_ == TT_Rotate)
    {
        if (space_ == TS_View || space_ == TS_World || axis_ == AL_None)
        {
            const Quaternion worldRot{ node->GetWorldRotation() };
            const Quaternion rotation{ transform.Rotation() * worldRot.Inverse() * delta.Rotation() * worldRot };
            const Vector3 aroundPivot{ transform.Rotation().Inverse() * fromPivot };
            const Vector3 translation{ rotation * aroundPivot + parentSpacePivot };
            transform = Matrix3x4{ translation, rotation, transform.Scale() };
        }
        else if (space_ == TS_Local)
        {
            const Quaternion rotation{ transform.Rotation() * delta.Rotation() };
            transform = Matrix3x4{ transform.Translation(), rotation.Normalized(), transform.Scale() };
        }
        else if (space_ == TS_Parent)
        {
            const Quaternion rotation{ delta.Rotation() * transform.Rotation() };
            const Vector3 translation{ delta.Rotation() * transform.Translation() };
            transform = Matrix3x4{ translation, rotation.Normalized(), transform.Scale() };
        }
    }
    else if (transformationType_ == TT_Scale)
    {
        if (space_ == TS_View || space_ == TS_World)
        {
            const Vector3 scale{ transform.Scale() + transform.Scale() * (invParRot * (delta.Scale() - Vector3::ONE)) };
            const Vector3 aroundPivot{ delta.Scale() * fromPivot - fromPivot };
            const Vector3 translationDelta{ delta.Translation() + aroundPivot };
            const Vector3 translation{ transform.Translation() + translationDelta };
            transform = Matrix3x4{ translation, transform.Rotation(), scale };
        }
        else if (space_ == TS_Local)
        {
            const Vector3 scale{ transform.Scale() * delta.Scale() };
            transform = Matrix3x4{ transform.Translation(), transform.Rotation(), scale };
        }
//        else if (space_ == TS_Parent)
//        {
//            const Vector3 translation{ transform.Translation() * delta.Scale() };
//            const Vector3 scale{ transform.Scale() * (invParRot * delta.Scale()) };
//            transform = Matrix3x4{ translation, transform.Rotation(), scale };
//        }
    }

    return transform;
}

void Transformer::translationAxes(Vector3& xTo, Vector3& yTo) const
{
    Node* cameraNode{ sceneViewer()->camera()->GetNode() };

    if (axis_ == AL_None || axis_ == AL_NotZ ||
        axis_ == AL_X || axis_ == AL_NotY)
        xTo = Vector3::RIGHT;
    if (axis_ == AL_None || axis_ == AL_NotZ ||
        axis_ == AL_Y || axis_ == AL_NotX)
        yTo = Vector3::UP;
    if (axis_ == AL_Z || axis_ == AL_NotY)
        yTo = Vector3::FORWARD;
    if (axis_ == AL_NotX)
        xTo = Vector3::FORWARD;

    if (space_ == TS_View || axis_ == AL_None)
    {
        const Quaternion camRot{ cameraNode->GetWorldRotation() };
        xTo = camRot * xTo;
        yTo = camRot * yTo;
    }
    else if (space_ == TS_World)
    {
        if (xTo.DotProduct(cameraNode->GetWorldRight()) < 0.f)
            xTo = -xTo;
        if (yTo.DotProduct(cameraNode->GetWorldUp())    < 0.f)
            yTo = -yTo;
    }
}

void Transformer::rotationAxes(Vector3& xTo, Vector3& yTo) const
{
    const Quaternion camRot{ sceneViewer()->camera()->GetNode()->GetWorldRotation() };

    if (axis_ == AL_X || axis_ == AL_NotY ||
        axis_ == AL_NotZ)
        yTo = Vector3::RIGHT;
    if (axis_ == AL_Y || axis_ == AL_NotX ||
        axis_ == AL_NotZ)
        xTo = Vector3::DOWN;
    if (axis_ == AL_None ||
        axis_ == AL_Z || axis_ == AL_NotY)
        xTo = Vector3::FORWARD;
    if (axis_ == AL_None || axis_ == AL_NotX)
        yTo = Vector3::FORWARD;

    if (space_ == TS_View || axis_ == AL_None)
    {
        xTo = camRot * xTo;
        yTo = camRot * yTo;
    }
}

void Transformer::scaleAxes(Vector3& xTo, Vector3& yTo) const
{
    Node* cameraNode{ sceneViewer()->camera()->GetNode() };

    if (axis_ == AL_None)
    {
        xTo = Vector3::ONE;
        yTo = Vector3::ONE;
    }
    else
    {
        if (axis_ == AL_X ||
            axis_ == AL_NotZ || axis_ == AL_NotY)
            xTo = Vector3::RIGHT;
        if (axis_ == AL_Y ||
            axis_ == AL_NotZ || axis_ == AL_NotX)
            yTo = Vector3::UP;
        if (axis_ == AL_Z || axis_ == AL_NotY)
            yTo = Vector3::FORWARD;
        if (axis_ == AL_NotX)
            xTo = Vector3::FORWARD;

        if (space_ == TS_View)
        {
            const Quaternion camRot{ cameraNode->GetWorldRotation() };
            xTo = VectorAbs(camRot * xTo);
            yTo = VectorAbs(camRot * yTo);
        }
    }
}

Vector3 Transformer::flipVector(Node* node, Node* camNode) const
{
    if (space_ == TS_Parent)
        node = node->GetParent();

    Vector3 flip{ Vector3::ONE };
    if (node)
    {
        if (node->GetWorldRight()    .DotProduct(camNode->GetWorldRight())     < 0.f)
            flip.x_ = -1.f;
        if (node->GetWorldUp()       .DotProduct(camNode->GetWorldUp())        < 0.f)
            flip.y_ = -1.f;
        if (node->GetWorldDirection().DotProduct(camNode->GetWorldDirection()) < 0.f)
            flip.z_ = -1.f;
    }

    return flip;
}

SceneViewer* Transformer::sceneViewer() const
{
    return static_cast<SceneViewer*>(parent());
}

//Gizmo::Gizmo(Context* context): Component(context)
//{
//}
