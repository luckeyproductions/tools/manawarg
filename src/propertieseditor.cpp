/* ManaWarg
// Copyright (C) 2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QTimer>
#include "selector.h"

#include "propertieseditor.h"

PropertiesEditor::PropertiesEditor(Context* context, QWidget* parent): FileWidget(context, parent),
    propertiesWidget_{ new PropertiesWidget{ context_, this } },
    typeLabel_{ new QLabel{ "" } }
{
    setObjectName("Properties Editor");

    typeLabel_->setVisible(false);
    typeLabel_->setAlignment(Qt::AlignCenter);
    QFont font{ typeLabel_->font() };
    font.setBold(true);
    typeLabel_->setFont(font);
    propertiesWidget_->setRowWrapPolicy(QFormLayout::WrapAllRows);

    QVBoxLayout* mainLayout{ new QVBoxLayout{} };
    mainLayout->setMargin(2);
    mainLayout->addWidget(typeLabel_);
    mainLayout->addWidget(propertiesWidget_);
    setLayout(mainLayout);

    Selector* selector{ GetSubsystem<Selector>() };
    connect(selector, SIGNAL(activeObjectChanged(Object*)), this, SLOT(activeObjectChanged(Object*)));
}

void PropertiesEditor::activeObjectChanged(Object* object)
{
    if (object)
    {
        typeLabel_->setText(toQString(object->GetTypeName()));
        typeLabel_->setVisible(true);

        if (object->GetTypeInfo()->IsTypeOf<Serializable>())
            propertiesWidget_->setObject(object);
    }
    else
    {
        typeLabel_->setVisible(false);
        propertiesWidget_->setObject(nullptr);
    }
}
