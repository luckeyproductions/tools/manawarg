/* ManaWarg
// Copyright (C) 2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QDesktopServices>
#include <QResizeEvent>
#include <QPushButton>
#include <QMessageBox>
#include <QMenu>
#include <QTimer>
#include <QSettings>
#include "colorwidget.h"
#include "texturewidget.h"
#include "utility.h"
#include "qocoon.h"

#include "materialeditor.h"

MaterialEditor::MaterialEditor(Context* context, QWidget* parent): FileWidget(context, parent),
    materialView_{ new View3D(context_) },
    splitter_{ new FlipSplitter() },
    bottomLayout_{ new QVBoxLayout() },
    bottomSplitter_{ new FlipSplitter(this, 0) },
    propertiesWidget_{  new PropertiesWidget(context_, this) },
    scene_{ nullptr },
    previewModel_{},
    backgroundColor_{ Color::BLACK }
{
    setObjectName("Material Editor");
    completeOperationsText("Material");

    QVBoxLayout* mainLayout{ new QVBoxLayout() };
    mainLayout->setMargin(1);

    createPreviewScene();
    materialView_->setScene(scene_);
    materialView_->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(materialView_, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(showPreviewMenu(const QPoint&)));

    splitter_->setObjectName("materialeditor/splitter");
    splitter_->addWidget(materialView_);
    splitter_->setCollapsible(0, false);
    splitter_->setThreshold(1.23);

    QWidget* bottomWidget{ new QWidget() };
    bottomLayout_->setMargin(1);
    bottomLayout_->setDirection(QBoxLayout::LeftToRight);

    createToolBar();
    bottomLayout_->addLayout(toolBarLayout_);
    bottomLayout_->addWidget(propertiesWidget_);

    createPickerBox();

    bottomWidget->setLayout(bottomLayout_);
    bottomSplitter_->setObjectName("materialeditor/bottomsplitter");
    bottomSplitter_->addWidget(bottomWidget);
    bottomSplitter_->addWidget(pickerBox_);
    bottomSplitter_->setOrientation(Qt::Vertical);
    bottomSplitter_->setStretchFactors(Qt::Horizontal, 1, 4);
    bottomSplitter_->setStretchFactors(Qt::Vertical,   1, 0);
    bottomSplitter_->setThreshold(1.42);

    splitter_->addWidget(bottomSplitter_);
    splitter_->setStretchFactors(Qt::Horizontal, 0, 1);
    splitter_->setStretchFactors(Qt::Vertical,   0, 1);

    mainLayout->addWidget(splitter_);
    setLayout(mainLayout);

    connect(splitter_,       SIGNAL(splitterMoved(int, int)), this, SLOT(updatePickerBoxTitle()));
    connect(bottomSplitter_, SIGNAL(splitterMoved(int, int)), this, SLOT(updatePickerBoxTitle()));
    connect(splitter_,       SIGNAL(orientationChanged(Qt::Orientation)), this, SLOT(updatePickerBoxTitle()));
    connect(bottomSplitter_, SIGNAL(orientationChanged(Qt::Orientation)), this, SLOT(updatePickerBoxTitle()));

    setOrientation(Qt::Vertical);
    setAcceptDrops(true);

    actionNew();
}

MaterialEditor::~MaterialEditor()
{
    QSettings settings{};

    settings.setValue(splitter_->objectName() + "/state", splitter_->saveState());
    settings.setValue(bottomSplitter_->objectName() + "/state", splitter_->saveState());
}

void MaterialEditor::showEvent(QShowEvent*)
{
    if (firstShow_)
    {
        const QSettings settings{};
        const QByteArray splitterState{ settings.value(splitter_->objectName() + "/state").toByteArray() };

        splitter_->restoreState(splitterState);

        firstShow_ = false;
    }
    else
    {
        bool horizontal{ splitter_->orientation() == Qt::Horizontal };
        int max{ horizontal ? splitter_->height() : splitter_->width() };

        if (!splitter_->widget(1)->visibleRegion().isEmpty())
        {
            int sizesSum{ splitter_->sizes()[0] + splitter_->sizes()[1] };

            splitter_->setSizes({ max, sizesSum - max });
        }
    }
}

void MaterialEditor::setOrientation(Qt::Orientation orientation)
{
    FileWidget::setOrientation(orientation);

    if (orientation == Qt::Vertical)
        bottomLayout_->setDirection(QBoxLayout::TopToBottom);
    else
        bottomLayout_->setDirection(QBoxLayout::LeftToRight);
}

void MaterialEditor::createPreviewScene()
{
//    ResourceCache* cache{ GetSubsystem<ResourceCache>() };

    scene_ = new Scene(context_);
    scene_->CreateComponent<Octree>();

    //Zone
    scene_->CreateComponent<Zone>();
    //Skybox
//    Skybox* skybox{ scene_->CreateComponent<Skybox>() };
//    skybox->SetModel(cache->GetResource<Model>("Models/Box.mdl"));
//    skybox->SetMaterial(cache->GetResource<Material>("Materials/Skybox.xml"));
//    skybox->SetEnabled(false);
    //Light
    Node* lightNode{ scene_->CreateChild("Light") };
    lightNode->SetPosition(Vector3(2.0f, 3.0f, 1.0f));
    lightNode->CreateComponent<Light>();
    //Camera
    Node* cameraNode{ scene_->CreateChild("Camera") };
    cameraNode->SetPosition(Vector3::ONE);
    cameraNode->LookAt(Vector3::ZERO);
    cameraNode->CreateComponent<Camera>()->SetFov(36.0f);
    //Preview model
    Node* modelNode{ scene_->CreateChild("Preview") };
    modelNode->SetTemporary(true);
    previewModel_ = modelNode->CreateComponent<StaticModel>();
//    previewModel_->SetModel(cache->GetTempResource<Model>("Models/Triacontahedron.mdl"));
    previewModel_->SetEnabled(false);

    setPreviewModel("Triacontahedron");
}

void MaterialEditor::showPreviewMenu(const QPoint& pos)
{
    QPoint globalPos{ mapToGlobal(pos) };

    QMenu previewMenu{};

    previewMenu.addAction(QIcon(":/Color"), "Background color", this, SLOT(pickBackgroundColor()));
    QMenu* modelMenu{ new QMenu("Preview model") };
    modelMenu->setIcon(QIcon(":/Model"));

    for (QString modelName: { "Cube", "Sphere", "Triacontahedron" })
    {
        QAction* action{ new QAction(modelName, this) };
        action->setCheckable(true);

        if (modelName == "Cube")
            modelName = "Box";

        action->setObjectName(modelName);
        connect(action, SIGNAL(triggered(bool)), this, SLOT(setPreviewModel()));

        Model* currentModel{ previewModel_->GetModel() };

        //Differentiate between default and custom box and sphere
        if ((modelName == "Box"    && previewModel_->GetNode()->GetScale().x_ != .666f)
         || (modelName == "Sphere" && previewModel_->GetNode()->GetScale().x_ != 1.f))
            currentModel = nullptr;

        if (currentModel)
            action->setChecked(currentModel->GetName().Contains(toString(modelName)));

        modelMenu->addActions({ action });
    }

    previewMenu.addMenu(modelMenu);

    QAction* continuousAction{ new QAction("Continuous update", this) };
    continuousAction->setCheckable(true);
    continuousAction->setChecked(materialView_->continuousUpdate());
    connect(continuousAction, SIGNAL(triggered(bool)), materialView_, SLOT(toggleContinuousUpdate()));
    previewMenu.addAction(continuousAction);

    previewMenu.exec(globalPos);
}

void MaterialEditor::pickBackgroundColor()
{
    if (pickerBox_)
    {
        QLayout* pickerLayout{ pickerBox_->layout() };
        QLayoutItem* item{ pickerLayout->itemAt(0) };

        if (item)
        {
            pickerLayout->removeItem(item);
            delete item->widget();
        }

        ColorPicker* backgroundPicker{ new ColorPicker(backgroundColor_) };
        pickerLayout->addWidget(backgroundPicker);
        QString colorName{ "Preview Background Color" };
        pickerBox_->setObjectName(colorName);
        pickerBox_->show();
        updatePickerBoxTitle();

        connect(backgroundPicker, SIGNAL(colorChanged(const Color&)), this, SLOT(updateBackgroundColor(const Color&)));
    }
}

void MaterialEditor::setPreviewModel()
{
    setPreviewModel(toString(sender()->objectName()));
}

void MaterialEditor::setPreviewModel(String modelName)
{
    if (modelName.IsEmpty())
        return;

    bool customModel{ false };
    Node* previewNode{ previewModel_->GetNode() };

    if (!modelName.Contains('.'))
    {
        previewNode->SetScale(modelName == "Box" ? .666f : 1.f);
        modelName = "Models/" + modelName + ".mdl";
    }
    else
    {
        customModel = true;
    }

    previewNode->SetTags(customModel ? StringVector{} : StringVector{ TAG_DEFAULT });
    previewModel_->SetModel(GetSubsystem<ResourceCache>()->GetTempResource<Model>(modelName));
    setMaterial(material_);

    if (customModel)
    {
        previewNode->SetScale(M_1_SQRT3 / previewModel_->GetBoundingBox().HalfSize().Length());
        previewNode->SetRotation(Quaternion::IDENTITY);
        previewNode->SetPosition(-previewModel_->GetBoundingBox().Center() * previewNode->GetScale().x_);
    }
    else
    {
        previewNode->SetPosition(Vector3::ZERO);
    }

    materialView_->updateView();
}

void MaterialEditor::setMaterial(Material* material)
{
    if (material_ == material)
        return;

//    pickerBox_->hide();
    material_ = material;
    previewModel_->SetMaterial(material_);
    previewModel_->SetEnabled(material_);
    propertiesWidget_->setObject(material);
    materialView_->updateView();
}

void MaterialEditor::setTechnique(Technique* technique)
{
    material_->SetTechnique(0, technique);
    materialView_->updateView();
}

bool MaterialEditor::openMaterial(const String& materialFileName) //Needs generalization for XML-based resources
{
    if (materialFileName.IsEmpty())
        return false;

    ResourceCache* cache{ GetSubsystem<ResourceCache>() };
    SharedPtr<XMLFile> xmlFile{ cache->GetTempResource<XMLFile>(materialFileName) };

    if (xmlFile)
    {
        const XMLElement& rootElem{ xmlFile->GetRoot("material") };

        if (rootElem.IsNull()) // XML does not seem to be a material
        {
            QString elemName{ toQString(xmlFile->GetRoot().GetName()) };
            QString message{ "Could not load " };

            if (!elemName.isEmpty())
                message += elemName.append(" as ");

            message += "material";

            QMessageBox* messageBox{ new QMessageBox(QMessageBox::Warning, "That failed", message )};
            messageBox->setWindowIcon(QIcon(":/Icon"));
            messageBox->exec();

            return false;
        }

        if (Qocoon::project()->inResourceFolder(materialFileName))
        {
            SharedPtr<Material> material{ GetSubsystem<ResourceCache>()->GetResource<Material>(materialFileName) };

            if (material)
            {
                String materialName{ Qocoon::trimmedResourceName(materialFileName) };
                GetSubsystem<ResourceCache>()->ReloadResource(material);
                setMaterial(material);
                fileName_ = materialFileName;
                updateTitle();
                raiseDockWidget();

                return true;
            }
        }
        else
        {
            if (Qocoon::project() == nullptr &&
                !Qocoon::locateResourceRoot(materialFileName).IsEmpty())
            {
                return openMaterial(materialFileName);
            }
            else // Resource not in project tree, what to do? Add folder/Switch project | Copy resource file
            {
                QMessageBox* messageBox{ new QMessageBox(QMessageBox::Warning, "Out of scope",
                                                         "Material exists outside current resource structure")};
                messageBox->setWindowIcon(QIcon(":/Icon"));
                messageBox->exec();
            }
        }
    }

    return false;
}

void MaterialEditor::actionNew()
{
    /// Check for changes

    SharedPtr<Material> newMat{ new Material(context_) };
    newMat->SetShaderParameter("MatDiffColor", Color::WHITE);
    newMat->SetShaderParameter("MatSpecColor", Color{ .25f, .25f, .25f, 5.f });
    newMat->SetShaderParameter("MatEmissiveColor", Color::BLACK);
    setMaterial(newMat);

    FileWidget::actionNew();

    materialView_->updateView();
}

void MaterialEditor::actionOpen()
{
    Qocoon* qocoon{ GetSubsystem<Qocoon>() };
    const String projectFolder{ qocoon->projectLocation() };
    const QString homeFolder{ QStandardPaths::writableLocation(QStandardPaths::HomeLocation) };
    QString xmlFilter{ tr("*.xml") };
    const String materialFileName{ toString(QFileDialog::getOpenFileName(
                                          this, tr("Open Material"), qocoon->project()
                                          ? toQString(projectFolder + Qocoon::project()->resourceFolders().Front())
                                          : homeFolder, xmlFilter, &xmlFilter)) };
    openMaterial(materialFileName);
}

void MaterialEditor::actionSave(bool saveAs)
{
    if (!material_)
        return;

    if (fileName_.IsEmpty() || saveAs)
    {
        Qocoon* qocoon{ GetSubsystem<Qocoon>() };
        const String projectFolder{ qocoon->projectLocation() };
        const QString homeFolder{ QStandardPaths::writableLocation(QStandardPaths::HomeLocation) };
        QString xmlFilter{ tr("XML Files (*.xml)") };
        const String materialFileName{ toString(QFileDialog::getSaveFileName(
                                              this, tr("Save Material As..."), qocoon->project()
                                              ? toQString(projectFolder + Qocoon::project()->resourceFolders().Front())
                                              : homeFolder, xmlFilter)) };
        if (!materialFileName.IsEmpty())
        {
            material_->SetName(materialFileName);
            fileName_ = materialFileName;
        }
        else
        {
            return;
        }
    }

    ResourceCache* cache{ GetSubsystem<ResourceCache>() };
    const String fullName{ cache->GetResourceFileName(material_->GetName()) };
    if (fullName.IsEmpty())
        return;

    File saveFile{ context_, fullName, FILE_WRITE };
    material_->Save(saveFile);
}

void MaterialEditor::updateMaterial(QObject* sender)
{
    String parameterName{ toString(sender->property("parameter").toString()) };

    if (auto cw = qobject_cast<ColorWidget*>(sender))
        material_->SetShaderParameter(parameterName, Vector4{ cw->color().Data() });
    else if (auto tw = qobject_cast<TextureWidget*>(sender))
        material_->SetTexture(tw->textureUnit(), tw->texture());

    materialView_->updateView();
}

void MaterialEditor::updateBackgroundColor(const Color& color)
{
    if (!scene_ || backgroundColor_ == color)
        return;

    backgroundColor_ = color;
    scene_->GetComponent<Zone>()->SetFogColor(backgroundColor_);
//    materialView_->setRenderPath("RenderPaths/Forward" + String(backgroundColor_.a_ == 1.0f ? ".xml" : "Transparent.xml"));
    materialView_->updateView();
}

void MaterialEditor::dragEnterEvent(QDragEnterEvent* event)
{
    const QMimeData* mime{ event->mimeData() };

    if (isOfType<Material>(mime)
     || isOfType<Model>(mime))
    {
        event->acceptProposedAction();
    }
}

void MaterialEditor::dropEvent(QDropEvent* event)
{
    const QMimeData* mime{ event->mimeData() };
    const String fileName{ toString(extractDataMap(mime)[FileName].toString()) };

    if (isOfType<Material>(mime))
        openMaterial(fileName);
    else if (isOfType<Model>(mime))
        setPreviewModel(fileName);
}
