/* ManaWarg
// Copyright (C) 2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QDoubleSpinBox>
#include <QCheckBox>
#include <QComboBox>
#include <QDialog>

#include "materialeditor.h"
#include "colorwidget.h"
#include "texturewidget.h"
#include "propertieswidget.h"
#include "numberwidget.h"
#include "resourcewidgets.h"

#include "propertyrow.h"
#include "utility.h"
#include "qocoon.h"

PropertyRow::PropertyRow(const QString& text, PropertyType type, PropertiesWidget* parent): QWidget(parent),
    propertiesWidget_{ parent },
    propertyWidgets_{},
    type_{ type },
    resourceType_{ StringHash::ZERO },
    list_{ false },
    value_{},
    label_{ new QLabel{ text } },
    layout_{ new QHBoxLayout{} }
{
    label_->setObjectName(text);
    label_->setMinimumWidth(8);

    if (isNumberType())
        createNumberWidget();
    else
    switch (type_)
    {
    case PT_None: default:
    break;
    case PT_Technique:
        createTechniqueWidgets();
    break;
    case PT_Color: case PT_ColorAndTexture:
        createColorWidget(text);

        if (type_ == PT_Color)
            break;
        else
            [[fallthrough]];
    case PT_Texture:
        createTextureWidget(text);
    break;
    case PT_Bool:
        createBoolWidget();
    break;
    case PT_String:
        createStringWidget();
    break;
    case PT_Enum:
        createEnumWidget();
    break;
    }
}

PropertyRow::PropertyRow(const QString& text, StringHash resourceType, bool list, PropertiesWidget* parent): PropertyRow(text, PT_Resource, parent)
{
    resourceType_ = resourceType;
    list_ = list;

    if (!list_)
    {
        if (resourceType_ == Texture2D::GetTypeStatic())
            createTextureWidget();
        else
            createStringWidget();
    }
    else
    {
        type_ = PT_ResourceList;

        ResourceListWidget* listWidget{ new ResourceListWidget{ resourceType_, this } };
        listWidget->setAttributeName(text);
        listWidget->updateNumResources(parent->serializable());
        propertyWidgets_[type_] = listWidget;
        layout_->addWidget(listWidget);

        connect(listWidget, SIGNAL(editingFinished()), this, SLOT(updateValue()));
        connect(propertiesWidget_, SIGNAL(subAttributeCountChanged(Serializable*)), listWidget, SLOT(updateNumResources(Serializable*)));
    }
}

QWidget* PropertyRow::propertyWidget(PropertyType propertyType) const
{
    if (type_ == PT_Resource)
    {
        if (resourceType_ == Texture2D::GetTypeStatic())
            return propertyWidgets_.at(PT_Texture);
        else
            return propertyWidgets_.at(PT_String);
    }
    else
    {
        return propertyWidgets_.at(propertyType);
    }
}

void PropertyRow::setOptions(const QStringList& options)
{
    if (type_ != PT_Enum)
        return;

    QComboBox* comboBox{ static_cast<QComboBox*>(propertyWidget(type_)) };
    comboBox->blockSignals(true);
    comboBox->clear();
    comboBox->addItems(options);
    comboBox->blockSignals(false);
}

void PropertyRow::createBoolWidget()
{
    QCheckBox* checkBox{ new QCheckBox(this) };

    propertyWidgets_[PT_Bool] = checkBox;
    checkBox->setObjectName(label_->text());
    layout_->addWidget(checkBox);

    connect(checkBox, SIGNAL(toggled(bool)), this, SLOT(updateValue()));
}

void PropertyRow::createNumberWidget()
{
    NumberWidget* numberWidget{ new NumberWidget{ type_, this } };

    propertyWidgets_[type_] = numberWidget;
    layout_->addWidget(numberWidget);
}

void PropertyRow::createStringWidget()
{
    QLineEdit* stringWidget{ nullptr };
    if (type_ == PT_Resource)
        stringWidget = new ResourceLineEdit{ resourceType_, this };
    else
        stringWidget = new QLineEdit{ this };

    propertyWidgets_[PT_String] = stringWidget;
    layout_->addWidget(stringWidget);

    connect(stringWidget, SIGNAL(editingFinished()), this, SLOT(updateValue()));
}

void PropertyRow::createEnumWidget()
{
    QComboBox* enumWidget{ new QComboBox{ this } };

    propertyWidgets_[PT_Enum] = enumWidget;
    layout_->addWidget(enumWidget);

    connect(enumWidget, SIGNAL(currentIndexChanged(int)), this, SLOT(updateValue()));
}

void PropertyRow::createTechniqueWidgets()
{
    QLineEdit* techniqueEdit{ new QLineEdit{} };

    propertyWidgets_[PT_Technique] = techniqueEdit;
    techniqueEdit->setReadOnly(true);
    techniqueEdit->setAlignment(Qt::AlignCenter);
    layout_->addWidget(techniqueEdit);

    QPushButton* openResourceButton{ new QPushButton{} };
    openResourceButton->setIcon(QIcon(":/Open"));
    openResourceButton->setToolTip("Pick technique");
    layout_->addWidget(openResourceButton);

    connect(openResourceButton, SIGNAL(clicked(bool)), propertiesWidget_, SLOT(pickTechnique()));
}

void PropertyRow::createColorWidget(const QString& text)
{
    ColorWidget* colorWidget{ new ColorWidget{ text, this } };

    propertyWidgets_[PT_Color] = colorWidget;
    layout_->addWidget(colorWidget);
    layout_->setStretchFactor(colorWidget, 0);
    layout_->addSpacerItem(new QSpacerItem{ 0, 0, QSizePolicy::MinimumExpanding });

    connect(colorWidget, SIGNAL(colorChanged()), propertiesWidget_, SLOT(propertyModified()));
    connect(colorWidget, SIGNAL(colorPicked()),  this, SLOT(updateValue()));
}

void PropertyRow::createTextureWidget(const QString& unitName)
{
    const TextureUnit textureUnit{ unitFromName(toString(unitName)) };
    TextureWidget* textureWidget{ new TextureWidget{ textureUnit } };

    propertyWidgets_[PT_Texture] = textureWidget;
    if (!unitName.isEmpty())
    {
        textureWidget->setProperty("name", unitName + " Texture");
        textureWidget->setProperty("parameter", unitName.toLower());
    }

    if (!layout_->count() && resourceType_ == StringHash::ZERO)
        layout_->addSpacerItem(new QSpacerItem{ 34, 0, QSizePolicy::MinimumExpanding });

    layout_->addWidget(textureWidget);
    layout_->setStretchFactor(textureWidget, 1);

    connect(textureWidget, SIGNAL(textureChanged()), propertiesWidget_, SLOT(propertyModified()));
}

TextureUnit PropertyRow::unitFromName(String name)
{
    if (name.IsEmpty())
        return MAX_TEXTURE_UNITS;

    name = name.ToLower();

    for (unsigned t{ 0 }; t < textureUnitNames.size(); ++t)
    {
        if (textureUnitNames[t] == name)
            return static_cast<TextureUnit>(t);
    }

    return MAX_TEXTURE_UNITS;
}

bool PropertyRow::isNumberType()
{
    return type_ == PT_Float
        || type_ == PT_Int
        || type_ == PT_Vector2
        || type_ == PT_Vector3
        || type_ == PT_Vector4
        || type_ == PT_IntVector2
        || type_ == PT_IntVector3
        || type_ == PT_Quaternion;
}

bool PropertyRow::isFloatNumber(PropertyType type)
{
    return type == PT_Float
        || type == PT_Vector2
        || type == PT_Vector3
        || type == PT_Vector4
        || type == PT_Quaternion;
}

bool PropertyRow::setValue(const Variant& value)
{
    const PropertyType valueType{ PropertiesWidget::toPropertyType(value.GetType()) };
    if (type_ == PT_Enum)
    {
        if (valueType != PT_Int)
            return false;
    }
    else if (type_ != valueType)
    {
        return false;
    }

    value_ = value;

    if (isNumberType())
    {
        updateSpinBoxes();
    }
    else
    {
        switch (type_)
        {
        case PT_None:   default:              break;
        case PT_Color:  updateColorWidget();  break;
        case PT_Bool:   updateBoolWidget();   break;
        case PT_String: updateStringWidget(); break;
        case PT_Enum:   updateEnumWidget();   break;
        case PT_Resource:
        {
            if (resourceType_ == Texture2D::GetTypeStatic())
            {
//                updateTextureWidget();
            }
            else
            {
                updateStringWidget();
            }
        }
        break;
        case PT_ResourceList: updateResourceListWidget(); break;
        }
    }

    return true;
}

void PropertyRow::updateColorWidget()
{
    if (ColorWidget* colorWidget{ static_cast<ColorWidget*>(propertyWidget(type_)) })
        colorWidget->setColor(value_.GetColor());
}

void PropertyRow::updateBoolWidget()
{
    if (QCheckBox* checkBox{ static_cast<QCheckBox*>(propertyWidget(type_)) })
        checkBox->setChecked(value_.GetBool());
}

void PropertyRow::updateStringWidget()
{
    if (QLineEdit* lineEdit{ static_cast<QLineEdit*>(propertyWidget(PT_String)) })
    {
        if (type_ == PT_String)
            lineEdit->setText(toQString(value_.GetString()));
        else if (type_ == PT_Resource)
            lineEdit->setText(toQString(value_.GetResourceRef().name_));
    }
}

void PropertyRow::updateEnumWidget()
{
    if (QComboBox* comboBox{ static_cast<QComboBox*>(propertyWidget(type_)) })
        comboBox->setCurrentIndex(value_.GetInt());
}

void PropertyRow::updateResourceListWidget()
{
    if (ResourceListWidget* listWidget{ static_cast<ResourceListWidget*>(propertyWidget(type_)) })
        listWidget->setValues(value_.GetResourceRefList());
}

void PropertyRow::updateSpinBoxes()
{
    if (NumberWidget* numberWidget{ static_cast<NumberWidget*>(propertyWidget(type_)) })
       numberWidget->setValue(value_);
}

void PropertyRow::updateValue()
{
    QWidget* widget{ propertyWidget(type_) };
    const String attributeName{ toString(objectName()) };

    if (isNumberType())
    {
        if (NumberWidget* numberWidget{ static_cast<NumberWidget*>(widget) })
        {
            if (type_ == PT_Quaternion)
                numberWidget->setValue(numberWidget->value());

            const Variant numberValue{ numberWidget->value() };

            if (value_ != numberValue)
            {
                value_ = numberValue;
                propertiesWidget_->propertyModified(attributeName);
            }
        }
    }
    else
    {
        switch (type_)
        {
        default: case PT_None: return;
        case PT_Color:
            value_ = static_cast<ColorWidget*>(widget)->color();
        break;
        case PT_Bool:
            value_ = static_cast<QCheckBox*>(widget)->isChecked();
        break;
        case PT_String:
            value_ = toString(static_cast<QLineEdit*>(widget)->text());
        break;
        case PT_Enum:
            value_ = static_cast<QComboBox*>(widget)->currentIndex();
        break;
        case PT_Resource:
            if (resourceType_ == Texture2D::GetTypeStatic())
                value_ = ResourceRef{ resourceType_, static_cast<TextureWidget*>(widget)->texture()->GetName() };
            else
                value_ = ResourceRef{ resourceType_, toString(static_cast<QLineEdit*>(widget)->text()) };
        break;
        case PT_ResourceList:
            value_ = static_cast<ResourceListWidget*>(widget)->refList();
        break;
        }

        propertiesWidget_->propertyModified(attributeName);
    }
}
