/* ManaWarg
// Copyright (C) 2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef RESOURCEPREVIEW_H
#define RESOURCEPREVIEW_H

#include <QTreeWidget>
#include <QLabel>
#include <QPushButton>
#include <QTimer>
#include <QFileSystemWatcher>

#include "../view3d.h"

#include <QObject>

enum PreviewType{ RT_None, RT_Image, RT_Material, RT_Model, RT_Particles, RT_Prefab, RT_Sound, RT_Font };

class ResourcePreview: public QWidget, public Object
{
    Q_OBJECT
    DRY_OBJECT(ResourcePreview, Object);

public:
    ResourcePreview(Context* context, QWidget* parent = nullptr);
    void setResource(Resource* resource);

public slots:
    void changeResource(QTreeWidgetItem* item);
    void playSound();

protected:
    void resizeEvent(QResizeEvent* event) override;

private slots:
    void soundButtonToggled(bool checked);
    void soundFinished();

    void handleResourceChanged();
    void reloadResource();

private:
    void createPreviewScene();

    void setImage(const QString& fileName, bool reload = false);
    void setMaterial(const QString& fileName, bool reload = false);
    void setModel(const QString& fileName, bool reload = false);
    void setParticles(const QString& fileName, bool reload = false);
    void setPrefab(const QString& fileName, bool reload = false);
    void setSound(const QString& fileName);
    void setFont(const QString& fileName);

    void updatePreviewPixmap(bool pixmapChanged);
    void updatePreviewFontSize();

    View3D* view3d_;
    QLabel* previewLabel_;
    QPixmap previewPixmap_;
    QPushButton* soundButton_;
    QTimer* soundTimer_;

    SharedPtr<Resource> resource_;
    Node* previewNode_;
    Node* prefabNode_;
    AnimatedModel* previewModel_;
    ParticleEmitter* previewEmitter_;
    SoundSource* previewSoundSource_;
    PreviewType previewType_;
    QFileSystemWatcher resourceWatcher_;
    int previewFontId_;

    QTimer reloadTimer_;
};


#endif // RESOURCEPREVIEW_H
