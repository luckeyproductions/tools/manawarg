/* ManaWarg
// Copyright (C) 2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QStyle>
#include <QHeaderView>
#include <QMenu>
#include <QSettings>
#include "../qocoon.h"
#include "../utility.h"
#include "../project.h"
#include "../materialeditor.h"
#include "../texteditor.h"

#include "resourcebrowser.h"

ResourceBrowser::ResourceBrowser(Context* context, QWidget* parent): FileWidget(context, parent),
    browser_{ new QWidget{ this } },
    treeViewWidget_{ new QWidget{ this } },
    treeWidget_{ nullptr },
    iconViewWidget_{ new QWidget{ this } },
    iconWidget_{ nullptr },
    splitter_{ new FlipSplitter{ this, 1 } },
    resourcePreview_{ new ResourcePreview{ context, this } },
    folderWatcher_{}
{
    setObjectName("Resource Browser");

    QVBoxLayout* mainLayout{ new QVBoxLayout{ this } };
    mainLayout->setMargin(0);

    QVBoxLayout* browserLayout{ new QVBoxLayout{} };
    browserLayout->setMargin(2);
    browser_->setLayout(browserLayout);

    createTreeViewWidget();
    createIconViewWidget();

    splitter_->setObjectName("resourcebrowser/splitter");
    splitter_->addWidget(browser_);
    splitter_->addWidget(resourcePreview_);
    splitter_->setStretchFactors(Qt::Horizontal, 1, 0);
    splitter_->setStretchFactors(Qt::Vertical,   3, 1);

    mainLayout->addWidget(splitter_);
    setLayout(mainLayout);

    refreshTreeView();

    Qocoon* qocoon{ GetSubsystem<Qocoon>() };
    connect(&folderWatcher_, SIGNAL(directoryChanged(const QString&)), SLOT(refreshTreeView()));
    connect(qocoon, SIGNAL(resourceFoldersChanged()), SLOT(refreshTreeView()));
    connect(qocoon, SIGNAL(currentProjectChanged()), SLOT(refreshTreeView()));
}

ResourceBrowser::~ResourceBrowser()
{
    QSettings settings{};
    settings.setValue(splitter_->objectName() + "/geometry", splitter_->saveState());
}

void ResourceBrowser::showEvent(QShowEvent*)
{
    if (firstShow_)
    {
        const QSettings settings{};
        const QByteArray stateBytes{ settings.value(splitter_->objectName() + "/geometry").toByteArray() };

        if (!stateBytes.isNull())
        {
            splitter_->restoreState(stateBytes);
        }

        firstShow_ = false;
    }
    else
    {
        bool horizontal{ splitter_->orientation() == Qt::Horizontal };
        int max{ horizontal ? height() : width() };

        if (!splitter_->widget(0)->visibleRegion().isEmpty()
            && !splitter_->widget(1)->visibleRegion().isEmpty())
        {
            int sizesSum{ splitter_->sizes()[0] + splitter_->sizes()[1] };

            splitter_->setSizes({ sizesSum - max, max });
        }
    }
}

void ResourceBrowser::showBrowserMenu(const QPoint& pos)
{
    if (!Qocoon::project())
        return;

    QMenu browserMenu{};
    QTreeWidgetItem* item{ treeWidget_->itemAt(pos) };

    if (item && !item->parent())
    {
        if (browserMenu.addAction(QIcon(":/Delete"), "Dismiss resource folder")
            == browserMenu.exec(mapToGlobal(pos)))
        {
            String path{ toString(item->data(0, FileName).toString()) };
            Qocoon::project()->removeResourceFolder(path);
        }
    }
}

void ResourceBrowser::createTreeViewWidget()
{
    QVBoxLayout* treeViewLayout{ new QVBoxLayout{ treeViewWidget_ } };
    treeViewLayout->setMargin(0);
    treeWidget_ = new QTreeWidget{};
    treeWidget_->setMinimumSize(64, 64);
    treeWidget_->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    treeWidget_->setTextElideMode(Qt::ElideNone);
    treeWidget_->setHeaderHidden(true);
//    treeWidget_->setHeaderLabels({"", "Type"});
//    treeWidget_->setColumnCount(2); //For some reason breaks drag 'n drop
//    treeWidget_->setColumnWidth(0, 256);
    treeWidget_->header()->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
    treeWidget_->header()->setSectionResizeMode(QHeaderView::ResizeToContents);
    treeWidget_->header()->setStretchLastSection(false);
    treeWidget_->setDragEnabled(true);

    treeViewLayout->addWidget(treeWidget_);
    treeViewWidget_->setLayout(treeViewLayout);
    treeWidget_->setContextMenuPolicy(Qt::CustomContextMenu);

    connect(treeWidget_, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(showBrowserMenu(const QPoint&)));
    connect(treeWidget_, SIGNAL(itemExpanded(QTreeWidgetItem*)), SLOT(updateFolderIcon(QTreeWidgetItem*)));
    connect(treeWidget_, SIGNAL(itemCollapsed(QTreeWidgetItem*)), SLOT(updateFolderIcon(QTreeWidgetItem*)));
    connect(treeWidget_, SIGNAL(currentItemChanged(QTreeWidgetItem*, QTreeWidgetItem*)),
            resourcePreview_, SLOT(changeResource(QTreeWidgetItem*)));
    connect(treeWidget_, SIGNAL(itemActivated(QTreeWidgetItem*, int)), this, SLOT(itemActivated(QTreeWidgetItem*)));

    browser_->layout()->addWidget(treeViewWidget_);
}

void ResourceBrowser::createIconViewWidget()
{
    QVBoxLayout* iconViewLayout{ new QVBoxLayout{ iconViewWidget_ } };
    iconViewLayout->setMargin(0);
    iconWidget_ = new QListWidget{};

    iconViewLayout->addWidget(iconWidget_);
    iconViewWidget_->setLayout(iconViewLayout);
    browser_->layout()->addWidget(iconViewWidget_);

    iconViewWidget_->hide();
}

void ResourceBrowser::refreshTreeView()
{
    QStringList expandedFolders;
    QTreeWidgetItem* selectedItem{ treeWidget_->currentItem() };
    const QString selected{ (selectedItem ? selectedItem->data(0, FileName).toString() : "" ) };
    const QStringList expanded{ expandedPaths() };

    prefabs_.clear();
    treeWidget_->clear();

    if (!Qocoon::project())
        return;

    if (!folderWatcher_.directories().isEmpty())
        folderWatcher_.removePaths(folderWatcher_.directories());

    QList<QTreeWidgetItem*> resourceFolderItems{};
    const StringVector& folders{ Qocoon::project()->resourceFolders() };

    for (const String& path: folders)
    {
        if (path.IsEmpty())
            continue;

        QString displayName{ toQString(GetFileName(RemoveTrailingSlash(path))) };
        String fullPath{ path };

        if (IsAbsolutePath(Qocoon::project()->localizedPath(path)))
            displayName.append(" [External]");
        else
            fullPath = Qocoon::project()->absoluteResourceFolder(fullPath);

        folderWatcher_.addPath(toQString(fullPath));

        QTreeWidgetItem* resourceFolderItem{ new QTreeWidgetItem{} };
        resourceFolderItem->setText(0, displayName);
        resourceFolderItem->setIcon(0, QIcon{ ":/Folder" });
        resourceFolderItem->setData(0, FileName, QVariant{ toQString(fullPath) });
        resourceFolderItem->setToolTip(0, toQString(fullPath));
        resourceFolderItem->setFlags(resourceFolderItem->flags() ^ Qt::ItemIsDragEnabled );

        buildTree(resourceFolderItem);

        resourceFolderItems.append(resourceFolderItem);
    }

    treeWidget_->addTopLevelItems(resourceFolderItems);

    for (QTreeWidgetItem* item: allItems())
    {
        const QString fileName{ item->data(0, FileName).toString() };
        if (expanded.contains(fileName))
            item->setExpanded(true);

        if (fileName == selected)
            treeWidget_->setCurrentItem(item);
    }

    GetSubsystem<Qocoon>()->updatePrefabMenu(prefabs_);

    /// Restore scroll position
}

void ResourceBrowser::buildTree(QTreeWidgetItem* treeItem)
{
    FileSystem* fs{ GetSubsystem<FileSystem>() };
    const String fullPath{ toString(treeItem->data(0, FileName).toString()) };

    Vector<String> scanResults{};
    fs->ScanDir(scanResults, fullPath, "*.*", SCAN_DIRS, false);
    Sort(scanResults.Begin(), scanResults.End());

    for (const String& subFolder: scanResults)
    {
        if (subFolder.Front() == '.')
            continue;

        const QString fullSubPath{ toQString(AddTrailingSlash(AddTrailingSlash(fullPath) + subFolder)) };
        folderWatcher_.addPath(fullSubPath);

        QTreeWidgetItem* subFolderItem{ new QTreeWidgetItem{} };

        subFolderItem->setText(0, toQString(subFolder));
        subFolderItem->setIcon(0, QIcon(":/Folder"));
        subFolderItem->setData(0, FileName, QVariant(fullSubPath));
        subFolderItem->setFlags(subFolderItem->flags() ^ Qt::ItemIsDragEnabled );
        subFolderItem->setToolTip(0, toQString(Qocoon::trimmedResourceName(AddTrailingSlash(fullPath) + subFolder)));

        buildTree(subFolderItem);
        treeItem->addChild(subFolderItem);
    }

    fs->ScanDir(scanResults, fullPath, "*.*", SCAN_FILES, false);
    Sort(scanResults.Begin(), scanResults.End());

    for (const String& fileName: scanResults)
    {
        QTreeWidgetItem* fileItem{ new QTreeWidgetItem{} };
        const String fullFileName{ AddTrailingSlash(fullPath) + fileName };
        const String extension{ GetExtension(fileName) };
        QString iconName{ ":/New" };

        fileItem->setText(0, toQString(fileName));
        fileItem->setToolTip(0, toQString(Qocoon::trimmedResourceName(fullFileName)));

        if (extension == ".png" || extension == ".jpg" || extension == ".dds")
        {
            fileItem->setData(0, DryTypeHash, QVariant(Texture2D::GetTypeStatic().ToHash()));
            fileItem->setText(1, "Image");
            iconName = ":/Image";
        }
        else if (extension == ".ogg" || extension == ".wav")
        {
            fileItem->setData(0, DryTypeHash, QVariant{ Sound::GetTypeStatic().ToHash() });
            fileItem->setText(1, "Sound");
            iconName = ":/Sound";
        }
        else if (extension == ".mdl")
        {
            fileItem->setData(0, DryTypeHash, QVariant{ Model::GetTypeStatic().ToHash() });
            fileItem->setText(1, "Model");
            iconName = ":/Model";
        }
        else if (extension == ".ani")
        {
            fileItem->setData(0, DryTypeHash, QVariant{ Animation::GetTypeStatic().ToHash() });
            fileItem->setText(1, "Animation");
            iconName = ":/Animation";
        }
        else if (extension == ".as")
        {
            fileItem->setData(0, DryTypeHash, QVariant{ ScriptFile::GetTypeStatic().ToHash() });
            fileItem->setText(1, "Script");
            iconName = ":/Script";
        }
        else if (extension == ".otf" || extension == ".ttf")
        {
            fileItem->setData(0, DryTypeHash, QVariant{ Font::GetTypeStatic().ToHash() });
            fileItem->setText(1, "Font");
            iconName = ":/Font";
        }
        else if (extension == ".json")
        {
            fileItem->setData(0, DryTypeHash, QVariant{ JSONFile::GetTypeStatic().ToHash() });
            fileItem->setText(1, "JSON File");
        }
        else if (extension == ".xml")
        {
            SharedPtr<XMLFile> xmlFile{ GetSubsystem<ResourceCache>()->GetTempResource<XMLFile>(fullFileName) };

            if (xmlFile)
            {
                String rootName{ xmlFile->GetRoot().GetName() };

                if (rootName == "material")
                {
                    fileItem->setData(0, DryTypeHash, QVariant{ Material::GetTypeStatic().ToHash() });
                    fileItem->setText(1, "Material");
                    iconName = ":/Material";
                }
                else if (rootName == "technique")
                {
                    fileItem->setData(0, DryTypeHash, QVariant{ Technique::GetTypeStatic().ToHash() });
                    fileItem->setText(1, "Technique");
                }
                else if (rootName == "scene")
                {
                    fileItem->setData(0, DryTypeHash, QVariant{ Scene::GetTypeStatic().ToHash() });
                    fileItem->setText(1, "Scene");
                    iconName = ":/SceneResource";
                }
                else if (rootName == "node")
                {
                    fileItem->setData(0, DryTypeHash, QVariant{ Node::GetTypeStatic().ToHash() });
                    fileItem->setText(1, "Prefab");
                    prefabs_.push_back(toQString(fullFileName));
                }
                else if (rootName == "particleeffect")
                {
                    fileItem->setData(0, DryTypeHash, QVariant{ ParticleEffect::GetTypeStatic().ToHash() });
                    fileItem->setText(1, "Particle Effect");
                    iconName = ":/Particles";
                }
                else
                {
                    fileItem->setData(0, DryTypeHash, QVariant{ XMLFile::GetTypeStatic().ToHash() });
                    fileItem->setText(1, "XML File");
                }
            }
        }
        else
        {
            fileItem->setData(0, DryTypeHash, QVariant{ File::GetTypeStatic().ToHash() });
        }

        fileItem->setData(0, FileName, QVariant{ toQString(fullFileName) });
        fileItem->setIcon(0, QIcon{ iconName });

        treeItem->addChild(fileItem);
    }
}

QStringList ResourceBrowser::expandedPaths() const
{
    QStringList expandedPaths{};
    for (QTreeWidgetItem* item: allItems())
    {
        if (item->isExpanded())
            expandedPaths.push_back(item->data(0, FileName).toString());
    }

    return expandedPaths;
}

std::vector<QTreeWidgetItem*> ResourceBrowser::allItems() const
{
    std::vector<QTreeWidgetItem*> treeItems{};

    for (int i{ 0 }; i < treeWidget_->topLevelItemCount(); ++i)
    {
        if (QTreeWidgetItem* item{ treeWidget_->topLevelItem(i) })
        {
            collectChildren(treeItems, item, true);
            treeItems.push_back(item);
        }
    }

    return treeItems;
}

void ResourceBrowser::collectChildren(std::vector<QTreeWidgetItem*>& children, QTreeWidgetItem* item, bool recursive) const
{
    for (int c{ 0 }; c < item->childCount(); ++c)
    {
        QTreeWidgetItem* childItem{ item->child(c) };
        children.push_back(childItem);

        if (recursive)
            collectChildren(children, childItem, true);
    }
}

void ResourceBrowser::updateFolderIcon(QTreeWidgetItem* item)
{
    item->setIcon(0, QIcon{ ":/" + QString{ item->isExpanded() ? "Open" : "Folder" } });
}

void ResourceBrowser::itemActivated(QTreeWidgetItem* item)
{
    const String fileName{ toString(item->data(0, FileName).toString()) };

    if (isOfType<Material>(item))
        GetSubsystem<MaterialEditor>()->openMaterial(fileName);
    else if (isOfType<Model>(item))
        GetSubsystem<MaterialEditor>()->setPreviewModel(fileName);
    else if (isOfType<Scene>(item))
        GetSubsystem<Qocoon>()->openScene(fileName);
    else if (isOfType<Sound>(item))
        resourcePreview_->playSound();
    else if (isOfType<ScriptFile>(item) ||
             isOfType<XMLFile>(item)    ||
             isOfType<JSONFile>(item)   ||
             isOfType<File>(item))
        GetSubsystem<TextEditor>()->openFile(fileName);
}
