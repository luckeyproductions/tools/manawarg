/* ManaWarg
// Copyright (C) 2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef RESOURCEBROWSER_H
#define RESOURCEBROWSER_H

#include <QListWidget>

#include "resourcepreview.h"

#include "../filewidget.h"

class ResourceBrowser: public FileWidget
{
    Q_OBJECT
    DRY_OBJECT(ResourceBrowser, FileWidget);

public:
    explicit ResourceBrowser(Context* context, QWidget* parent = nullptr);
    ~ResourceBrowser();

public slots:
    void refreshTreeView();
    void showEvent(QShowEvent*) override;

private slots:
    void showBrowserMenu(const QPoint& pos);
    void updateFolderIcon(QTreeWidgetItem* item);
    void itemActivated(QTreeWidgetItem* item);

private:
    void createTreeViewWidget();
    void createIconViewWidget();
    void buildTree(QTreeWidgetItem* treeItem);
    QStringList expandedPaths() const;
    std::vector<QTreeWidgetItem*> allItems() const;
    void collectChildren(std::vector<QTreeWidgetItem*>& children, QTreeWidgetItem* item, bool recursive) const;

    QWidget* browser_;
    QWidget* treeViewWidget_;
    QTreeWidget* treeWidget_;
    QWidget* iconViewWidget_;
    QListWidget* iconWidget_;
    FlipSplitter* splitter_;
    ResourcePreview* resourcePreview_;
    QFileSystemWatcher folderWatcher_;
    QStringList prefabs_;
};

template <class T> static bool isOfType(QTreeWidgetItem* item)
{
    return (isOfType<T>(item->data(0, DryTypeHash).toUInt()));
}

#endif // RESOURCEBROWSER_H
