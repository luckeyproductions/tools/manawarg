/* ManaWarg
// Copyright (C) 2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QFontDatabase>
#include "../utility.h"

#include "resourcepreview.h"

ResourcePreview::ResourcePreview(Context* context, QWidget* parent): QWidget(parent), Object(context),
    view3d_{ new View3D{ context } },
    previewLabel_{ new QLabel{ "No preview" } },
    soundButton_{ new QPushButton{ QIcon{ ":/Sound" }, "Play" } },
    soundTimer_{ new QTimer{ this } },
    resource_{ nullptr },
    previewNode_{ nullptr },
    prefabNode_{ nullptr },
    previewModel_{ nullptr },
    previewEmitter_{ nullptr },
    previewSoundSource_{ nullptr },
    previewType_{ RT_None },
    resourceWatcher_{},
    previewFontId_{ -1 },
    reloadTimer_{}
{
    previewLabel_->setEnabled(false);
    previewLabel_->setScaledContents(true);
    previewLabel_->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
    previewLabel_->setAlignment(Qt::AlignCenter);
    soundButton_->setCheckable(true);

    QVBoxLayout* mainLayout{ new QVBoxLayout{ this } };
    mainLayout->setMargin(1);
    mainLayout->addWidget(previewLabel_);
    mainLayout->addWidget(view3d_);
    mainLayout->addWidget(soundButton_);
    mainLayout->setAlignment(previewLabel_, Qt::AlignCenter);

    createPreviewScene();

    view3d_->setVisible(false);
    soundButton_->setVisible(false);

    connect(soundButton_, SIGNAL(toggled(bool)), this, SLOT(soundButtonToggled(bool)));
    connect(soundTimer_, SIGNAL(timeout()), this, SLOT(soundFinished()));
    connect(&resourceWatcher_, SIGNAL(fileChanged(const QString&)), this, SLOT(handleResourceChanged()));
    connect(&reloadTimer_, SIGNAL(timeout()), this, SLOT(reloadResource()));

    setLayout(mainLayout);
}

void ResourcePreview::createPreviewScene()
{
    Scene* previewScene{ new Scene{ context_ } };
    previewScene->AddTag("Preview");
    previewScene->CreateComponent<Octree>();
    previewScene->CreateComponent<Zone>();

    // Light
    Node* lightNode{ previewScene->CreateChild("Light") };
    lightNode->SetPosition({ -2.f, 3.f, 1.f });
    lightNode->CreateComponent<Light>()/*->SetBrightness(1.23f)*/;

    // Camera
    Node* cameraNode{ previewScene->CreateChild("Camera") };
    cameraNode->SetTemporary(true);
    cameraNode->SetPosition(Vector3{ -1.f, .55f, 1.f }.Normalized() * M_SQRT3);
    cameraNode->LookAt(Vector3::ZERO);
    cameraNode->CreateComponent<Camera>()->SetFov(36.f);

    // Drawable preview
    previewNode_ = previewScene->CreateChild("Preview");
    previewNode_->SetTemporary(true);
    previewModel_ = previewNode_->CreateComponent<AnimatedModel>();
    previewNode_->CreateComponent<AnimationController>();
    previewEmitter_ = previewNode_->CreateComponent<ParticleEmitter>();
    prefabNode_ = previewNode_->CreateChild("Prefab");

    //Sound
    previewSoundSource_ = previewScene->CreateComponent<SoundSource>();

    view3d_->setScene(previewScene);
}

void ResourcePreview::changeResource(QTreeWidgetItem* item)
{
    if (previewType_ == RT_Particles)
    {
        previewEmitter_->RemoveAllParticles();
        previewEmitter_->SetEmitting(false);
    }
    else if (previewType_ == RT_Prefab)
    {
        prefabNode_->Remove();
        prefabNode_ = previewNode_->CreateChild("Prefab");
    }

    previewType_ = RT_None;
    for (QString file: resourceWatcher_.files())
        resourceWatcher_.removePath(file);

    if (previewFontId_ != -1)
    {
        QFont defaultFont{};
        defaultFont.setFamily(defaultFont.defaultFamily());
        previewLabel_->setFont(defaultFont);
        previewLabel_->setWordWrap(false);
        QFontDatabase::removeApplicationFont(previewFontId_);
        previewFontId_ = -1;
    }

    if (item)
    {
        const QString fileName{ item->data(0, FileName).toString() };
        const unsigned typeHash{ item->data(0, DryTypeHash).toUInt() };

        if (typeHash != 0u && !fileName.isEmpty() && !fileName.endsWith('/'))
        {
            if (isOfType<Texture2D>(typeHash))
                setImage(fileName);
            else if (isOfType<Material>(typeHash))
                setMaterial(fileName);
            else if (isOfType<Model>(typeHash))
                setModel(fileName);
            else if (isOfType<ParticleEffect>(typeHash))
                setParticles(fileName);
            else if (isOfType<Node>(typeHash))
                setPrefab(fileName);
            else if (isOfType<Sound>(typeHash))
                setSound(fileName);
            else if (isOfType<Font>(typeHash))
                setFont(fileName);
        }
    }

    previewLabel_->setVisible(previewType_ == RT_None || previewType_ == RT_Image || previewType_ == RT_Font);
    view3d_->setVisible(previewType_ == RT_Material || previewType_ == RT_Model || previewType_ == RT_Particles || previewType_ == RT_Prefab);
    view3d_->setContinuousUpdate(previewType_ == RT_Particles || previewType_ == RT_Prefab);
    soundButton_->setVisible(previewType_ == RT_Sound);
    soundButton_->setChecked(false);

    if (previewType_ == RT_None)
    {
        previewLabel_->setText("No preview");
        previewLabel_->setEnabled(false);
        resource_.Reset();
        return;
    }

    if (resource_)
    {
        const String resourceFilename{ GetSubsystem<ResourceCache>()->GetResourceFileName(resource_->GetName()) };
        resourceWatcher_.addPath(toQString(resourceFilename));
    }
}

void ResourcePreview::updatePreviewPixmap(bool pixmapChanged)
{
    if (previewPixmap_.isNull())
        return;

    const QSize containerSize{ previewLabel_->parentWidget()->size() };
    const int w{ std::min(containerSize.width(),  previewPixmap_.width())  };
    const int h{ std::min(containerSize.height(), previewPixmap_.height()) };
    if (!pixmapChanged && previewLabel_->pixmap(Qt::ReturnByValue).size() == QSize{ w, h })
        return;

    previewLabel_->setPixmap(previewPixmap_.scaled(w, h, Qt::KeepAspectRatio, Qt::SmoothTransformation));
}

void ResourcePreview::updatePreviewFontSize()
{
    QFont previewFont{ previewLabel_->font() };
    const QSize containerSize{ previewLabel_->parentWidget()->size() };
    previewFont.setPixelSize(sqrt(containerSize.width() * containerSize.height()) / 12 + 1);
    previewLabel_->setFont(previewFont);
    previewLabel_->resize(previewLabel_->size());
    previewLabel_->setText(previewLabel_->text());
}

void ResourcePreview::handleResourceChanged()
{
    int interval{ 10 };
    switch (previewType_)
    {
    default: break;
    case RT_Model: case RT_Image: interval = 125; break;
    }

    reloadTimer_.start(interval);
}

void ResourcePreview::reloadResource()
{
    const QString resourceName{ toQString(resource_->GetName()) };

    if (previewType_ == RT_Image)
        setImage(resourceName, true);
    else if (previewType_ == RT_Material)
        setMaterial(resourceName, true);
    else if (previewType_ == RT_Model)
        setModel(resourceName, true);
    else if (previewType_ == RT_Prefab)
        setPrefab(resourceName, true);
}

void ResourcePreview::setImage(const QString& fileName, bool reload)
{
    previewType_ = RT_Image;

    SharedPtr<Texture2D> texture{ GetSubsystem<ResourceCache>()->GetTempResource<Texture2D>(toString(fileName), !reload) };
    if (!texture || !texture->GetImage())
    {
        if (!reload)
            changeResource(nullptr);

        return;
    }

    if (!reload && resource_ == texture)
        return;

    resource_ = texture;
    previewPixmap_ = toPixmap(texture->GetImage());
    updatePreviewPixmap(true);
    previewLabel_->setEnabled(true);
}

void ResourcePreview::setMaterial(const QString& fileName, bool reload)
{
    previewType_ = RT_Material;

    SharedPtr<Material> material{ GetSubsystem<ResourceCache>()->GetTempResource<Material>(toString(fileName), !reload) };
    if (!material)
    {
        if (!reload)
            changeResource(nullptr);

        return;
    }

    if (!reload && resource_ == material)
        return;

    resource_ = material;
    previewModel_->SetModel(GetSubsystem<ResourceCache>()->GetTempResource<Model>("Models/Triacontahedron.mdl"));
    previewModel_->SetMaterial(material);

    previewNode_->SetScale(1.f);
    previewNode_->SetPosition(Vector3::ZERO);
    previewNode_->SetTags({ TAG_DEFAULT });

    view3d_->camera()->SetFov(36.f);
    view3d_->updateView();
}

void ResourcePreview::setModel(const QString& fileName, bool reload)
{
    previewType_ = RT_Model;

    SharedPtr<Model> model{ GetSubsystem<ResourceCache>()->GetTempResource<Model>(toString(fileName), !reload) };
    if (!model)
    {
        if (!reload)
            changeResource(nullptr);

        return;
    }

    if (!reload && resource_ == model)
        return;

    resource_ = model;
    previewModel_->SetModel(model);
    previewModel_->SetMaterial(GetSubsystem<ResourceCache>()->GetTempResource<Material>("Materials/None.xml"));

    previewNode_->SetScale(M_1_SQRT3 / Max(M_EPSILON, previewModel_->GetBoundingBox().HalfSize().Length()));
    previewNode_->SetPosition(-previewModel_->GetBoundingBox().Center() * previewNode_->GetScale().x_);
    previewNode_->SetRotation(Quaternion::IDENTITY);
    previewNode_->RemoveAllTags();

    view3d_->updateView();
}

void ResourcePreview::setParticles(const QString& fileName, bool reload)
{
    previewType_ = RT_Particles;

    SharedPtr<ParticleEffect> effect{ GetSubsystem<ResourceCache>()->GetTempResource<ParticleEffect>(toString(fileName), !reload) };
    if (!effect)
    {
        if (!reload)
            changeResource(nullptr);

        return;
    }

    if (!reload && resource_ == effect)
        return;

    resource_ = effect;
    previewEmitter_->SetEffect(effect);
    previewEmitter_->SetEmitting(true);
    previewEmitter_->SetRelative(true);

    previewModel_->SetModel(nullptr);
    previewNode_->SetTransform(Vector3::ZERO, Quaternion::IDENTITY);

    const Vector3 emitterSize{ effect->GetEmitterSize() };
    const Vector2 maxParticleSize{ effect->GetMaxParticleSize() };
    previewNode_->SetScale(M_1_SQRT3 / Max(M_EPSILON, emitterSize.Length() + maxParticleSize.Length()));
    previewNode_->RemoveAllTags();
}

void ResourcePreview::setPrefab(const QString& fileName, bool reload)
{
    previewType_ = RT_Prefab;

    ResourceCache* cache{ GetSubsystem<ResourceCache>() };
    bool success{ false };

    previewModel_->SetModel(nullptr);
    previewNode_->SetTransform({}, {}, 1.f);
    previewNode_->RemoveAllTags();

    if (fileName.endsWith(".xml", Qt::CaseInsensitive))
    {
        SharedPtr<XMLFile> prefabXML{ cache->GetTempResource<XMLFile>(toString(fileName)) };
        if (prefabXML)
        {
            success = prefabNode_->LoadXML(prefabXML->GetRoot("node"));
            resource_ = prefabXML;
        }
    }
    else if (fileName.endsWith(".json", Qt::CaseInsensitive))
    {
        SharedPtr<JSONFile> prefabJSON{ cache->GetTempResource<JSONFile>(toString(fileName)) };
        if (prefabJSON)
        {
            success = prefabNode_->LoadJSON(prefabJSON->GetRoot());
            resource_ = prefabJSON;
        }
    }

    if (!success)
    {
        changeResource(nullptr);
    }
    else
    {
        prefabNode_->SetPosition({});
        const BoundingBox prefabBounds{ sceneBoundingBox(previewNode_) };
        prefabNode_->SetScale(M_1_SQRT3 / Max(M_EPSILON, prefabBounds.HalfSize().Length()));
        prefabNode_->SetPosition(-prefabBounds.Center() * prefabNode_->GetScale().x_);

        view3d_->updateView();
    }
}

void ResourcePreview::setSound(const QString& fileName)
{
    previewType_ = RT_Sound;

    soundButton_->setProperty("fileName", fileName);
}

void ResourcePreview::setFont(const QString& fileName)
{
    previewType_ = RT_Font;

    previewFontId_ = QFontDatabase::addApplicationFont(fileName);
    const QString previewFontFamily{ QFontDatabase::applicationFontFamilies(previewFontId_).at(0) };
    const QFont font{ previewFontFamily };
    previewLabel_->setFont(font);
    previewLabel_->setText("the quick brown fox jumps over the lazy dog.\n\n"
                           "THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG!");
    previewLabel_->setWordWrap(true);
    updatePreviewFontSize();
}

void ResourcePreview::soundButtonToggled(bool checked)
{
    soundTimer_->stop();

    if (previewSoundSource_->IsPlaying())
        previewSoundSource_->Stop();

    if (checked)
    {
        ResourceCache* cache{ GetSubsystem<ResourceCache>() };
        SharedPtr<Sound> sound{ cache->GetTempResource<Sound>(toString(soundButton_->property("fileName").toString())) };

        previewSoundSource_->Play(sound, sound->GetFrequency());
        soundTimer_->start(sound->GetLength() * 1000);
    }
}

void ResourcePreview::soundFinished()
{
    soundButton_->toggle();
}

void ResourcePreview::playSound()
{
    if (previewType_ == RT_Sound)
    {
        if (soundButton_->isChecked())
            soundButton_->toggle();

        soundButton_->toggle();
    }
}

void ResourcePreview::resizeEvent(QResizeEvent* /*event*/)
{
    if (previewType_ == RT_Image)
        updatePreviewPixmap(false);
    else if (previewType_ == RT_Font)
        updatePreviewFontSize();
}
