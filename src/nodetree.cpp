/* ManaWarg
// Copyright (C) 2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QHeaderView>
#include <QTimer>
#include <QFileInfo>
#include <QMenu>
#include <QStatusBar>

#include "qocoon.h"
#include "nodetree.h"

NodeTree::NodeTree(Context* context, QWidget *parent): FileWidget(context, parent),
    treeWidget_{ nullptr },
    scene_{ nullptr },
    selectedItems_{}
{
    setObjectName("Node Tree");
    createTreeWidget();

    setAcceptDrops(true);
}

void NodeTree::createTreeWidget()
{
    QVBoxLayout* treeViewLayout{ new QVBoxLayout() };
    treeViewLayout->setMargin(2);
    treeWidget_ = new QTreeWidget();
    treeWidget_->setMinimumSize(64, 64);
    treeWidget_->setTextElideMode(Qt::ElideNone);
    treeWidget_->setHeaderHidden(true);
    treeWidget_->header()->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
    treeWidget_->header()->setSectionResizeMode(0, QHeaderView::Stretch);
    treeWidget_->header()->setStretchLastSection(false);
    treeWidget_->setDragEnabled(true);
    treeWidget_->setSelectionMode(QAbstractItemView::ExtendedSelection);
    treeWidget_->setContextMenuPolicy(Qt::CustomContextMenu);

    treeViewLayout->addWidget(treeWidget_);
    setLayout(treeViewLayout);

    connect(treeWidget_, SIGNAL(itemSelectionChanged()), this, SLOT(treeItemSelectionChanged()));
    connect(treeWidget_, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(showContextMenu(const QPoint&)));

    Selector* selector{ GetSubsystem<Selector>() };
    connect(selector, SIGNAL(selectionChanged(Selector*)), this, SLOT(selectionChanged(Selector*)));
}

void NodeTree::updateTree(bool restore)
{
    QVector<unsigned> expandedNodes;
    QPair<QVector<unsigned>, QVector<unsigned>> selectedObjects;
    if (restore)
    {
        expandedNodes   = collectExpandedNodeIDs();
        selectedObjects = collectSelectedObjectIDs();
    }

    treeWidget_->clear();

    scene_ = GetSubsystem<Qocoon>()->activeScene();
    if (!scene_)
       return;

    QTreeWidgetItem* sceneItem{ toItem(scene_) };
    grow(sceneItem, scene_);
    treeWidget_->addTopLevelItem(sceneItem);
    sceneItem->setExpanded(true);

    if (restore)
    {
        restoreExpanded(expandedNodes);
        restoreSelection(selectedObjects);
    }
}

void NodeTree::grow(QTreeWidgetItem* parent, Node* node)
{
    for (Component* c: node->GetComponents())
    {
        if (c->IsTemporary())
            continue;

        parent->addChild(toItem(c, parent));
    }

    for (Node* n: node->GetChildren())
    {
        if (n->IsTemporary())
            continue;

        QTreeWidgetItem* nodeItem{ toItem(n, parent) };

        grow(nodeItem, n);
        parent->addChild(nodeItem);
    }
}

QTreeWidgetItem* NodeTree::toItem(Object* object, QTreeWidgetItem* parentItem)
{
    bool isNode{ object->IsInstanceOf<Node>() };

    Node* n{ static_cast<Node*>(object) };
    Component* c{ static_cast<Component*>(object) };

    QTreeWidgetItem* objectItem{
        (parentItem ? new QTreeWidgetItem{
                     parentItem, { toQString(isNode ? n->GetName() : object->GetTypeName()) } }
                    : new QTreeWidgetItem{
                     treeWidget_, { toQString(isNode ? n->GetName() : object->GetTypeName()) } }) };

    if (objectItem->text(0).isEmpty())
        objectItem->setText(0, toQString(object->GetTypeName()));

    QString iconName{ ":/Component" };
    const QFileInfo iconCheck{ ":/" + toQString(object->GetTypeName()) };

    if (iconCheck.exists())
        iconName = iconCheck.filePath();

    objectItem->setIcon(0, QIcon{ iconName });
    objectItem->setData(0, ID, QVariant{ (isNode ? n->GetID() : c->GetID()) });
    objectItem->setData(0, DryTypeHash, QVariant{ object->GetType().ToHash() });

    return objectItem;
}

Object* NodeTree::getObject(QTreeWidgetItem* item)
{
    return getObjectFromScene(scene_,
                              item->data(0, DryTypeHash).toUInt(),
                              item->data(0, ID).toUInt());
}

QVector<unsigned> NodeTree::collectExpandedNodeIDs() const
{
    QVector<unsigned> expandedNodes{};
    for (QTreeWidgetItem* item: nodeItems())
    {
        if (item->isExpanded())
            expandedNodes.push_back(item->data(0, ID).toUInt());
    }

    return expandedNodes;
}

QPair<QVector<unsigned>, QVector<unsigned>> NodeTree::collectSelectedObjectIDs() const
{
    QPair<QVector<unsigned>, QVector<unsigned>> selectedObjects{};
    for (QTreeWidgetItem* item: selectedItems_)
    {
        if (!item)
            continue;

        const unsigned typeHash{ item->data(0, DryTypeHash).toUInt() };
        const unsigned id{ item->data(0, ID).toUInt() };

        if (isOfType<Node>(typeHash) || isOfType<Scene>(typeHash))
            selectedObjects.first.push_back(id);
        else
            selectedObjects.second.push_back(id);
    }

    return selectedObjects;
}

void NodeTree::restoreExpanded(const QVector<unsigned>& expandedNodes)
{
    for (QTreeWidgetItem* item: nodeItems())
    {
        if (expandedNodes.contains(item->data(0, ID).toUInt()))
            item->setExpanded(true);
    }
}

void NodeTree::restoreSelection(const QPair<QVector<unsigned>, QVector<unsigned>>& selectedObjects)
{
    disconnect(treeWidget_, SIGNAL(itemSelectionChanged()), this, SLOT(treeItemSelectionChanged()));

    selectedItems_.clear();
    for (QTreeWidgetItem* item: allItems())
    {
        const unsigned id{ item->data(0, ID).toUInt() };
        const unsigned typeHash{ item->data(0, DryTypeHash).toUInt() };
        if (isOfType<Node>(typeHash) || isOfType<Scene>(typeHash))
            item->setSelected(selectedObjects.first.contains(id));
        else
            item->setSelected(selectedObjects.second.contains(id));

        if (item->isSelected())
        {
            QTreeWidgetItem* parentItem{ item->parent() };
            while (parentItem)
            {
                parentItem->setExpanded(true);
                parentItem = parentItem->parent();
            }

            selectedItems_.push_back(item);
        }
    }

    connect(treeWidget_, SIGNAL(itemSelectionChanged()), this, SLOT(treeItemSelectionChanged()));
}

QList<QTreeWidgetItem*> NodeTree::nodeItems() const
{
    QList<QTreeWidgetItem*> items{};

    for (QTreeWidgetItem* item: allItems())
    {
        if (isOfType<Node>(item->data(0, DryTypeHash).toUInt()))
            items.push_back(item);
    }

    return items;
}

std::vector<QTreeWidgetItem*> NodeTree::allItems() const
{
    std::vector<QTreeWidgetItem*> treeItems{};

    for (int i{ 0 }; i < treeWidget_->topLevelItemCount(); ++i)
    {
        if (QTreeWidgetItem* item{ treeWidget_->topLevelItem(i) })
        {
            collectChildren(treeItems, item, true);
            treeItems.push_back(item);
        }
    }

    return treeItems;
}

void NodeTree::collectChildren(std::vector<QTreeWidgetItem*>& children, QTreeWidgetItem* item, bool recursive) const
{
    for (int c{ 0 }; c < item->childCount(); ++c)
    {
        QTreeWidgetItem* childItem{ item->child(c) };
        children.push_back(childItem);

        if (recursive)
            collectChildren(children, childItem, true);
    }
}

void NodeTree::treeItemSelectionChanged()
{
    selectedItems_.clear();

    HashSet<Object*> selection{};
    for (QTreeWidgetItem* item: treeWidget_->selectedItems())
    {
        selectedItems_.push_back(item);
        Object* object{ getObject(item) };

        if (object)
            selection.Insert(object);
    }

    Selector*   selector{ GetSubsystem<Selector>() };
    disconnect( selector, SIGNAL(selectionChanged(Selector*)), this, SLOT(selectionChanged(Selector*)));
    selector->select(selection, false);
    connect(    selector, SIGNAL(selectionChanged(Selector*)), this, SLOT(selectionChanged(Selector*)));
}

PODVector<Animatable*> NodeTree::deletableAnimatables()
{
    PODVector<Animatable*> animatables{};

    for (QTreeWidgetItem* item: treeWidget_->selectedItems())
    {
        if (Animatable* animatable{ static_cast<Animatable*>(getObject(item)) })
        {
            if (!animatable->GetTypeInfo()->IsTypeOf<Scene>() &&
                !animatable->GetTypeInfo()->IsTypeOf<Octree>())
                animatables.Push(animatable);
        }
    }

    return animatables;
}

void NodeTree::showContextMenu(const QPoint& pos)
{
    QMenu contextMenu{};
    Qocoon* qocoon{ GetSubsystem<Qocoon>() };
    QTreeWidgetItem* item{ treeWidget_->itemAt(pos) };

    if (item)
    {
        PODVector<Animatable*> animatables{ deletableAnimatables() };

        QAction* savePrefabAction{ nullptr };
        const bool isNodeItem{ item->data(0, DryTypeHash).toUInt() == Node::GetTypeStatic().ToHash() };
        const bool isSceneItem{ item->data(0, DryTypeHash).toUInt() == Scene::GetTypeStatic().ToHash() };

        if (isNodeItem)
            savePrefabAction = contextMenu.addAction(QIcon(":/SaveAs"), "Save as prefab");
        if (isNodeItem || isSceneItem)
            contextMenu.addMenu(qocoon->addMenu());

        QAction* deleteAction{ contextMenu.addAction(QIcon(":/Delete"), "Delete") };
        deleteAction->setEnabled(!animatables.IsEmpty() && !qocoon->isRunning());
        QAction* pickedAction{ contextMenu.exec(mapToGlobal(pos)) };

        if (pickedAction)
        {
             if (pickedAction == savePrefabAction)
             {
                 Node* node{ static_cast<Node*>(getObject(item)) };

                 savePrefab(node);
             }
             else if (pickedAction == deleteAction)
             {
                 qocoon->actionDelete();
             }
        }
    }
}

void NodeTree::savePrefab(Node* node)
{
    Qocoon* qocoon{ GetSubsystem<Qocoon>() };
    const QString projectFolder{ toQString(qocoon->projectLocation()) };
    std::shared_ptr<QString> selectedFilter{ new QString{} };
    String filename{ toString(QFileDialog::getSaveFileName(
                     nullptr, "Save Prefab As...", projectFolder,
                     "XML Files (*.xml);;JSON Files (*.json)", selectedFilter.get())) };

    if (filename.IsEmpty())
        return;

    const int asterisk{ selectedFilter->lastIndexOf('*') + 1 };
    const String extension{ toString(selectedFilter->mid(asterisk, selectedFilter->lastIndexOf(')') - asterisk)) };
    if (!filename.EndsWith(extension))
        filename.Append(extension);

    bool success{ false };
    if (extension == ".xml")
    {
        SharedPtr<XMLFile> prefabXML{ new XMLFile{ context_ } };
        XMLElement root{ prefabXML->CreateRoot("node") };
        success = node->SaveXML(root) && prefabXML->SaveFile(filename);

    }
    else if (extension == ".json")
    {
        SharedPtr<JSONFile> prefabJSON{ new JSONFile{ context_ } };
        success = node->SaveJSON(prefabJSON->GetRoot()) && prefabJSON->SaveFile(filename);
    }

    if (success)
        qocoon->statusBar()->showMessage("Saved prefab: " + toQString(filename));
    else
        qocoon->statusBar()->showMessage("Failed to save prefab: " + toQString(filename));

}

void NodeTree::selectionChanged(Selector* selector)
{
    disconnect(treeWidget_, SIGNAL(itemSelectionChanged()), this, SLOT(treeItemSelectionChanged()));

    for (QTreeWidgetItem* item: allItems())
    {
        Object* object{ getObject(item) };
        item->setSelected(selector->selection(false).Contains(object));

        if (item->isSelected())
        {
            while (item->parent())
            {
                item->parent()->setExpanded(true);
                item = item->parent();
            }
        }
    }

    connect(treeWidget_, SIGNAL(itemSelectionChanged()), this, SLOT(treeItemSelectionChanged()));
}

PODVector<Animatable*> NodeTree::selectedObjects()
{
    PODVector<Animatable*> selection{};

    for (QTreeWidgetItem* item: treeWidget_->selectedItems())
        selection.Push(static_cast<Animatable*>(getObject(item)));

    return selection;
}

PODVector<Node*> NodeTree::selectedNodes(bool recursive, bool excludeScene)
{
    PODVector<Node*> selection{};

    for (QTreeWidgetItem* item: treeWidget_->selectedItems())
    {
        Object* object{ getObject(item) };
        if (excludeScene && object->IsInstanceOf<Scene>())
            continue;

        if (object && object->GetTypeInfo()->IsTypeOf<Node>())
        {
            Node* node{ static_cast<Node*>(object) };
            if (!selection.Contains(node))
            {
                selection.Push(node);

                if (recursive)
                {
                    PODVector<Node*> children{};
                    node->GetChildren(children, true);
                    for (Node* child: children)
                    {
                        if (!selection.Contains(child))
                            selection.Push(child);
                    }
                }
            }
        }
    }

    return selection;
}

void NodeTree::dragEnterEvent(QDragEnterEvent* event)
{
    const QMap<int, QVariant> dataMap{ extractDataMap(event->mimeData()) };

    if (GetSubsystem<Qocoon>()->isRunning())
        event->ignore();
    else if (dataMap.contains(ID) || isOfType<Node>(dataMap[DryTypeHash].toUInt()))
        event->acceptProposedAction();
}

void NodeTree::dragMoveEvent(QDragMoveEvent* event)
{
    const QMap<int, QVariant> dataMap{ extractDataMap(event->mimeData()) };
    QTreeWidgetItem* item{ treeWidget_->itemAt(event->pos()) };
    if (!item)
    {
        event->ignore();
        return;
    }

    const unsigned itemTypeHash{ item->data(0, DryTypeHash).toUInt() };
    if (!isOfType<Node>(itemTypeHash) && !isOfType<Scene>(itemTypeHash))
    {
        event->ignore();
    }
    else
    {
        Node* target{ static_cast<Node*>(getObject(item)) };
        PODVector<Node*> selection{ selectedNodes(true) };
        if (dataMap.contains(ID) && selection.Contains(target))
            event->ignore();
        else
            event->acceptProposedAction();
    }
}

void NodeTree::dropEvent(QDropEvent* event)
{
    const QMap<int, QVariant> dataMap{ extractDataMap(event->mimeData()) };
    QTreeWidgetItem* item{ treeWidget_->itemAt(event->pos()) };
    if (!item)
    {
        event->ignore();
        return;
    }

    if (dataMap.contains(ID))
    {
        PODVector<Animatable*> selection{ selectedObjects() };
        Node* target{ static_cast<Node*>(getObject(item)) };

        GetSubsystem<Qocoon>()->reparent(selection, target);
    }
    else if (isOfType<Node>(dataMap[DryTypeHash].toUInt()))
    {
        Qocoon* qocoon{ GetSubsystem<Qocoon>() };
        const bool shiftDown{ (QGuiApplication::keyboardModifiers() & Qt::ShiftModifier) != 0 };
        const String fileName{ toString(dataMap[FileName].toString()) };
        if (!shiftDown)
        {
            Node* parent{ static_cast<Node*>(getObject(item)) };

            if (parent && !fileName.IsEmpty())
                qocoon->addPrefabToNode(parent, fileName);
        }
        else
        {
            PODVector<Node*> selection{ qocoon->filterConflicting(selectedNodes(false, true)) };
            qocoon->applyPrefab(selection, fileName);
        }
    }
}
