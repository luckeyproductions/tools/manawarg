/* ManaWarg
// Copyright (C) 2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QDragMoveEvent>
#include <QDropEvent>
#include <QDir>
#include <QTextBlock>
#include <QTextDocumentFragment>
#include <QTextStream>
#include <QTemporaryFile>
#include <QDebug>
#include <QScrollBar>
#include "utility.h"
#include "qocoon.h"
#include "sceneviewer.h"
#include <AngelScript/angelscript.h>

#include "texteditor.h"

TextEditor::TextEditor(Context* context, QWidget* parent): FileWidget(context, parent),
    mainLayout_{ new QVBoxLayout{} },
    textEdit_{ new QTextEdit{} },
    lineWrapAction_{ new QAction{ this } },
    runAction_{ new QAction{ this } },
    commentAction_{ new QAction{ this } },
    tempScriptNode_{ nullptr },
    tempScriptFile_{ nullptr },
    file_{ nullptr }
{   
    setObjectName("Text Editor");
    setAcceptDrops(true);
    completeOperationsText("File");

    lineWrapAction_->setIcon(QIcon{ ":/LineWrap" });
    lineWrapAction_->setToolTip("Word wrap");
    lineWrapAction_->setCheckable(true);
    connect(lineWrapAction_, SIGNAL(triggered(bool)), this, SLOT(lineWrapToggled(bool)));

    runAction_->setIcon(QIcon{ ":/Play" });
    runAction_->setToolTip("Run as script");
    runAction_->setCheckable(true);
    connect(runAction_, SIGNAL(triggered(bool)), this, SLOT(runActionToggled(bool)));

    commentAction_->setIcon(QIcon{ ":/Comment" });
    commentAction_->setToolTip("Toggle comment");
    commentAction_->setShortcut(QKeySequence{ "Ctrl+/" });
    connect(commentAction_, SIGNAL(triggered(bool)), this, SLOT(actionComment()));

    QAction* sceneRunAction{ GetSubsystem<Qocoon>()->runAction() };
    connect(sceneRunAction, SIGNAL(triggered(bool)), this, SLOT(runActionToggled(bool)));

    mainLayout_->setMargin(2);
    createToolBar();

    QFont font{};
    font.setFamily("Monospace");
    textEdit_->setCurrentFont(font);
    textEdit_->setFont(font);
    textEdit_->setWordWrapMode(QTextOption::NoWrap);
    textEdit_->setTabStopDistance(4 * QFontMetrics{ font }.horizontalAdvance(' '));
    mainLayout_->addWidget(textEdit_);

    setLayout(mainLayout_);
}

void TextEditor::createToolBar()
{
    actionsToolBar_->addAction(lineWrapAction_);
    actionsToolBar_->addAction(runAction_);
    actionsToolBar_->addAction(commentAction_);

    mainLayout_->addLayout(toolBarLayout_);

    FileWidget::createToolBar();
}

void TextEditor::setOrientation(Qt::Orientation orientation)
{
    FileWidget::setOrientation(orientation);

    if (orientation == Qt::Vertical)
        mainLayout_->setDirection(QBoxLayout::TopToBottom);
    else
        mainLayout_->setDirection(QBoxLayout::LeftToRight);
}

void TextEditor::actionNew()
{
    /// Check for changes

    FileWidget::actionNew();

    textEdit_->clear();
}

void TextEditor::actionOpen()
{
    /// Check for changes

    Qocoon* qocoon{ GetSubsystem<Qocoon>() };
    const String projectFolder{ qocoon->projectLocation() };
    const QString homeFolder{ QStandardPaths::writableLocation(QStandardPaths::HomeLocation) };
    QString xmlFilter{ tr("*.*") };
    const String fileName{ toString(QFileDialog::getOpenFileName(
                                        this, tr("Open File"), qocoon->project()
                                        ? toQString(projectFolder + Qocoon::project()->resourceFolders().Front())
                                        : homeFolder, xmlFilter, &xmlFilter)) };
    openFile(fileName);
}

void TextEditor::actionSave(bool SaveAs)
{
    if (fileName_.IsEmpty() || SaveAs)
    {
        Qocoon* qocoon{ GetSubsystem<Qocoon>() };
        const String projectFolder{ qocoon->projectLocation() };
        const QString homeFolder{ QStandardPaths::writableLocation(QStandardPaths::HomeLocation) };
        const String fileName{ toString(QFileDialog::getSaveFileName(
                                                    this, tr("Save File As..."), qocoon->project()
                                                    ? toQString(projectFolder + Qocoon::project()->resourceFolders().Front())
                                                    : homeFolder)) };
        if (!fileName.IsEmpty())
            fileName_ = fileName;
        else
            return;
    }

    QFile outputFile{ toQString(fileName_) };
    if (outputFile.open(QIODevice::WriteOnly | QIODevice::Text))
    {
         QTextStream outStream(&outputFile);
         outStream << textEdit_->toPlainText();
    }

    outputFile.close();
}

void TextEditor::lineWrapToggled(bool toggled)
{
    textEdit_->setWordWrapMode(toggled ? QTextOption::WrapAtWordBoundaryOrAnywhere
                                       : QTextOption::NoWrap);
}

void TextEditor::runActionToggled(bool start)
{
    if (start && sender() != runAction_)
        return;

    ResourceCache* cache{ GetSubsystem<ResourceCache>() };
    Scene* scene{ GetSubsystem<Qocoon>()->activeScene() };

    if (!start)
    {
        GetSubsystem<Qocoon>()->runSimulation(false);
        tempScriptNode_ = nullptr;
        tempScriptFile_.Reset();

        if (runAction_->isChecked())
        {
            runAction_->blockSignals(true);
            runAction_->setChecked(false);
            runAction_->blockSignals(false);
        }

        return;
    }

    const QString tempScriptName{ QDir::tempPath() + "/tempscript.as" };
    QFile tempFile{ tempScriptName };

    if (tempFile.open(QIODevice::WriteOnly | QIODevice::Text))
    {
         QTextStream outStream(&tempFile);
         outStream << textEdit_->toPlainText();
    }

    tempFile.close();

    if (scene)
    {
        tempScriptFile_ = cache->GetTempResource<ScriptFile>(toString(tempScriptName));

        if (tempScriptFile_)
        {
            asIScriptModule* scriptModule{ tempScriptFile_->GetScriptModule() };
            const String className{ scriptModule->GetObjectTypeByIndex(0)->GetName() };
            if (!className.IsEmpty())
            {
                GetSubsystem<Qocoon>()->runSimulation(true);
                tempScriptNode_ = scene->CreateChild("TempScript");
                tempScriptNode_->SetTemporary(true);
                ScriptInstance* tempScriptInstance{ tempScriptNode_->CreateComponent<ScriptInstance>() };
                Log::Write(LOG_INFO, "Creating " + className);
                tempScriptInstance->CreateObject(tempScriptFile_, className);
            }
            else
            {
                runAction_->setChecked(false);
            }
        }
        else
        {
            runAction_->setChecked(false);
        }
    }
}

void TextEditor::actionComment()
{
    QTextCursor cursor{ textEdit_->textCursor() };

    const int selectedLines{ cursor.selection().toPlainText().count("\n") + 1 };
    cursor.setPosition(cursor.selectionStart());
    cursor.setPosition(cursor.block().position());
    const int start{ cursor.position() };

    bool uncomment{ true };
    for (int i{ 0 }; i < selectedLines; ++i)
    {
        if (!cursor.block().text().startsWith("//"))
            uncomment = false;

        cursor.setPosition(cursor.block().next().position());
    }

    QTextCursor editCursor{ textEdit_->document() };
    editCursor.setPosition(start);

    for (int i{ 0 }; i < selectedLines; ++i)
    {
        bool hasComment{ editCursor.block().text().startsWith("//") };

        if (uncomment == hasComment)
        {
            if (uncomment)
            {
                editCursor.deleteChar();
                editCursor.deleteChar();
            }
            else
            {
                editCursor.insertText("//");
            }
        }

        editCursor.setPosition(editCursor.block().next().position());
    }
}

void TextEditor::dragEnterEvent(QDragEnterEvent* event)
{
    const QMimeData* mime{ event->mimeData() };

    if (!isOfBinaryType(mime))
        event->acceptProposedAction();
}

void TextEditor::dropEvent(QDropEvent* event)
{
    const QMimeData* mime{ event->mimeData() };
    QMap<int, QVariant> roleDataMap{ extractDataMap(mime) };

    for (const QVariant& val: roleDataMap.values())
    {
        const int key{ roleDataMap.key(val) };

        if (key == FileName)
            openFile(toString(val.toString()));
    }
}

void TextEditor::openFile(const String& fileName)
{
    if (fileName.IsEmpty())
        return;

    File* file{ new File{ context_ } };
    textEdit_->setUndoRedoEnabled(false);

    if (file->Open(fileName))
    {
        fileName_ = fileName;
        textEdit_->clear();
        updateTitle();

        while (!file->IsEof())
        {
            const String line{ file->ReadLine() };
            textEdit_->append(toQString(line));
        }
    }

    textEdit_->setUndoRedoEnabled(true);
    textEdit_->verticalScrollBar()->setValue(0);
    textEdit_->textCursor().setPosition(0);

    raiseDockWidget();
}
