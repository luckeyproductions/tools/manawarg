/* ManaWarg
// Copyright (C) 2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef TRANSFORMER_H
#define TRANSFORMER_H

#include "dryobject.h"

#include <QAction>
#include "utility.h"

//class Gizmo: public Component
//{
//    DRY_OBJECT(Gizmo, Component);

//public:
//    Gizmo(Context* context);

//private:
//    SharedPtr<CustomGeometry> gizmoGeometry_;
//};

class SceneViewer;

class Transformer: public DryObject
{
    Q_OBJECT
    DRY_OBJECT(Transformer, DryObject);

public:
    Transformer(Context* context, SceneViewer* parent = nullptr);

    void handleMouseMove(const QPoint& dPos);

    bool isTransforming() const { return transforming_; }
    QList<QAction*> transformActions() const
    {
        return
        {
            translateAction_, rotateAction_, scaleAction_,
                    applyAction_, cancelAction_,
                    xAction_, yAction_, zAction_,
                    xPlaneAction_, yPlaneAction_, zPlaneAction_,
                    resetTranslationAction_, resetRotationAction_, resetScaleAction_
        };
    }

    QList<QAction*> duringTransformActions() const
    {
        return
        {
            applyAction_, cancelAction_,
                    xAction_, yAction_, zAction_,
                    xPlaneAction_, yPlaneAction_, zPlaneAction_
        };
    }

    QList<QAction*> notDuringTransformActions() const
    {
        return
        {
            resetTranslationAction_, resetRotationAction_, resetScaleAction_
        };
    }

public slots:
    void endTransformation();
    void cancelTransformation();

private slots:
    void beginTransformation();

    void beginTranslate();
    void beginRotate();
    void beginScale();
    void setAxisLock();
    void setPlaneLock();

    void resetTranslation();
    void resetRotation();
    void resetScale();

private:
    SceneViewer* sceneViewer() const;

    void createTransformActions();
    void updateAvailableActions();

    void resetAttribute(const String& attribute);
    void updateTransforms(const Matrix3x4& delta) const;
    void applyTransformation() const;
    Matrix3x4 determineNewTransform(Node* node, const Matrix3x4& delta) const;

    void translationAxes(Vector3& xTo, Vector3& yTo) const;
    void rotationAxes(Vector3& xTo, Vector3& yTo) const;
    void scaleAxes(Vector3& xTo, Vector3& yTo) const;
    Vector3 flipVector(Node* node, Node* camNode) const;

//    Gizmo* activeGizmo_;
    QAction* translateAction_;
    QAction* rotateAction_;
    QAction* scaleAction_;
    QAction* applyAction_;
    QAction* cancelAction_;
    QAction* xAction_;
    QAction* yAction_;
    QAction* zAction_;
    QAction* xPlaneAction_;
    QAction* yPlaneAction_;
    QAction* zPlaneAction_;
    QAction* resetTranslationAction_;
    QAction* resetRotationAction_;
    QAction* resetScaleAction_;
    TransformationSpace space_;
    TransformationType transformationType_;
    Vector3 pivot_;
    AxisLock axis_;
    Matrix3x4 transformation_;
    bool transforming_;

    static AxisLock charToLock(char c, bool shift)
    {
        if (!shift)
        {
            switch (c)
            {
            default:  return AL_None;
            case 'X': return AL_X;
            case 'Y': return AL_Y;
            case 'Z': return AL_Z;
            }
        }
        else
        {
            switch (c)
            {
            default:  return AL_None;
            case 'X': return AL_NotX;
            case 'Y': return AL_NotY;
            case 'Z': return AL_NotZ;
            }
        }
    }
};


#endif // TRANSFORMER_H
