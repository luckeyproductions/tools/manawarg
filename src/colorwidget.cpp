/* ManaWarg
// Copyright (C) 2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QHBoxLayout>
#include <QDialogButtonBox>
#include <QVariant>
#include <QPainter>
#include <QGraphicsEffect>
#include <QDialog>
#include <QStyle>
#include "filewidget.h"
#include "materialeditor.h"
#include "utility.h"

#include "colorwidget.h"

ColorWidget::ColorWidget(const QString& name, QWidget* parent): QWidget(parent),
    dryWidget_{ nullptr },
    colorButton_{ new QPushButton(this) },
    color_{ Color::BLACK }
{
    if (PropertiesWidget* propWidget{ static_cast<PropertiesWidget*>(parent->parent()) })
        dryWidget_ = propWidget->dryWidget();

    const QString fullName{ (name.contains(' ') || name.contains("Color", Qt::CaseInsensitive)
                ? name
                : name + " color") };

    setProperty("name", fullName);
    setToolTip(fullName);
    const QString shaderParameterName{ "Mat" + (name == "Emissive" ? name : name.left(4)) + "Color" };
    setProperty("parameter", shaderParameterName);

    QHBoxLayout* mainLayout{ new QHBoxLayout() };
    mainLayout->setMargin(0);

    mainLayout->addWidget(colorButton_);
    colorButton_->setMaximumSize(32, 29);
    colorButton_->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
//    colorButton_->setIconSize(QSize(30, 30));
    setLayout(mainLayout);

    connect(colorButton_, SIGNAL(clicked(bool)), this, SLOT(pickColor()));
}

QGroupBox* ColorWidget::pickerBox() const
{
    if (dryWidget_)
        return dryWidget_->pickerBox();
    else
        return nullptr;
}

void ColorWidget::resizeEvent(QResizeEvent*)
{
    QSize iconSize{ colorButton_->size() - QSize{ 8, 4 } };

    if (iconSize.height() < 8)
        iconSize.setHeight(8);

    colorButton_->setIconSize(iconSize);
    updateColorButton();
}

bool ColorWidget::isDiffuse() const
{
    return property("parameter").toString().contains("Diff");
}

bool ColorWidget::isSpecular() const
{
    return property("parameter").toString().contains("Spec");
}

bool ColorWidget::isEmissive() const
{
    return property("parameter").toString().contains("Emissive");
}

void ColorWidget::setColor(const Color& color)
{
    if (color == color_)
        return;

    color_ = color;
    updateColorButton();
    emit colorChanged();
}

void ColorWidget::updateColorButton()
{
    QPixmap colorPixmap{ colorButton_->iconSize() };

    if (isDiffuse())
        paintCheckerboard(&colorPixmap);

    QPainter p{ &colorPixmap };
    p.fillRect(colorPixmap.rect(), toQColor(color_, isDiffuse()));
    p.end();

    colorButton_->setIcon(QIcon{ colorPixmap });
}

void ColorWidget::pickColor()
{
    ColorPicker* picker{ new ColorPicker{ this } };

    if (pickerBox())
    {
        QLayout* pickerLayout{ pickerBox()->layout() };
        QLayoutItem* item{ pickerLayout->itemAt(0) };

        if (item)
        {
            pickerLayout->removeItem(item);
            delete item->widget();
        }

        pickerLayout->addWidget(picker);
        QString colorName{ property("name").toString() };
        pickerBox()->setObjectName(colorName);
        pickerBox()->show();
        dryWidget_->updatePickerBoxTitle();
        connect(picker, SIGNAL(colorChanged(const Color&)), this, SLOT(setColor(const Color&)));
    }
    else
    {
        QDialog* colorDialog{ new QDialog() };
        QBoxLayout* colorDialogLayout{ new QBoxLayout{ QBoxLayout::TopToBottom } };
        colorDialogLayout->addWidget(picker);
        colorDialog->setLayout(colorDialogLayout);
        connect(picker, SIGNAL(colorChanged(const Color&)), this, SLOT(setColor(const Color&)));
        connect(colorDialog, SIGNAL(finished(int)), this, SIGNAL(colorPicked()));

        colorDialog->exec();
    }
}

///

ColorMode ColorPicker::mode_{ RGB };

ColorPicker::ColorPicker(ColorWidget* parent): QWidget(parent),
    colorWidget_{ parent },
    color_{ colorWidget_->color() },
    colorLabel_{},
    tabBar_{ new QTabBar{ this } }
{
    createMainLayout();

    colorLabel_ = new QLabel();
    colorLabel_->setMinimumSize(12, 12);
    colorLabel_->setScaledContents(true);
    mainLayout_->addWidget(colorLabel_);

    createFormLayout();
    createSliders();
    createTabBar();
}

ColorPicker::ColorPicker(Color& color): QWidget(),
    colorWidget_{ nullptr },
    color_{ color },
    colorLabel_{ nullptr },
    tabBar_{ new QTabBar{ this } }
{
    createMainLayout();
    createFormLayout();
    createSliders();
    createTabBar();
}

void ColorPicker::hideBox()
{
    parentWidget()->hide();
}

void ColorPicker::createMainLayout()
{
    mainLayout_ = new QVBoxLayout();
    mainLayout_->setContentsMargins(4, 0, 4, 0);

    QPushButton* closeButton{ new QPushButton(QIcon(style()->standardIcon(QStyle::SP_TitleBarCloseButton, 0, this)), "") };
    closeButton->setFlat(true);
    closeButton->setMaximumSize(17, 15);
    connect(closeButton, SIGNAL(clicked(bool)), this, SLOT(hideBox()));
    mainLayout_->addWidget(closeButton);
    mainLayout_->setAlignment(closeButton, Qt::AlignRight);

    setLayout(mainLayout_);
    setMinimumWidth(64);
}

void ColorPicker::createTabBar()
{
    tabBar_->addTab("RGB");
    tabBar_->addTab("HSV");
    tabBar_->addTab("Hex");
    tabBar_->setExpanding(false);
    tabBar_->setShape(QTabBar::RoundedSouth);
    tabBar_->setStyleSheet("QTabBar::tab { height: 18px; width: 42px; }");
    mainLayout_->addWidget(tabBar_);

    connect(tabBar_, SIGNAL(currentChanged(int)), this, SLOT(changeMode()));
    tabBar_->setCurrentIndex(mode_);
}

void ColorPicker::createFormLayout()
{
    formLayout_ = new QFormLayout();
    formLayout_->setMargin(0);
    formLayout_->setSpacing(0);
    formLayout_->setLabelAlignment(Qt::AlignRight | Qt::AlignVCenter);
    mainLayout_->addLayout(formLayout_);
}

void ColorPicker::createSliders()
{
    for (int c{ RED }; c <= ALPHA; ++c)
    {
        QSlider* colorSlider{ new QSlider(Qt::Horizontal) };
        colorSlider->setMinimum(0);
        colorSlider->setMaximum(1000);
        colorSlider->setSingleStep(10);
        colorSlider->setPageStep(100);
        colorSlider->setTickInterval(250);
        colorSlider->setTickPosition(QSlider::TicksBothSides);
        colorSlider->setMinimumHeight(15);

        QString colorName{};
        switch (c)
        {
        default: break;
        case RED:
        {
            colorName = "Red";
        }
        break;
        case GREEN:
        {
            colorName = "Green";

        }
        break;
        case BLUE:
        {
            colorName = "Blue";
        }
        break;
        case ALPHA:
        {
            colorName = "Alpha";

            if (colorWidget_)
            {
                if (colorWidget_->isEmissive())
                {
                    continue;
                }
                else if (colorWidget_->isSpecular())
                {
                    colorName = "Hardness";

                    colorSlider->setSingleStep(100);
                    colorSlider->setPageStep(800);
                    colorSlider->setTickInterval(2000);
                    colorSlider->setMaximum(8000);
                }
            }
        }
        break;
        }

        QGraphicsColorizeEffect* colorize{ new QGraphicsColorizeEffect(colorSlider) };
        colorize->setColor(QColor(colorName.toLower()));
        colorize->setStrength(2/3.f);
        colorSlider->setGraphicsEffect(colorize);

        QLabel* sliderLabel{ new QLabel(colorName) };
        sliderLabel->setObjectName(colorName);
        sliderLabel->setMinimumHeight(14);
        formLayout_->addRow(sliderLabel, colorSlider);
        sliders_[c] = colorSlider;
        labels_[c] = sliderLabel;

        connect(colorSlider, SIGNAL(valueChanged(int)), this, SLOT(updateColor()));
    }

    updateSliderValues();
}

void ColorPicker::updateSliderValues()
{
    if (colorWidget_)
        color_ = colorWidget_->color();

    if (!modeHex())
    {
        for (int c{ RED }; c <= ALPHA; ++c)
        {
            QSlider* colorSlider{ sliders_[c] };
            if (!colorSlider)
                continue;

            colorSlider->blockSignals(true);

            switch (c)
            {
            case RED:   colorSlider->setValue(1000.f * (modeHSV() ? color_.Hue()           : color_.r_)); break;
            case GREEN: colorSlider->setValue(1000.f * (modeHSV() ? color_.SaturationHSV() : color_.g_)); break;
            case BLUE:  colorSlider->setValue(1000.f * (modeHSV() ? color_.Value()         : color_.b_)); break;
            case ALPHA: colorSlider->setValue(1000.f * (colorWidget_ && colorWidget_->isSpecular() ? pow(color_.a_, 1 / SPECPOWER) : color_.a_)); break;
            default: break;
            }

            colorSlider->blockSignals(false);
        }

        updateToolTips();
    }
}

void ColorPicker::updateToolTips()
{
    for (int c{ RED }; c <= ALPHA; ++c)
    {
        if (!sliders_[c])
            continue;

        switch (c)
        {
        case RED:   sliders_[c]->setToolTip(QString::number(modeHSV() ? color_.Hue()           : color_.r_, 'd', 3)); break;
        case GREEN: sliders_[c]->setToolTip(QString::number(modeHSV() ? color_.SaturationHSV() : color_.g_, 'd', 3)); break;
        case BLUE:  sliders_[c]->setToolTip(QString::number(modeHSV() ? color_.Value()         : color_.b_, 'd', 3)); break;
        case ALPHA: sliders_[c]->setToolTip(QString::number(color_.a_, 'd', 3)); break;
        default: break;
        }
    }
}

void ColorPicker::updateColor()
{
    const Color previousColor{ color_ };

    for (int c{ RED }; c <= ALPHA; ++c)
    {
        if (!sliders_[c])
            continue;

        float colorValue{ sliders_[c]->sliderPosition() / 1000.f };

        if (colorWidget_)
        {
            if (c == ALPHA && colorWidget_->isSpecular())
                colorValue = pow(colorValue, SPECPOWER);
        }

        switch (c) {
        default:    break;
        case RED:   color_.r_ = colorValue; break;
        case GREEN: color_.g_ = colorValue; break;
        case BLUE:  color_.b_ = colorValue; break;
        case ALPHA: color_.a_ = colorValue; break;
        }
    }

    if (modeHSV())
        color_.FromHSV(color_.r_, color_.g_, color_.b_, color_.a_);

    if (color_ != previousColor)
    {
        updateToolTips();
        emit colorChanged(color_);
    }
}

void ColorPicker::paintEvent(QPaintEvent*)
{
    if (!colorLabel_)
        return;

    QPixmap pixmap{ colorLabel_->size() };
    paintCheckerboard(&pixmap, 12);
    QPainter p{ &pixmap };
    p.fillRect(QRect{ 1, 1, pixmap.width() - 2, pixmap.height() - 2 },
               toQColor(color_, (colorWidget_ ? colorWidget_->isDiffuse() : true)));
    p.end();

    colorLabel_->setPixmap(pixmap);
}

void ColorPicker::resizeEvent(QResizeEvent*)
{
    updateColorLabelVisibility();
    truncateLabels();

    int c{ tabBar_->count() };
    tabBar_->setStyleSheet("QTabBar::tab { font-size: " + QString::number(std::min((width() - 25) / (1 + c * c), 12)) +
                           "px;  height: 18px; width: " + QString::number(std::min((width() - 12) / c, 42)) + "px; }");

    if (colorWidget_)
    {
        if (FileWidget* dryWidget{ colorWidget_->dryWidget() })
            dryWidget->updatePickerBoxTitle();
    }
}

void ColorPicker::updateColorLabelVisibility()
{
    if (colorLabel_)
        colorLabel_->setVisible(height() > (64 + 64 * !modeHex()));
}

void ColorPicker::truncateLabels()
{
    for (int c{ RED }; c <= ALPHA; ++c)
    {
        QLabel* l{ labels_[c] };
        if (!l)
            continue;

        QString text{ (width() > 192 ? l->objectName()
                                     : l->objectName().left(1)) };
        if (text != l->text())
        {
            l->setText(text);
            l->setAlignment((text.length() == 1 ? Qt::AlignHCenter : Qt::AlignRight) | Qt::AlignVCenter);
        }
    }
}

void ColorPicker::updateFormLabels()
{
    for (int c{ RED }; c < ALPHA; ++c)
    {
        QLabel* l{ labels_[c] };

        if (!l)
            continue;

        switch (c)
        {
        case RED:   l->setObjectName(modeHSV() ? "Hue"        : "Red"  ); break;
        case GREEN: l->setObjectName(modeHSV() ? "Saturation" : "Green"); break;
        case BLUE:  l->setObjectName(modeHSV() ? "Value"      : "Blue" ); break;
        default: break;
        }
    }

    truncateLabels();
}

void ColorPicker::changeMode()
{
    mode_ = static_cast<ColorMode>(tabBar_->currentIndex());

    if (!modeHex())
    {
        for (QSlider* slider: sliders_.values())
        {
            if (!slider)
                continue;

            QGraphicsColorizeEffect* effect{ static_cast<QGraphicsColorizeEffect*>(slider->graphicsEffect()) };
            if (!effect)
                continue;

            effect->setEnabled(modeRGB());
        }

        updateSliderValues();
        updateFormLabels();
    }
}
