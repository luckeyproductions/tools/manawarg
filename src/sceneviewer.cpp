/* ManaWarg
// Copyright (C) 2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QMenu>
#include <QFileInfo>
#include <QRubberBand>
#include "qocoon.h"
#include "undostack/hierarchycommand.h"
#include "nodetree.h"
#include "transformer.h"

#include "sceneviewer.h"

std::vector<SceneViewer*> SceneViewer::sceneViewers_{};

SceneViewer::SceneViewer(Context* context, QWidget* parent): View3D(context, parent),
    astralScene_{ nullptr },
    astralJib_{ nullptr },
    astralIcons_{},
    astralModelNodes_{},
    astrallyRepresented_{},
    glowWire_{ nullptr },
    revertData_{ nullptr },
    clickStart_{},
    selectionRect_{ new QRubberBand{ QRubberBand::Rectangle, this } },
    transformer_{ nullptr }
{
    setObjectName("sceneviewer");
    setAcceptDrops(true);
    sceneViewers_.push_back(this);

    QAction* showAddMenuAction{ new QAction{ "Add", this } };
    showAddMenuAction->setShortcut(QKeySequence{ "Shift+A" });
    connect(showAddMenuAction, SIGNAL(triggered(bool)), this, SLOT(showAddMenu()));
    addAction(showAddMenuAction);

    createAstralscene();

    Selector* selector{ GetSubsystem<Selector>() };
    connect(selector, SIGNAL(selectionChanged(Selector*)), this, SLOT(selectionChanged()));
    connect(selector, SIGNAL(activeObjectChanged(Object*)), this, SLOT(selectionChanged()));

    transformer_ = new Transformer{ context_, this };
    addActions(transformer_->transformActions());
}

void SceneViewer::setScene(Scene* scene)
{
    View3D::setScene(scene);
}

void SceneViewer::setContinuousUpdate(bool enabled)
{
    if (continuousUpdate() == enabled)
        return;

    if (scene_)
    {
        if (enabled)
            storeScene();
        else
            restoreScene();
    }

    View3D::setContinuousUpdate(enabled);
}

void SceneViewer::storeScene()
{
    revertData_ = new XMLFile{ context_ };
    if (!scene_)
        return;

    XMLElement revertRoot{ revertData_->CreateRoot("scene") };
    scene_->SaveXML(revertRoot);
}

void SceneViewer::restoreScene()
{
    if (!revertData_ || !scene_)
        return;

    clearAstral();

    GetSubsystem<Selector>()->store();
    scene_->LoadXML(revertData_->GetRoot());
    GetSubsystem<NodeTree>()->updateTree(true);
    GetSubsystem<Selector>()->restore();
}

bool SceneViewer::isTransforming() const
{
    return transformer_->isTransforming();
}

void SceneViewer::createAstralscene()
{
    ResourceCache* cache{ GetSubsystem<ResourceCache>() };
    glowWire_ = cache->GetResource<Material>("Materials/GlowWire.xml")->Clone();
    glowWire_->SetShaderParameter("MatEmissiveColor", activeColor());
    astralScene_ = new Scene{ context_ };
    astralScene_->CreateComponent<Octree>();

    astralJib_ = astralScene_->CreateChild("Camera")->CreateComponent<Jib>();
    Zone* zone{ astralJib_->GetNode()->CreateComponent<Zone>() };
    zone->SetAmbientColor(Color::WHITE);
    zone->SetFogColor(Color::TRANSPARENT_BLACK);
    zone->SetFogStart(0.f);
    zone->SetFogEnd(1024.f);

    BillboardSet* nodeBillboards{ astralScene_->CreateComponent<BillboardSet>() };
    nodeBillboards->SetMaterial(cache->GetResource<Material>("Materials/Icon.xml"));
    astralIcons_[Node::GetTypeStatic()] = nodeBillboards;
}

void SceneViewer::updateAstralIcons()
{
    Selector* selector{ GetSubsystem<Selector>() };
    if (!selector)
        return;

    BillboardSet* nodeIcons{ astralIcons_[Node::GetTypeStatic()] };
    nodeIcons->SetNumBillboards(selector->selectedNodes().Size());

    unsigned n{ 0u };
    for (Node* node: selector->selectedNodes())
    {
        bool active{ selector->activeObject() == node};

        Billboard* b{ nodeIcons->GetBillboard(n) };
        b->enabled_ = true;
        b->position_ = node->GetWorldPosition();
        b->size_ = Vector2::ONE * .42f;
        b->color_ = active ? activeColor() : Color::WHITE;

        ++n;
    }

    nodeIcons->Commit();

    for (BillboardSet* set: astralIcons_.Values())
    {
        if (set != nodeIcons)
            set->SetNumBillboards(0);
    }

    for (Component* c: selector->selectedComponents())
    {
        if (!c)
            continue;

        Node* node{ c->GetNode() };
        if (!node)
            continue;

        if (!astralIcons_[c->GetType()])
        {
            ResourceCache* cache{ GetSubsystem<ResourceCache>() };

            astralIcons_[c->GetType()] = astralScene_->CreateComponent<BillboardSet>();
            astralIcons_[c->GetType()]->SetMaterial(cache->GetResource<Material>("Materials/Icon.xml")->Clone());
            SharedPtr<Texture2D> iconTexture{ cache->GetResource<Texture2D>("UI/" + c->GetTypeName() + ".png") };

            if (!iconTexture)
                iconTexture = cache->GetResource<Texture2D>("UI/Component.png");

            iconTexture->SetAddressMode(COORD_U, ADDRESS_CLAMP);
            iconTexture->SetAddressMode(COORD_V, ADDRESS_CLAMP);
            astralIcons_[c->GetType()]->GetMaterial()->SetTexture(TU_DIFFUSE, iconTexture);
        }

        BillboardSet* set{ astralIcons_[c->GetType()] };
        if (set)
        {
            set->SetNumBillboards(set->GetNumBillboards() + 1u);
            bool active{ selector->activeObject() == c};

            Billboard* b{ set->GetBillboard(set->GetNumBillboards() - 1) };
            b->enabled_ = true;
            b->position_ = node->GetWorldPosition();
            b->size_ = Vector2::ONE * .42f;
            b->color_ = active ? activeColor() : Color::WHITE;
        }
    }

    for (BillboardSet* set: astralIcons_.Values())
    {
        if (set != astralIcons_[Node::GetTypeStatic()])
            set->Commit();
    }
}

void SceneViewer::clearAstral()
{
    for (Node* n: astralModelNodes_)
        n->Remove();

    astralModelNodes_.Clear();
    astrallyRepresented_.Clear();
}

void SceneViewer::updateAstralModels()
{
    clearAstral();

    Selector* selector{ GetSubsystem<Selector>() };

    for (Component* c: selector->selectedComponents())
    {
        if (!c || astrallyRepresented_.Contains(c))
            continue;

        const StringHash type{ c->GetType() };

        if (type == StaticModel::GetTypeStatic())
        {
            StaticModel* staticModel{ static_cast<StaticModel*>(c) };
            addAstralStaticModel(staticModel->GetNode(), staticModel->GetModel());
        }
        else if (type == AnimatedModel::GetTypeStatic())
        {
        }
        else if (type == CollisionShape::GetTypeStatic())
        {
            addAstralCollisionshape(static_cast<CollisionShape*>(c));
        }
        else if (type == RigidBody::GetTypeStatic())
        {
            PODVector<Component*> shapes{};
            c->GetNode()->GetComponents(shapes, CollisionShape::GetTypeStatic()/*, true? */);
            for (Component* shape: shapes)
                addAstralCollisionshape(static_cast<CollisionShape*>(shape));
        }
        else if (type == Camera::GetTypeStatic())
        {
            //Display frustrum
        }
        else if (type == Light::GetTypeStatic())
        {
            //Display shape
        }
        else if (type == Zone::GetTypeStatic())
        {
            //Display bounding box
        }

        if (!astrallyRepresented_.Contains(c))
            astrallyRepresented_.Push(c);
    }
}

Node* SceneViewer::addAstralStaticModel(Node* node, Model* model)
{
    if (!model)
        return nullptr ;

    Node* astralNode{ node->CreateChild("ASTRAL") };
    astralNode->SetTemporary(true);

    StaticModel* astralModel{ astralNode->CreateComponent<StaticModel>() };
    astralModel->SetModel(model);
    astralModel->SetMaterial(glowWire_);

    astralModelNodes_.Push(astralNode);
    return astralNode;
}

void SceneViewer::addAstralCollisionshape(CollisionShape* collisionShape)
{
    const ShapeType shape{ collisionShape->GetShapeType() };
    const bool basicShape{ shape < SHAPE_TRIANGLEMESH };

    if ((!basicShape && !collisionShape->GetModel()) || astrallyRepresented_.Contains(collisionShape))
        return;

    ResourceCache* cache{ GetSubsystem<ResourceCache>() };
    SharedPtr<Model> model{ nullptr };
    if (!basicShape)
    {
        model = collisionShape->GetModel();
    }
    else switch (shape)
    {
    default: break;
    case SHAPE_BOX: model = cache->GetTempResource<Model>("Models/WireBox.mdl");
    }

    Node* astralNode{ addAstralStaticModel(collisionShape->GetNode(), model) };
    if (!astralNode)
        return;

    astralNode->Translate(collisionShape->GetPosition());
    astralNode->Rotate(collisionShape->GetRotation());
    astralNode->Scale(collisionShape->GetSize());

    astrallyRepresented_.Push(collisionShape);
}

void SceneViewer::selectionChanged()
{
    updateAstralScene();
    updateView();
}

void SceneViewer::showAddMenu()
{
    const QPoint mousePos{ QCursor::pos() };

    if (rect().contains(mapFromGlobal(mousePos)))
        GetSubsystem<Qocoon>()->addMenu()->exec(mousePos);
}

void SceneViewer::updateAstralScene()
{
    updateAstralJib();
    updateAstralIcons();
    updateAstralModels();
}

void SceneViewer::updateViewport()
{
    if (!activeCamera_)
        return;

    RenderSurface* renderSurface{ renderTexture_->GetRenderSurface() };

    if (renderSurface->GetNumViewports() != 2)
        renderSurface->SetNumViewports(2);

    if (renderSurface->GetViewport(0))
    {
        renderSurface->GetViewport(0)->SetScene(astralScene_);
    }
    else
    {
        SharedPtr<RenderPath> renderPath{ new RenderPath{} };
        renderPath->Load(GetSubsystem<ResourceCache>()->GetResource<XMLFile>("RenderPaths/ForwardTransparent.xml"));
        SharedPtr<Viewport> viewport{ new Viewport{ context_, astralScene_, astralJib_, renderPath } };
        renderSurface->SetViewport(0, viewport);
    }

    if (renderSurface->GetViewport(1))
    {
        renderSurface->GetViewport(1)->SetScene(scene_);
    }
    else
    {
        SharedPtr<Viewport> viewport{ new Viewport{ context_, scene_, jib_, GetSubsystem<Renderer>()->GetDefaultRenderPath() } };
        renderSurface->SetViewport(1, viewport);
    }

    if (height())
    {
        float portraitRatio{ Min(1.f, static_cast<float>(width()) / height()) };

        if (activeCamera_ != jib_)
            jib_->SetZoom(activeCamera_->GetZoom() * portraitRatio);
        else
            jib_->SetZoom(portraitRatio);

        astralJib_->SetZoom(jib_->GetZoom());
    }

    updateView();
}

void SceneViewer::updateAstralJib()
{
    astralJib_->GetNode()->SetTransform(jib_->GetNode()->GetTransform());
    astralJib_->SetFov(jib_->GetFov());
    astralJib_->SetZoom(jib_->GetZoom());
}

void SceneViewer::updateView()
{
    if (scene_->IsUpdateEnabled())
        scene_->Update(GetSubsystem<Time>()->GetTimeStep());

    updateAstralScene();

    View3D::updateView();
}

void SceneViewer::mousePressEvent(QMouseEvent* event)
{
    if (!scene_)
        return;

    const Qt::MouseButtons buttons{ QApplication::mouseButtons() };

    if (transformer_->isTransforming())
    {
        if (buttons & Qt::LeftButton)
            transformer_->endTransformation();
        else if (buttons & Qt::RightButton)
            transformer_->cancelTransformation();

        return;
    }

    if ( buttons & Qt::MiddleButton ||
        (buttons & Qt::RightButton && buttons & Qt::LeftButton)) // Prepare for camera rotation
    {
        jib_->updatePivot();
    }
    else if (buttons & Qt::LeftButton)
    {
        Qt::KeyboardModifiers modifiers{ event->modifiers() };
        const bool append{ (modifiers & Qt::ShiftModifier) != 0 };
        Selector* selector{ GetSubsystem<Selector>() };
        selector->store();

        Object* toSelect{ sceneRaycastFirst(modifiers) };
        if (toSelect)
        {
            if (!selector->isActive(toSelect) || !(append))
                selector->select(toSelect, append);
            else if (modifiers & Qt::ShiftModifier)
                selector->deselect(toSelect);
        }
        else if (!append)
        {
            selector->deselectAll();
        }

        clickStart_ = event->pos();
        selectionRect_->setGeometry({ clickStart_, QSize{} });
    }
}

void SceneViewer::mouseMoveEvent(QMouseEvent* event)
{
    if (!scene_)
        return;

    const QPoint dPos{ QCursor::pos() - previousMousePos_ };
    if (transformer_->isTransforming())
    {
        transformer_->handleMouseMove(dPos);
        wrapCursor(event->pos());
        previousMousePos_ = QCursor::pos();

        return;
    }

//    setFocus();

    const Vector2 dVec{ dPos.x() * 1.25e-3f, dPos.y() * 1.666e-3f };
    const Qt::MouseButtons buttons{ QApplication::mouseButtons() };
    const Vector2 posNormalized{ static_cast<float>(event->pos().x()) / (width()  - 1),
                                 static_cast<float>(event->pos().y()) / (height() - 1)};
    const Qt::KeyboardModifiers modifiers{ event->modifiers() };

    jib_->updateMouseRay(posNormalized);

    if (!selectionRect_->isVisible())
    {
        if ( buttons & Qt::MiddleButton ||
             (buttons & Qt::RightButton && buttons & Qt::LeftButton))
        {

            if (modifiers & Qt::ShiftModifier && modifiers & Qt::ControlModifier) //Change field of view
                jib_->move(Vector3::ONE * dVec.y_, MT_FOV);
            else if (modifiers & Qt::ShiftModifier) //Pan camera
                jib_->move(Vector3(dVec.x_, dVec.y_, 0.f), MT_PAN);
            else if (modifiers & Qt::ControlModifier) //Zoom camera
                jib_->move(Vector3::FORWARD * -dVec.y_, MT_PAN);
            else // Rotate camera around a projected point
                jib_->move(Vector3{ dVec.x_, dVec.y_, 0.f }, MT_ROTATE);

            wrapCursor(event->pos());
            updateView();
        }
        else if (buttons & Qt::RightButton)
        {
            if (modifiers & Qt::ShiftModifier) //Pan camera
                jib_->move({ dVec.x_, dVec.y_, 0.f }, MT_PAN);
            else // Turn camera
                jib_->move({ dVec.x_, dVec.y_, 0.f }, MT_TURN);

            wrapCursor(event->pos());
            updateView();
        }
        else if (buttons & Qt::LeftButton)
        {
            const QRect bandRect{ QRect{ clickStart_, event->pos() }.normalized() };
            selectionRect_->setGeometry(bandRect);
            if (bandRect.size().width() * bandRect.size().height() > 4)
                selectionRect_->show();
        }
    }
    else
    {
        const QRect bandRect{ QRect{ clickStart_, event->pos() }.normalized() };
        selectionRect_->setGeometry(bandRect);
        updateSelection((modifiers & Qt::ShiftModifier) != 0);
    }

    previousMousePos_ = QCursor::pos();
}

void SceneViewer::mouseReleaseEvent(QMouseEvent* event)
{
    if (selectionRect_->isVisible())
        selectionRect_->hide();
}

void SceneViewer::wheelEvent(QWheelEvent* event)
{
    if (!scene_)
        return;

    jib_->updatePivot();
    const int vScroll{ event->angleDelta().y() };
    jib_->move(Vector3::FORWARD * vScroll * 5.e-5f * jib_->pivotDot(), MT_PAN, true);

    updateView();
}

void SceneViewer::updateSelection(bool append)
{
    QRect selectionRect{ selectionRect_->geometry() };

    HashSet<Object*> toSelect{};
    PODVector<Node*> allNodes{};
    scene()->GetChildren(allNodes, true);
    for (Node* n: allNodes)
    {
        const Vector2 nodeScreenPos{ jib_->WorldToScreenPos(n->GetWorldPosition()) };
        const QPoint screenPoint{ RoundToInt(nodeScreenPos.x_ * width()),
                    RoundToInt(nodeScreenPos.y_ * height()) };
        if (selectionRect.contains(screenPoint))
            toSelect.Insert(n);
    }

    Selector* selector{ GetSubsystem<Selector>() };
    if (append)
        selector->restore();
    selector->select(toSelect, append);
}

void SceneViewer::dragEnterEvent(QDragEnterEvent* event)
{
    const QMap<int, QVariant> dataMap{ extractDataMap(event->mimeData()) };

    if (isOfType<Scene>(event->mimeData()) && !dataMap.contains(ID))
        event->acceptProposedAction();
}

void SceneViewer::dropEvent(QDropEvent* event)
{
    const String fileName{ toString(extractDataMap(event->mimeData())[FileName].toString()) };
    GetSubsystem<Qocoon>()->openScene(fileName, this);
}

Object* SceneViewer::sceneRaycastFirst(Qt::KeyboardModifiers modifiers)
{
    Object* object{ nullptr };
    const QPoint localCursorPos{ mapFromGlobal(QCursor::pos()) };
    const float xNorm{ static_cast<float>(localCursorPos.x()) / width() };
    const float yNorm{ static_cast<float>(localCursorPos.y()) / height() };
    const Vector2 posNormalized{ xNorm, yNorm };
    const Ray mouseRay{ jib_->GetScreenRay(posNormalized) };

    if (!(modifiers & Qt::ControlModifier))
    {
        if (Octree* octree{ scene_->GetComponent<Octree>() })
        {
            PODVector<RayQueryResult> result{};
            RayOctreeQuery query(result, mouseRay, RAY_TRIANGLE, M_INFINITY, DRAWABLE_GEOMETRY);
            octree->RaycastSingle(query);

            if (result.Size())
                object = result.Front().drawable_;
        }
    }

    if (!object) //\\\ Should select CollisionShape when alt is not pressed
    {
        if (PhysicsWorld* physicsWorld{ scene_->GetComponent<PhysicsWorld>() })
        {
            PhysicsRaycastResult result{};
            physicsWorld->RaycastSingle(result, mouseRay, M_LARGE_VALUE);

            object = result.body_;
        }
    }

    if (!(modifiers & Qt::AltModifier))
    {
        if (Component* component{ static_cast<Component*>(object) })
            object = component->GetNode();
    }

    return object;
}
