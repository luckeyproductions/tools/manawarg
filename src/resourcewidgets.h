/* ManaWarg
// Copyright (C) 2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef RESOURCEWIDGETS_H
#define RESOURCEWIDGETS_H

#include <QVBoxLayout>
#include "utility.h"

#include <QLineEdit>

class ResourceLineEdit: public QLineEdit
{
    Q_OBJECT

public:
    ResourceLineEdit(StringHash type, QWidget* parent = nullptr);

protected:
    void dragEnterEvent(QDragEnterEvent* event) override;
    void dropEvent(QDropEvent* event) override;

private:
    StringHash resourceType_;

};

class ResourceListWidget: public QWidget
{
    Q_OBJECT

public:
    ResourceListWidget(StringHash type, QWidget* parent = nullptr);

    void setAttributeName(const QString& name) { attributeName_ = toString(name); }
    void setNumResources(int count);
    void setValues(const Variant& values);
    ResourceRefList refList() const;

signals:
    void editingFinished();

public slots:
    void updateNumResources(Serializable* serializable);

private:
    void clear();
    void addResourceEdit();
    void storeValues();
    void restoreValues();

    String attributeName_;
    StringHash resourceType_;
    QVBoxLayout* layout_;
    QVector<ResourceLineEdit*> resourceWidgets_;
    QStringList valueStore_;
};

#endif // RESOURCEWIDGETS_H
