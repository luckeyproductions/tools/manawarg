/* ManaWarg
// Copyright (C) 2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QWidget>
#include <QTimer>
#include <QDebug>
#include <QSettings>
#include <QFileDialog>
#include <QMessageBox>
#include <QThread>
#include "qocoon.h"
#include "resourcebrowser/resourcebrowser.h"
#include "selector.h"
#include "utility.h"

#include "weaver.h"

Context* Weaver::context_s{ nullptr };

Weaver::Weaver(int& argc, char** argv, Context* context): QApplication(argc, argv), Application(context),
    argument_{},
    requireUpdate_{ true },
    qocoon_{ nullptr }
{
    context_s = context_;

    std::locale::global(std::locale::classic());

    const QString displayName{ "ManaWarg" };
    QCoreApplication::setOrganizationName("LucKey Productions");
    QCoreApplication::setOrganizationDomain("luckey.games");
    QCoreApplication::setApplicationName(displayName.toLower());
    QGuiApplication::setApplicationDisplayName(displayName);

    if (argc == 2)
    {
        const String argument{ argv[1] };

        if (GetExtension(argument) == ".xml")
            argument_ = argument;
    }
}

Weaver::~Weaver()
{
    delete qocoon_;
}

void Weaver::Setup()
{
    FileSystem* fs{ GetSubsystem<FileSystem>() };
    String manaWargResourcePath{ "Resources" };

    if (!fs->DirExists(fs->GetProgramDir() + manaWargResourcePath))
        manaWargResourcePath = fs->GetAppPreferencesDir("luckey", toString(applicationName())); //Assume ManaWarg is installed

    engineParameters_[EP_RESOURCE_PATHS] = manaWargResourcePath;
    engineParameters_[EP_WORKER_THREADS] = false;
    engineParameters_[EP_EXTERNAL_WINDOW] = (void*)(new QWidget{})->winId();
    engineParameters_[EP_LOG_NAME] = fs->GetAppPreferencesDir("luckey", "logs") +
                                     toString(applicationName()) + ".log";
}

void Weaver::Start()
{
    SetRandomSeed(GetSubsystem<Time>()->GetSystemTime());

    ResourceCache* cache{ GetSubsystem<ResourceCache>() };
    cache->SetAutoReloadResources(true);
//    SharedPtr<XMLFile> renderPathFile{ cache->GetResource<XMLFile>("RenderPaths/ForwardTransparent.xml") };
//    GetSubsystem<Renderer>()->SetDefaultRenderPath(renderPathFile);

    context_->RegisterSubsystem(this);
    context_->RegisterSubsystem<Script>();
    context_->RegisterSubsystem(new Selector{ context_, this });
    context_->RegisterFactory<Jib>();

    if (!argument_.IsEmpty())
        handleArgument();

    if (!qocoon_)
        qocoon_ = new Qocoon{ context_ };

    QTimer timer;
    connect(&timer, SIGNAL(timeout()), this, SLOT(onTimeout()));
    timer.start(1000 / GetSubsystem<Graphics>()->GetRefreshRate());

    exec();
    exit();
}

void Weaver::Stop()
{
    engine_->DumpResources(true);
}

void Weaver::exit()
{
    engine_->Exit();
}

bool Weaver::looksLikeUrho(const QString& urhoFolder) const
{
   return looksLikeUrho(toString(urhoFolder));
}

bool Weaver::looksLikeUrho(const String& urhoFolder) const
{
    FileSystem* fs{ GetSubsystem<FileSystem>() };

    return fs->DirExists(urhoFolder + "/bin/Data")
        && fs->DirExists(urhoFolder + "/bin/CoreData");
}

void Weaver::handleArgument()
{
    FileSystem* fs{ GetSubsystem<FileSystem>() };

    if (!IsAbsolutePath(argument_))
    {
        while (argument_.StartsWith("../")) // Resolve basic relativity
        {
            fs->SetCurrentDir(GetParentPath(fs->GetCurrentDir()));
            argument_ = argument_.Substring(3);
        }

        argument_ = fs->GetCurrentDir() + argument_;
    }

    ResourceCache* cache{ GetSubsystem<ResourceCache>() };
    SharedPtr<XMLFile> xmlFile{ cache->GetTempResource<XMLFile>(argument_) };
    const String resourceRoot{ Qocoon::locateResourceRoot(xmlFile->GetName()) };

    if (xmlFile)
    {
        const String rootName{ xmlFile->GetRoot().GetName() };
        if (rootName == "project")
        {
            qocoon_ = new Qocoon(context_);
            qocoon_->loadProject(toQString(resourceRoot + FILENAME_PROJECT));
        }
        else if (rootName == "scene")
        {
            qocoon_ = new Qocoon(context_);

            if (!qocoon_->loadProject(toQString(resourceRoot + FILENAME_PROJECT)))
                qocoon_->newProject(toQString(GetPath(xmlFile->GetName())));

            Project* project{ qocoon_->project() };
            if (project)
            {
                if (!project->inResourceFolder(xmlFile->GetName()))
                    qocoon_->addResourceFolder();

                qocoon_->openScene(xmlFile->GetName());
            }
        }
        else
        {
            qocoon_ = new Qocoon{ context_, xmlFile};
        }
    }
}

void Weaver::onTimeout()
{
    if (requireUpdate_ && !qocoon_->isMinimized()
        && engine_ && !engine_->IsExiting())
    {
        runFrame();
    }
}

void Weaver::runFrame()
{
    requireUpdate_ = false;
    engine_->RunFrame();

//    qDebug() << GetSubsystem<Time>()->GetTimeStamp().CString() << " Ran frame";
}
