/* ManaWarg
// Copyright (C) 2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QSpacerItem>
#include <QFileInfo>
#include <QSettings>
#include "qocoon.h"

#include "noprojectwidget.h"

NoProjectWidget::NoProjectWidget(Context* context, QWidget *parent): QWidget(parent), Object(context),
    mainLayout_{ new QVBoxLayout() }, // Main layout \\\ Should be scroll area
    recentHeader_{ new QLabel{ "Recent Projects" }, new QFrame{} },
    recentButtons_{}
{
    QVBoxLayout* rootLayout{ new QVBoxLayout() }; // Has main frame as only direct child
    rootLayout->setContentsMargins(0, 2, 0, 0);
    QFrame* mainFrame{ new QFrame(this) }; // Sunken frame around main layout
    mainFrame->setFrameShape(QFrame::Panel);
    mainFrame->setFrameShadow(QFrame::Sunken);
    mainFrame->setLineWidth(1);
    rootLayout->addWidget(mainFrame);
    setLayout(rootLayout);

    mainLayout_->setContentsMargins(64, 8, 64, 8);
    mainLayout_->setSpacing(3);
    mainFrame->setLayout(mainLayout_);

    QLabel* logo{ new QLabel() }; // Add downscaled logo
    QPixmap pixmap{ ":/Logo" };
    logo->setPixmap(pixmap.scaled(pixmap.size() / 3, Qt::KeepAspectRatio, Qt::SmoothTransformation));
    logo->setAlignment(Qt::AlignHCenter);
    mainLayout_->addWidget(logo);

    mainLayout_->addSpacing(12);

    for (bool first: { true, false }) // Add New Project and Open Project buttons
    {
        const QString name{ (first ? "New" : "Open") };
        QPushButton* button{ new QPushButton(QIcon(":/" + name), name + " Project...") };
        button->setFixedWidth(pixmap.width() / 3);
        mainLayout_->addWidget(button);
        mainLayout_->setAlignment(button, Qt::AlignCenter);

        if (first)
            connect(button, SIGNAL(clicked(bool)), this, SIGNAL(newProject()));
        else
            connect(button, SIGNAL(clicked(bool)), this, SIGNAL(openProject()));
    }

    mainLayout_->addSpacing(6);
    createRecentHeader();
    mainLayout_->addSpacerItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Expanding));
}

void NoProjectWidget::createRecentHeader()
{
    recentHeader_.first->setEnabled(false);
    recentHeader_.first->setAlignment(Qt::AlignHCenter);
    mainLayout_->addWidget(recentHeader_.first);

    recentHeader_.second->setFrameShape(QFrame::HLine);
    recentHeader_.second->setFrameShadow(QFrame::Sunken);
    mainLayout_->addWidget(recentHeader_.second);
}

void NoProjectWidget::updateRecentList()
{
    QSettings settings{};
    QStringList recentProjects{ settings.value("recentprojects").toStringList() };
    const bool anyRecent{ !recentProjects.isEmpty() };

    for (QPushButton* b: recentButtons_) // Clear recent projects list
        b->deleteLater();

    recentButtons_.clear();

    recentHeader_.first->setVisible(anyRecent);
    recentHeader_.second->setVisible(anyRecent);

    for (const QString& fileName: recentProjects) // Compose recent project list
    {
        if (fileName.isEmpty())
            continue;

        SharedPtr<XMLFile> projectFile{ GetSubsystem<ResourceCache>()
                    ->GetResource<XMLFile>(toString(fileName)) };

        if (projectFile.IsNull() || projectFile->GetRoot("project").IsNull() )
            continue;

        QString projectName{ projectFile->GetRoot("project").GetAttributeCString("name") };

        if (projectName.isEmpty())
            projectName = "Untitled";

        QPushButton* recentButton{ createRecentProjectButton(fileName, projectName) };
        mainLayout_->insertWidget(mainLayout_->count() - 1, recentButton);
        recentButtons_.push_back(recentButton);

        Qocoon* qocoon{ GetSubsystem<Qocoon>() };
        connect(recentButton, SIGNAL(clicked(bool)), qocoon, SLOT(openRecent()));
    }
}

QPushButton* NoProjectWidget::createRecentProjectButton(const QString& fileName, const QString& projectName)
{
    QPushButton* recentButton{ new QPushButton{} };

    QFont font{ recentButton->font() };
    font.setPixelSize(16);
    font.setBold(true);

    recentButton->setFont(font);
    recentButton->setObjectName(fileName);
    recentButton->setToolTip(fileName);
    recentButton->setText(projectName);
    recentButton->setFlat(true);
    recentButton->setMaximumHeight(24);
    recentButton->setCursor(Qt::PointingHandCursor);

    return recentButton;
}
