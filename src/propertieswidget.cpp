/* ManaWarg
// Copyright (C) 2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QDesktopServices>
#include <QPushButton>
#include <QFileDialog>
#include <QMessageBox>
#include "undostack/attributecommand.h"
#include "colorwidget.h"
#include "texturewidget.h"
#include "materialeditor.h"
#include "filewidget.h"
#include "utility.h"
#include "qocoon.h"
#include "project.h"

#include "propertieswidget.h"

PropertiesWidget::PropertiesWidget(Context* context, FileWidget* parent): QScrollArea(parent), Object(context),
    dryWidget_{ parent },
    object_{ nullptr },
    propertiesLayout_{ new QFormLayout{ this } },
    containerWidget_{ new QWidget{ this } },
    rows_{},
    ignoreModifications_{ false }
{
    propertiesLayout_->setSpacing(2);
    propertiesLayout_->setContentsMargins(4, 2, 1, 0);

    propertiesLayout_->setLabelAlignment(Qt::AlignRight | Qt::AlignVCenter);
    containerWidget_->setLayout(propertiesLayout_);
    setWidget(containerWidget_);
    setMinimumWidth(81);
    setMinimumHeight(36);
}

void PropertiesWidget::clear()
{
    while (propertiesLayout_->rowCount())
        propertiesLayout_->removeRow(0);

    rows_.clear();
}

void PropertiesWidget::resizeEvent(QResizeEvent* event)
{
    containerWidget_->resize(QSize(viewport()->width(), std::max(containerWidget_->sizeHint().height(), viewport()->height())));

    updateLabels();

    QScrollArea::resizeEvent(event);
}

void PropertiesWidget::updateLabels()
{
    for (PropertyRow* row: rows_)
    {
        QLabel* label{ row->label() };
        QString text{ label->objectName() };

        propertiesLayout_->setLabelAlignment(Qt::AlignRight | Qt::AlignVCenter);

        if (propertiesLayout_->rowWrapPolicy() == QFormLayout::DontWrapRows)
        {
            if (width() < 160)
            {
                text = text.at(0);
                propertiesLayout_->setLabelAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

            }
            else if (width() < 192)
            {
                if (text.contains("Emis"))
                    text = "Emit";
                else
                    text = text.left(4);
            }
        }

        if (label->text() != text)
            label->setText(text);
    }
}

void PropertiesWidget::setObject(Object* object)
{
    if (object == object_)
        return;

    const StringHash prevType{ (object_ ? object_->GetType() : StringHash::ZERO) };
    object_ = object;

    if (!object_)
    {
        clear();
        return;
    }
    else if (object_->GetType() == prevType)
    {
        updatePropertyFields();
        if (dryWidget_ && dryWidget_->pickerBox())
        {
            QLayout* pickerLayout{ dryWidget_->pickerBox()->layout() };
            if (pickerLayout->count())
            {
                ColorPicker* colorPicker{ static_cast<ColorPicker*>(pickerLayout->itemAt(0)->widget()) };
                colorPicker->updateSliderValues();
            }
        }
    }
    else
    {
        createPropertyFields();
    }
}

void PropertiesWidget::setPropertiesVisible(bool visible)
{
    containerWidget_->setVisible(visible);
}

void PropertiesWidget::propertyModified(const String& attributeName)
{
    if (ignoreModifications_)
        return;

    if (object_->GetTypeInfo()->IsTypeOf<Serializable>() && !attributeName.IsEmpty())
    {
        AttributeCommand* attributeModification{ new AttributeCommand{ static_cast<Serializable*>(object_.Get()), attributeName } };
        attributeModification->setNewValue(attributeValue(attributeName));

        if (scene() && attributeModification->isChange())
        {
            Qocoon* qocoon{ GetSubsystem<Qocoon>() };
            qocoon->sceneStack(scene())->push(attributeModification);
        }
        else
        {
            delete attributeModification;
        }
    }
    else if (object_->GetTypeInfo()->IsTypeOf<Material>())
    {
        static_cast<MaterialEditor*>(dryWidget_)->updateMaterial(sender());
    }
}

void PropertiesWidget::createPropertyFields()
{
    clear();

    if (!object_)
        return;

    if (object_->GetTypeInfo()->IsTypeOf<Material>())
        createMaterialPropertyFields();
    else if (object_->GetTypeInfo()->IsTypeOf<Serializable>())
        createAttributeFields();

    updatePropertyFields();
}

void PropertiesWidget::createMaterialPropertyFields()
{
    createPropertyRow("Technique", PT_Technique);

    for (QString colorName: { "Diffuse", "Specular", "Emissive" })
        createPropertyRow(colorName, PT_ColorAndTexture);

    createPropertyRow("Normal", PT_Texture);

    resizeEvent(nullptr);
}

void PropertiesWidget::createAttributeFields()
{
    Serializable* serializable{ static_cast<Serializable*>(object_.Get()) };

    for (const AttributeInfo& attribute: *serializable->GetAttributes())
    {
        if (attribute.enumNames_)
        {
            PropertyRow* row{ createPropertyRow(toQString(attribute.name_), PT_Enum) };

            QStringList enumNames{};
            const char** enumNamePtrs{ attribute.enumNames_ };
            while (enumNamePtrs && *enumNamePtrs)
                enumNames.push_back(QString{ *enumNamePtrs++ });

            row->setOptions(enumNames);
        }
        else
        {
            const PropertyType propertyType{ toPropertyType(attribute.type_) };
            if (propertyType == PT_Resource || propertyType == PT_ResourceList)
            {
                int list{ false };

                StringHash resourceType{ StringHash::ZERO };
                if (attribute.type_ == VAR_RESOURCEREF)
                {
                    resourceType = attribute.defaultValue_.GetResourceRef().type_;
                }
                else if (attribute.type_ == VAR_RESOURCEREFLIST)
                {
                    ResourceRefList defaultRefList{ attribute.defaultValue_.GetResourceRefList() };
                    resourceType = defaultRefList.type_;

                    list = true;
                }

                createResourcePropertyRow(toQString(attribute.name_), resourceType, list);
            }
            else if (propertyType != PT_None)
            {
                createPropertyRow(toQString(attribute.name_), propertyType);
            }
        }
    }

    delayedResize();
}

PropertyRow* PropertiesWidget::createPropertyRow(const QString& text, PropertyType type)
{
    PropertyRow* row{ new PropertyRow{ text, type, this } };

    row->setObjectName(text);
    rows_.push_back(row);
    propertiesLayout_->addRow(row->label(), row->layout());

    return row;
}

PropertyRow* PropertiesWidget::createResourcePropertyRow(const QString& text, StringHash resourceType, bool list)
{
    PropertyRow* row{ new PropertyRow(text, resourceType, list, this) };

    row->setObjectName(text);
    rows_.push_back(row);
    propertiesLayout_->addRow(row->label(), row->layout());

    return row;
}

void PropertiesWidget::pickTechnique()
{
    const QString projectFolder{ toQString(Qocoon::projectLocation()) };
    const QString homeFolder{ QStandardPaths::writableLocation(QStandardPaths::HomeLocation) };
    QString startFolder{ projectFolder };

    if (projectFolder.isEmpty())
    {
        startFolder = homeFolder;
    }
    else if (Qocoon::project()->resourceFolders().Size() == 1)
    {
        startFolder = toQString(Qocoon::project()->location() + Qocoon::project()->resourceFolders().Front());
    }

    QString xmlFilter{ tr("*.xml") };
    String techniqueFileName{toString(
                    QFileDialog::getOpenFileName(this, tr("Pick Technique"), startFolder, xmlFilter, &xmlFilter)) };

    if (techniqueFileName.IsEmpty())
    {
        return; //Cancelled
    }
    else
    {
        ResourceCache* cache{ GetSubsystem<ResourceCache>() };
        SharedPtr<XMLFile> xmlFile{ cache->GetTempResource<XMLFile>(techniqueFileName) };

        if (xmlFile)
        {
            const XMLElement& rootElem{ xmlFile->GetRoot("technique") };
            const QString errorTitle{ "Could not set technique" };

            if (rootElem.IsNull())
            {
                QMessageBox::warning(this, errorTitle, "File not recognized as technique"  );
            }
            else
            {
                if (Qocoon::project() && Qocoon::project()->inResourceFolder(techniqueFileName))
                {
                    String trimmedName{ Qocoon::trimmedResourceName(techniqueFileName) };
                    Technique* technique{ cache->GetResource<Technique>(trimmedName, false) };

                    if (technique)
                    {
                        MaterialEditor* me{ static_cast<MaterialEditor*>(dryWidget_) };
                        if (me->material()->GetTechnique(0) != technique)
                        {
                            me->setTechnique(technique);
                            updatePropertyFields();

                            Log::Write(LOG_INFO, "Loaded technique: " + trimmedName);
                        }
                    }
                    else
                    {
                        QMessageBox::warning(this, errorTitle, "Technique failed to load"  );

                        Log::Write(LOG_INFO, "Faled to load: " + trimmedName);
                    }
                }
                else
                {
                    QMessageBox::warning(this, errorTitle, "Technique exists outside resource tree"  );
                }
            }
        }
    }
}

void PropertiesWidget::updatePropertyFields()
{
    if (object_->GetTypeInfo()->IsTypeOf<Material>())
    {
        Material* material{ static_cast<Material*>(object_.Get()) };
        if (!material)
            return;

        SharedPtr<Technique> technique{ material->GetTechnique(0) };
        const HashMap<StringHash, MaterialShaderParameter>& parameters{ material->GetShaderParameters() };

        for (PropertyRow* row: rows_)
        {
            switch (row->type())
            {
            default: break;
            case PT_Technique:
            {
                QLineEdit* le{ qobject_cast<QLineEdit*>(row->propertyWidget(PT_Technique)) };

                if (technique)
                {
                    le->setText(toQString(technique->GetName().Split('/').Back().Split('.').Front()));
                    le->setToolTip(toQString(technique->GetName()));
                }
                else
                {
                    le->setText("");
                    le->setToolTip("No technique");
                }
            }
            break;
            case PT_Texture: case PT_ColorAndTexture:
            {
                TextureWidget* tw{ qobject_cast<TextureWidget*>(row->propertyWidget(PT_Texture)) };

                if (tw)
                {
                    if (techniqueUsesUnit(technique, tw->textureUnit()))
                    {
                        tw->setVisible(true);

                        if (material)
                        {
                            Texture* texture{ material->GetTexture(tw->textureUnit()) };
                            tw->setTexture(texture);
                        }
                        else
                        {
                            tw->setTexture(nullptr);
                        }
                    }
                    else
                    {
                        tw->setVisible(false);
                    }

                    if (row->type() == PT_Texture)
                        row->label()->setVisible(tw->isVisible());
                }

                if (row->type() == PT_Texture)
                    break;
                else
                    [[fallthrough]];
            }
            case PT_Color:
            {
                ColorWidget* cw{ static_cast<ColorWidget*>(row->propertyWidget(PT_Color)) };
                String parameterName{ toString(cw->property("parameter").toString()) };
                MaterialShaderParameter parameter{};

                if (cw)
                {
                    if (parameters.TryGetValue(parameterName, parameter))
                    {
                        if (parameter.value_.GetType() == VAR_VECTOR3)
                            cw->setColor(Color{ parameter.value_.GetVector3() });
                        else if (parameter.value_.GetType() == VAR_VECTOR4 || parameter.value_.GetType() == VAR_COLOR)
                            cw->setColor(parameter.value_.GetColor());
                        else
                            cw->setColor(Color::BLACK);
                    }
                    else
                        cw->setVisible(false);
                }
            }
            break;
            }
        }

        updateLabels();
    }
    else if (object_->GetTypeInfo()->IsTypeOf<Serializable>())
    {
        Serializable* serializable{ static_cast<Serializable*>(object_.Get()) };
        if (!serializable)
            return;

        ignoreModifications_ = true;
        const Vector<AttributeInfo>* attributes{ serializable->GetAttributes() };

        for (unsigned a{ 0u }; a < attributes->Size(); ++a)
        {
            AttributeInfo attribute{ attributes->At(a) };

            for (PropertyRow* row: rows_)
            {
                if (row->objectName() == toQString(attribute.name_))
                {
                    const Variant attrVal{ serializable->GetAttribute(attribute.name_) };

                    if (!attrVal.IsEmpty())
                        row->setValue(attrVal);
                    else
                        row->setValue(attribute.defaultValue_);
                }
            }
        }

        ignoreModifications_ = false;
    }
}

bool PropertiesWidget::techniqueUsesUnit(Technique* technique, TextureUnit unit)
{
    if (!technique)
        return false;

    const String shortUnitName{ textureUnitNames[unit].Substring(0u, 4u) };

    for (Pass* p: technique->GetPasses())
        if (p->GetPixelShaderDefines().Contains(shortUnitName, false))
            return true;

    return false;
}

PropertyType PropertiesWidget::toPropertyType(VariantType variantType)
{
    switch (variantType)
    {
    default:                         return PT_None;
    case VAR_BOOL:                   return PT_Bool;
    case VAR_INT:   case VAR_INT64:  return PT_Int;
    case VAR_FLOAT: case VAR_DOUBLE: return PT_Float;
    case VAR_VECTOR2:                return PT_Vector2;
    case VAR_VECTOR3:                return PT_Vector3;
    case VAR_VECTOR4:                return PT_Vector4;
    case VAR_INTVECTOR2:             return PT_IntVector2;
    case VAR_INTVECTOR3:             return PT_IntVector3;
    case VAR_QUATERNION:             return PT_Quaternion;
    case VAR_STRING:                 return PT_String;
    case VAR_COLOR:                  return PT_Color;
    case VAR_RESOURCEREF:            return PT_Resource;
    case VAR_RESOURCEREFLIST:        return PT_ResourceList;
    }
}

Variant PropertiesWidget::attributeValue(const String& attributeName)
{
    if (object_->GetTypeInfo()->IsTypeOf<Serializable>())
    {
        for (PropertyRow* row: rows_)
        {
            if (row->objectName() == toQString(attributeName))
                return row->value();
        }
    }

    return Variant::EMPTY;
}

Scene* PropertiesWidget::scene()
{
    if (object_)
    {
        if (object_->IsInstanceOf<Node>())
        {
            Node* node{ static_cast<Node*>(object_.Get()) };

            if (node)
                return node->GetScene();
        }
        else
        {
            Component* component{ static_cast<Component*>(object_.Get()) };

            if (component)
                return component->GetScene();
        }
    }

    return nullptr;
}

int PropertiesWidget::countSubAttributes()
{
    if (!object_ || !object_->IsInstanceOf<Serializable>())
        return 0;

    int count{ 0 };
    Serializable* serializable{ static_cast<Serializable*>(object_.Get()) };

    for (const AttributeInfo& info: *serializable->GetAttributes())
    {
        const Variant attribute{ serializable->GetAttribute(info.name_) };
        if (info.type_ == VAR_RESOURCEREFLIST)
             count += attribute.GetResourceRefList().names_.Size();
         else if (info.type_ == VAR_VARIANTVECTOR)
             count += attribute.GetVariantVector().Size();
         else if (info.type_ == VAR_VARIANTMAP)
             count += attribute.GetVariantMap().Size();
    }

    return count;
}

void PropertiesWidget::handleNumSubAttributesChanged()
{
    emit subAttributeCountChanged(serializable());
    delayedResize();
}
