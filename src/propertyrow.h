/* ManaWarg
// Copyright (C) 2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef PROPERTYROW_H
#define PROPERTYROW_H

#include <QLabel>
#include <QAbstractSpinBox>
#include <QLineEdit>
#include "filewidget.h"

#include <QWidget>

class PropertiesWidget;

class PropertyRow: public QWidget
{
    Q_OBJECT

public:
    PropertyRow(const QString& text, PropertyType type, PropertiesWidget* parent);
    PropertyRow(const QString& text, StringHash resourceType, bool list, PropertiesWidget* parent);

    PropertyType type() const { return type_; }
    StringHash resourceType() const { return resourceType_; }
    bool isList() const { return list_; }

    QLabel* label() const { return label_; }
    QHBoxLayout* layout() const { return layout_; }
    QWidget* propertyWidget(PropertyType propertyType) const;
    QWidget* propertyWidget() const { return propertyWidget(type_); }
    void setOptions(const QStringList& options);

    Variant value() { return value_; }
    bool setValue(const Variant& value);
    static bool isFloatNumber(PropertyType type);


private slots:
    void updateValue();

private:
    void createColorWidget(const QString& text);
    void createTextureWidget(const QString& unit = "");
    void createTechniqueWidgets();
    void createBoolWidget();
    void createNumberWidget();
    void createStringWidget();
    void createEnumWidget();

    void updateColorWidget();
    void updateBoolWidget();
    void updateStringWidget();
    void updateEnumWidget();
    void updateResourceListWidget();
    void updateSpinBoxes();

    bool isNumberType();

    TextureUnit unitFromName(String name);

    PropertiesWidget* propertiesWidget_;
    std::map<PropertyType, QWidget*> propertyWidgets_;
    PropertyType type_;
    StringHash resourceType_;
    bool list_;
    Variant value_;
    QLabel* label_;
    QHBoxLayout* layout_;
};

#endif // PROPERTYROW_H
