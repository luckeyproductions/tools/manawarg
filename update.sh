cd $(dirname "$0")

if [ ! -d Dry ]
then
    git clone https://gitlab.com/luckeyproductions/Dry.git
    cd Dry
else
    cd Dry

    branch=$(
        git rev-parse --abbrev-ref HEAD
    )

    git checkout master
    git pull
fi

./script/cmake_clean.sh
./script/cmake_generic.sh . -DDRY_TOOLS=0 -DDRY_SAMPLES=0 -DDRY_PLAYER=0

make
git checkout $branch
cd -

mkdir build
cd build
qmake ../ManaWarg.pro

sudo make install
sudo chown -R $USER ~/.local/share/luckey/manawarg/
update-icon-caches /usr/local/share/icons/
cd -
rm -rf build

manawarg

