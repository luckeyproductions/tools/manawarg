![ManaWarg](logo.png)

[![pipeline status](https://gitlab.com/luckeyproductions/manawarg/badges/master/pipeline.svg)](https://gitlab.com/luckeyproductions/manawarg/-/commits/master) [![coverage report](https://gitlab.com/luckeyproductions/manawarg/badges/master/coverage.svg)](https://gitlab.com/luckeyproductions/manawarg/-/commits/master) [![Gitter](https://luckeyproductions.nl/images/chat.svg)](https://matrix.to/#/#luckey:matrix.org)


A protean world builder for [Urho3D](https://urho3d.github.io) and the [Dry](https://gitlab.com/luckeyproductions/dry) engine, made with Qt.

To install run `install.sh`.

![Screenshot](Screenshot.png)
